package app.com.kazoku.lib.activities.realm;


import android.app.Activity;
import android.app.Application;

import androidx.fragment.app.Fragment;


import app.com.kazoku.lib.model.Student;
import io.realm.Realm;
import io.realm.RealmResults;


public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }

    //clear all objects from Student.class
    public void clearAll() {

        realm.beginTransaction();
        realm.clear(Student.class);
        realm.commitTransaction();
    }

    //find all objects in the Student.class
    public RealmResults<Student> getBooks() {

        return realm.where(Student.class).findAll();
    }

    //query a single item with the given id
    public Student getBook(String id) {

        return realm.where(Student.class).equalTo("id", id).findFirst();
    }

    //check if Student.class is empty
    public boolean hasBooks() {

        return !realm.allObjects(Student.class).isEmpty();
    }

    //query example
    public RealmResults<Student> queryedBooks() {

        return realm.where(Student.class)
                .contains("author", "Author 0")
                .or()
                .contains("title", "Realm")
                .findAll();

    }
}
