package app.com.kazoku.lib.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.com.kazoku.lib.R;
import app.com.kazoku.lib.model.BeltColor;
import app.com.kazoku.lib.model.buoiTapModel;

public class CaAdapter extends RecyclerView.Adapter<CaAdapter.ViewHolder> {


    List<buoiTapModel> monthModelList;
    public CaAdapter(List<buoiTapModel> monthModelList ) {
        this.monthModelList = monthModelList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ca, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String label = monthModelList.get(position).getCahoc();
        final String day = monthModelList.get(position).getDayOfWeek();
        holder.textView.setText(day+"-" + label);

    }
    public void setData(List<buoiTapModel> monthModelLists){
        monthModelList.clear();
        monthModelList.addAll(monthModelLists);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return monthModelList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text);

        }
    }
}