package app.com.kazoku.lib.model.modelConver;

import androidx.annotation.NonNull;

public class monthModelN {
    private String monthsName;
    private boolean select;
    public monthModelN() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public monthModelN(String monthsName, boolean select) {
        this.monthsName = monthsName;
        this.select = select;
    }

    public String getMonthsName() {
        return monthsName;
    }

    public void setMonthsName(String monthsName) {
        this.monthsName = monthsName;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    @NonNull
    @Override
    public String toString() {
        return  monthsName + select;
    }
}
