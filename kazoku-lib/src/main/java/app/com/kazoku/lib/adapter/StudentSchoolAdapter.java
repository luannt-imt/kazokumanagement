package app.com.kazoku.lib.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.FirebaseDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.com.kazoku.lib.R;
import app.com.kazoku.lib.activities.realm.RealmController;
import app.com.kazoku.lib.model.BeltColor;
import app.com.kazoku.lib.model.Student;
import app.com.kazoku.lib.model.buoiTapModel;
import app.com.kazoku.lib.model.monthModel;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ravi on 16/11/17.
 */

public class StudentSchoolAdapter extends RecyclerView.Adapter<StudentSchoolAdapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Student> contactList;
    private List<Student> contactListFiltered;
    private ContactsAdapterListener listener;
    private Realm realm;

    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    String imgs = "";

    private final static int ALL_PERMISSIONS_RESULT = 107;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView card;
        public TextView txtName;
        public TextView txtDob;
        public TextView txtPhone;
        public TextView txtAddress;
        public TextView txtNote;
        public TextView txtStudyDay;
        public TextView txtFeePayment;
        public TextView txtMonth;
        public ImageView imageBackground;
        public LinearLayout lnBackground;

        public MyViewHolder(View view) {
            super(view);
            card = (CardView) itemView.findViewById(R.id.card_books);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtDob = (TextView) itemView.findViewById(R.id.txtDob);
            txtPhone = (TextView) itemView.findViewById(R.id.txtPhone);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
            txtNote = (TextView) itemView.findViewById(R.id.txtNote);
            txtStudyDay = (TextView) itemView.findViewById(R.id.txtStudyDay);
            txtMonth = (TextView) itemView.findViewById(R.id.txtMonth);
            txtFeePayment = (TextView) itemView.findViewById(R.id.txtFeePayment);
            imageBackground = (ImageView) itemView.findViewById(R.id.image_background);
            lnBackground = (LinearLayout) itemView.findViewById(R.id.lnBackground);

        }
    }


    public StudentSchoolAdapter(Context context, List<Student> contactList ) {
        this.context = context;

        this.contactList = contactList;
        this.contactListFiltered = contactList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_student, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Student student = contactListFiltered.get(position);
        realm = RealmController.getInstance().getRealm();
        final FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();
        Calendar c= Calendar.getInstance();
        int cyear = c.get(Calendar.YEAR);//calender year starts from 1900 so you must add 1900 to the value recevie.i.e., 1990+112 = 2012
        int cmonth = c.get(Calendar.MONTH)+1;



        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());

        Date readDate = null;
        try {
            readDate = df.parse(student.getDateStudy());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(readDate.getTime());
        int yearStudy = cal.get(Calendar.YEAR);
        Log.d("TAG", "Year: "+cal.get(Calendar.YEAR));
        int monthStudy = cal.get(Calendar.MONTH)+1;
        Log.d("TAG", "Day: "+cal.get(Calendar.DAY_OF_MONTH));


        if (cyear ==  yearStudy){
            if (monthStudy == cmonth){
                holder.lnBackground.setBackgroundColor(context.getResources().getColor(R.color.yellowc));
            }
        }
        //this is ap

        // set the title and the snippet
        holder.txtName.setText("Tên: " + student.getName());
        holder.txtDob.setText("Năm sinh: " + student.getDob());
        holder.txtPhone.setText("Số Điện Thoại: " + student.getPhone());
        holder.txtAddress.setText("Địa chỉ: " + student.getAddress());
        holder.txtNote.setText("Ghi chú: " + student.getNote());
        holder.txtStudyDay.setText("Ngày vào học: " + student.getDateStudy());
        String month = "";
        String day = "";
        boolean temp = false;
        boolean tempDay = false;
        for (int i = 0; i < student.getMonthModels().size(); i++) {
            if (student.getMonthModels().get(i).isSelect()) {
                if (!temp) {
                    month = student.getMonthModels().get(i).getMonthsName();
                    temp = true;
                } else {
                    month = month + ", " + student.getMonthModels().get(i).getMonthsName();
                }
            }


        }
        for (int i = 0; i < student.getBuoiTapModels().size(); i++) {
            if (student.getBuoiTapModels().get(i).isSelect()) {
                if (!tempDay) {
                    day = student.getBuoiTapModels().get(i).getDayOfWeek();
                    tempDay = true;
                } else {
                    day = day + ", " + student.getBuoiTapModels().get(i).getDayOfWeek();
                }
            }


        }

        holder.txtFeePayment.setText("Học phí đã đóng: " + month);
        holder.txtMonth.setText("Buổi tập: " + day);

        // load the background image
        if (student.getImageUrl() != null) {
            String avatar = student.getImageUrl();
            byte[] a = Base64.decode(avatar, Base64.DEFAULT);
            System.out.println("bbbb: " + a.toString());
            System.out.println("aaaaavatar: " + avatar);
            Glide.with(context)
                    .load(Base64.decode(avatar, Base64.DEFAULT))

                    .into(holder.imageBackground);


        }

    }

    private String imgProfile() {
        return imgs;
    }

    public void setimgProfile(String img) {
        imgs = img;
    }

    private void selectImage() {
        try {
            PackageManager pm = context.getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, context.getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            ((Activity) context).startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            ((Activity) context).startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(context, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(context, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }




    public List<Student> getModelList() {
        List<Student> list = new ArrayList<>();

        try {
            realm = Realm.getDefaultInstance();
            RealmResults<Student> results = realm
                    .where(Student.class)
                    .findAll();
            list.addAll(realm.copyFromRealm(results));
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return list;
    }

    private void removeAt(int position) {
        contactListFiltered.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, contactListFiltered.size());
    }

    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                contactList = getModelList();
                if (charString.isEmpty()) {
                    contactListFiltered = contactList;
                } else {
                    List<Student> filteredList = new ArrayList<>();
                    for (Student row : contactList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getPhone().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<Student>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected(Student contact);
    }
}
