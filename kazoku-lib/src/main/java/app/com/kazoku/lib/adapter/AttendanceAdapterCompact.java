package app.com.kazoku.lib.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import app.com.kazoku.lib.R;
import app.com.kazoku.lib.activities.realm.RealmController;
import app.com.kazoku.lib.model.BeltColor;
import app.com.kazoku.lib.model.Student;
import app.com.kazoku.lib.model.buoiTapModel;
import app.com.kazoku.lib.model.monthModel;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ravi on 16/11/17.
 */

public class AttendanceAdapterCompact extends RecyclerView.Adapter<AttendanceAdapterCompact.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Student> contactList;
    private List<Student> contactListFiltered;
    private ContactsAdapterListener listener;
    private Realm realm;
    boolean tmpCa1 = false;
    boolean tmpCa2 = false;
    boolean tmpCa3 = false;
    private LayoutInflater inflater;
    MonthAdapter monthAdapter;
    List<monthModel> monthModelList;
    List<buoiTapModel> buoiTapModels;
    List<BeltColor> beltColorList;
    DayOfWeekAdapter dayOfWeekAdapter;
    ColorAdapter colorAdapter;

    Bitmap myBitmap;
    Uri picUri;


    private final static int ALL_PERMISSIONS_RESULT = 107;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView card;
        public TextView txtName;
        public TextView txtDob;
        public TextView txtPhone;
        public TextView txtAddress;
        public TextView txtNote;
        public TextView txtStudyDay;
        public TextView txtFeePayment;
        public ImageView imageBackground;
        public TextView txtMonth;
        public MyViewHolder(View view) {
            super(view);
            card = (CardView) itemView.findViewById(R.id.card_books);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtDob = (TextView) itemView.findViewById(R.id.txtDob);
            txtPhone = (TextView) itemView.findViewById(R.id.txtPhone);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
            txtNote = (TextView) itemView.findViewById(R.id.txtNote);
            txtStudyDay = (TextView) itemView.findViewById(R.id.txtStudyDay);
            txtFeePayment = (TextView) itemView.findViewById(R.id.txtFeePayment);
            txtMonth = (TextView) itemView.findViewById(R.id.txtMonth);
            imageBackground = (ImageView) itemView.findViewById(R.id.image_background);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(contactListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }


    public AttendanceAdapterCompact(Context context, List<Student> contactList) {
        this.context = context;

        this.contactList = contactList;
        this.contactListFiltered = contactList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_student, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Student student = contactListFiltered.get(position);
        realm = RealmController.getInstance().getRealm();

        // set the title and the snippet
        holder.txtName.setText("Tên: " + student.getName()+ " - Số Điện Thoại: " + student.getPhone());
        holder.txtDob.setVisibility(View.GONE);
        holder.txtPhone.setVisibility(View.GONE);
        holder.txtAddress.setVisibility(View.GONE);
        holder.txtNote.setVisibility(View.GONE);
        holder.txtStudyDay.setVisibility(View.GONE);



        holder.txtFeePayment.setVisibility(View.GONE);
        holder.txtMonth.setVisibility(View.GONE);
        holder.imageBackground.setVisibility(View.GONE);

        holder.card.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               /* Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + student.getPhone()));
                callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                context.startActivity(callIntent);*/
               sendSMS(student.getPhone(),student);

            }
        });

    }

    private void refreshData() {
        contactListFiltered = getModelList();
        notifyDataSetChanged();
    }
    public List<Student> getModelList() {
        List<Student> list = new ArrayList<>();

        try {
            realm = Realm.getDefaultInstance();
            RealmResults<Student> results = realm
                    .where(Student.class)
                    .findAll();
            list.addAll(realm.copyFromRealm(results));
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return list;
    }
    private void removeAt(int position) {
        contactListFiltered.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, contactListFiltered.size());
    }
    private void sendSMS(String phone, Student student) {


            Uri uri = Uri.parse("smsto:" + phone);
            Intent it = new Intent(Intent.ACTION_SENDTO, uri);
            it.putExtra("sms_body", "CLB Karatedo Xin Thông Báo. Vsinh: " + student.getName() + " Hôm nay vắng mặt. " + "Quý phụ huynh vui lòng xác nhận cho HLV. Xin cảm ơn.");
            context.startActivity(it);



//        String number = phone;  // The number on which you want to send SMS
//        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
    }
    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = contactList;
                } else {
                    List<Student> filteredList = new ArrayList<>();
                    for (Student row : contactList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getPhone().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<Student>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected(Student contact);
    }
}
