package app.com.kazoku.lib.model.modelConver;

public class BeltColorN {
    private int color;
    private boolean select;

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}
