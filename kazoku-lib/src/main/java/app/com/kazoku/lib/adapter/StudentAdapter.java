package app.com.kazoku.lib.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import app.com.kazoku.lib.R;
import app.com.kazoku.lib.activities.realm.RealmController;
import app.com.kazoku.lib.model.BeltColor;
import app.com.kazoku.lib.model.Student;
import app.com.kazoku.lib.model.buoiTapModel;
import app.com.kazoku.lib.model.caModel;
import app.com.kazoku.lib.model.modelConver.deleteModelReaml;
import app.com.kazoku.lib.model.monneyModelReaml;
import app.com.kazoku.lib.model.monthModel;
import app.com.kazoku.lib.permison.RxPermissions;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by ravi on 16/11/17.
 */

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Student> contactList;
    private List<Student> contactListFiltered = new ArrayList<>();
    private ContactsAdapterListener listener;
    private Realm realm;

    private LayoutInflater inflater;
   public MonthUpdateAdapter monthAdapter;
   public CaAdapter caAdapter;
    List<monthModel> monthModelList;
    List<buoiTapModel> buoiTapModels;
    List<buoiTapModel> buoiTapModelsCa;
    List<BeltColor> beltColorList;
    DayOfWeekAdapter dayOfWeekAdapter;
    ColorAdapter colorAdapter;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    String charString;
    Bitmap myBitmap;
    Uri picUri;
    String imgs = "";
    int counter  = 0;
    int counterDelete  = 0;
    List<String> monthSelect = new ArrayList<>();
    private final static int ALL_PERMISSIONS_RESULT = 107;
    Activity mContext;
    DatabaseReference databaseReference;
    String[] listItems;
    boolean isPhone;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView card;
        public TextView txtName;
        public TextView txtDob;
        public TextView txtPhone;
        public TextView txtAddress;
        public TextView txtNote;
        public TextView txtStudyDay;
        public TextView txtFeePayment;
        public TextView txtMonth;
        public ImageView imageBackground;

        public MyViewHolder(View view) {
            super(view);
            card = (CardView) itemView.findViewById(R.id.card_books);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtDob = (TextView) itemView.findViewById(R.id.txtDob);
            txtPhone = (TextView) itemView.findViewById(R.id.txtPhone);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
            txtNote = (TextView) itemView.findViewById(R.id.txtNote);
            txtStudyDay = (TextView) itemView.findViewById(R.id.txtStudyDay);
            txtMonth = (TextView) itemView.findViewById(R.id.txtMonth);
            txtFeePayment = (TextView) itemView.findViewById(R.id.txtFeePayment);
            imageBackground = (ImageView) itemView.findViewById(R.id.image_background);

//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    // send selected contact in callback
//                    listener.onContactSelected(contactListFiltered.get(getAdapterPosition()));
//                }
//            });
        }
    }


    public StudentAdapter(Context context, List<Student> contactList, ContactsAdapterListener listener,Activity mContext, boolean isPhone) {
        this.context = context;
        this.mContext = mContext;
        this.listener = listener;
        this.contactList = contactList;
        this.contactListFiltered = contactList;
        this.isPhone = isPhone;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_student, parent, false);
        databaseReference = FirebaseDatabase.getInstance().getReference();

        contactList = getModelList();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Student student = contactListFiltered.get(position);
        realm = RealmController.getInstance().getRealm();
        final FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();

         // set the title and the snippet
        holder.txtName.setText("Tên: " + student.getName());
        holder.txtDob.setText("Năm sinh: " + student.getDob());
        holder.txtPhone.setText("Số Điện Thoại: " + student.getPhone());
        holder.txtAddress.setText("Địa chỉ: " + student.getAddress());
        holder.txtNote.setText("Ghi chú: " + student.getNote());
        holder.txtStudyDay.setText("Ngày vào học: " + student.getDateStudy());
        String month = "";
        String day = "";
        boolean temp = false;
        boolean tempDay = false;
        for (int i = 0; i < student.getMonthModels().size(); i++) {
            if (student.getMonthModels().get(i).isSelect()) {
                if (!temp) {
                    month = student.getMonthModels().get(i).getMonthsName();
                    temp = true;
                } else {
                    month = month + ", " + student.getMonthModels().get(i).getMonthsName();
                }
            }


        }
        for (int i = 0; i < student.getBuoiTapModels().size(); i++) {
            if (student.getBuoiTapModels().get(i).isSelect()) {
                if (!tempDay) {
                    day = student.getBuoiTapModels().get(i).getDayOfWeek();
                    tempDay = true;
                } else {
                    day = day + ", " + student.getBuoiTapModels().get(i).getDayOfWeek();
                }
            }


        }

        holder.txtFeePayment.setText("Học phí đã đóng: " + month);
        holder.txtMonth.setText("Buổi tập: " + day);


        // load the background image
//        if (student.getImageUrl() !=null && !student.getImageUrl().isEmpty()) {
//
//            Glide.with(context)
//                    .load(Base64.decode(student.getImageUrl(), Base64.DEFAULT)).diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache( true )
//                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                    .thumbnail(Glide.with(context)
//                            .load(Base64.decode(student.getImageUrl(), Base64.DEFAULT))
//                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE))
//                    .centerCrop()
//                    .into(holder.imageBackground);
//        }else {
//            Glide.with(context)
//                    .load(R.drawable.error).diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache( false )
//                    .into(holder.imageBackground);
//        }


        holder.card.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                deleteModelReaml deleteModelReaml = null;
                                RealmResults<Student> results = realm.where(Student.class).findAll();
                                for (int a = 0; a < results.size(); a++) {
                                    if (student.getId() ==  results.get(a).getId()){

                                        counterDelete =a;
                                          deleteModelReaml = new deleteModelReaml();
                                        deleteModelReaml.setId(RealmController.getInstance().getBooks().size() + System.currentTimeMillis());
                                        deleteModelReaml.setName(results.get(a).getName());
                                        deleteModelReaml.setPhone(results.get(a).getPhone());
                                    }
                                }
                                // Get the student title to show it in toast message

                                realm.beginTransaction();

                                // remove single match
                                results.remove(counterDelete);
                                listener.onContactSelected();
                                realm.commitTransaction();

                                // delete student
                                realm.beginTransaction();
                                
                                realm.copyToRealm(deleteModelReaml);
                                realm.commitTransaction();
                                

                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                dialog.dismiss();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("HLV có muốn xóa võ sinh " + student.getName() + "?").setPositiveButton("Xóa", dialogClickListener)
                        .setNegativeButton("Không", dialogClickListener).show();


//                Toast.makeText(context, title + " is removed from Realm", Toast.LENGTH_SHORT).show();
                return false;
            }
        });


        //update single match from realm
        holder.card.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                Toast.makeText(context,"click id: "+ student.getId() + "po: "+position,Toast.LENGTH_LONG).show();


                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View content = inflater.inflate(R.layout.edit_item, null);
                int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.90);
                int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.90);

                final TextInputEditText name_edit_text = (TextInputEditText) content.findViewById(R.id.name_edit_text);
                final TextInputEditText dob_edit_text = (TextInputEditText) content.findViewById(R.id.dob_edit_texts);
                final TextInputEditText phone_edit_text = (TextInputEditText) content.findViewById(R.id.phone_edit_texts);
                final TextInputEditText address_edit_text = (TextInputEditText) content.findViewById(R.id.address_edit_text);
                final TextInputEditText note_edit_text = (TextInputEditText) content.findViewById(R.id.note_edit_text);



                final Button btnCamera = (Button) content.findViewById(R.id.btnCamera);

                final Button btnClearMonth = (Button) content.findViewById(R.id.btnClearMonth);
                btnCamera.setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void onClick(View v) {

                        RxPermissions rxs = new  RxPermissions((FragmentActivity) mContext);
                        rxs.request(Manifest.permission.CAMERA)
                                .subscribe(granted -> {
                                    if (granted) {
                                        selectImage();
                                        // Tất cả quyền đã cấp
                                    } else {
                                        // Ít nhất 1 quyền đã bị từ chối
                                    }
                                });
//                        rx.request(Manifest.permission.CAMERA).subscribe() {
//                            if (rx.isGranted(Manifest.permission.CAMERA)){
//                                selectImage();
//                            }else{
//                                println("chua")
//                            }
//                        }
//                        selectImage();
                    }
                });


              /*  btnCa4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tmpCa4 = !tmpCa4;
                        v.setBackgroundResource(tmpCa4 ? R.drawable.my_button_bg_select : R.drawable.my_button_bg);

                    }
                });*/

                btnClearMonth.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        monthModelList.clear();
                        for (int i = 0; i < student.getMonthModels().size(); i++) {
                            monthModel model = new monthModel();
                            model.setMonthsName(student.getMonthModels().get(i).getMonthsName());
                            model.setSelect(false);
                            monthModelList.add(model);
                        }
                        monthAdapter.notifyDataSetChanged();
                    }
                });
                name_edit_text.setText(student.getName());
                dob_edit_text.setText(student.getDob());
                phone_edit_text.setText(student.getPhone());
                address_edit_text.setText(student.getAddress());
                note_edit_text.setText(student.getNote());


                RecyclerView recyclerView = (RecyclerView) content.findViewById(R.id.recycler_view);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new GridLayoutManager(context, 6));
                monthModelList = new ArrayList<>();

                RecyclerView recyclerView2 = (RecyclerView) content.findViewById(R.id.recycler_view_dayOfWeek);
                recyclerView2.setHasFixedSize(true);
                recyclerView2.setLayoutManager(new GridLayoutManager(context, 7));
                buoiTapModels = new ArrayList<>();


                RecyclerView recyclerView3 = (RecyclerView) content.findViewById(R.id.recycler_view_ca);
                recyclerView3.setHasFixedSize(true);
                recyclerView3.setLayoutManager(new GridLayoutManager(context, 7));
                buoiTapModelsCa = new ArrayList<>();


                RecyclerView recyclerViewColor = (RecyclerView) content.findViewById(R.id.recycler_view_color);
                recyclerViewColor.setHasFixedSize(true);
                recyclerViewColor.setLayoutManager(new GridLayoutManager(context, 5));
                beltColorList = new ArrayList<>();

                for (int i = 0; i < student.getMonthModels().size(); i++) {

                    monthModel model = new monthModel();
                    model.setMonthsName(student.getMonthModels().get(i).getMonthsName());
                    model.setSelect(student.getMonthModels().get(i).isSelect());
                    monthModelList.add(model);
                    if (student.getMonthModels().get(i).isSelect()){
                        monthSelect.add(String.valueOf(i));
                    }
                }


                monthAdapter = new MonthUpdateAdapter(monthModelList,monthSelect,isPhone);
                recyclerView.setAdapter(monthAdapter);


                for (int i = 0; i < student.getBuoiTapModels().size(); i++) {
                    buoiTapModel model = new buoiTapModel();
                    model.setDayOfWeek(student.getBuoiTapModels().get(i).getDayOfWeek());
                    model.setSelect(student.getBuoiTapModels().get(i).isSelect());
                    model.setCahoc(student.getBuoiTapModels().get(i).getCahoc());
                    buoiTapModels.add(model);
                    if (student.getBuoiTapModels().get(i).isSelect()){
                        buoiTapModelsCa.add(model);
                    }
                }
                String[] testArray = new String[] {"Ca 1","Ca 2","Ca 3","Ca 4","Ca 5","Ca 6",};

                 dayOfWeekAdapter = new DayOfWeekAdapter(buoiTapModels, testArray, mContext, new DayOfWeekAdapter.adapterListener() {
                    @Override
                    public void onClick(  buoiTapModel buoiTap) {
                       // Toast.makeText(mContext,"click" ,Toast.LENGTH_LONG).show();
                        buoiTapModelsCa.add(buoiTap);
                        caAdapter.notifyDataSetChanged();
                    }


                     @Override
                    public void onClickRemove(buoiTapModel buoiTap) {
                        //Toast.makeText(mContext,"remove",Toast.LENGTH_LONG).show();
                        boolean flag =  false;
                        for (int i =0; i<buoiTapModelsCa.size();i++){
                            if (!flag){
                                if (buoiTapModelsCa.get(i).getDayOfWeek().contains(buoiTap.getDayOfWeek())){
                                    buoiTapModelsCa.remove(i);
                                    flag = true;
                                }
                            }
                        }
                        caAdapter.notifyDataSetChanged();

                    }
                },isPhone);
                recyclerView2.setAdapter(dayOfWeekAdapter);

                //ca

                caAdapter = new CaAdapter(buoiTapModelsCa);
                recyclerView3.setAdapter(caAdapter);

                ////////////
                for (int i = 0; i < student.getBeltColorsModel().size(); i++) {
                    BeltColor model = new BeltColor();
                    model.setColor(student.getBeltColorsModel().get(i).getColor());
                    model.setSelect(student.getBeltColorsModel().get(i).isSelect());
                    beltColorList.add(model);
                }

                colorAdapter = new ColorAdapter(beltColorList, context);
                recyclerViewColor.setAdapter(colorAdapter);



                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setView(content)
                        .setTitle("Chỉnh sửa võ sinh")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                RealmResults<Student> results = realm.where(Student.class).findAll();
                                  counter  = 0;
                                for (int a = 0; a < results.size(); a++) {
                                    if (student.getId() ==  results.get(a).getId()){
                                        counter =  a;
                                        System.out.println("aaaa: "+counter);


                                    }
                                }
                                realm.beginTransaction();
                                results.get(counter).setName(name_edit_text.getText().toString());
                                results.get(counter).setDob(dob_edit_text.getText().toString());
                                results.get(counter).setPhone(phone_edit_text.getText().toString());
                                results.get(counter).setAddress(address_edit_text.getText().toString());
                                results.get(counter).setNote(note_edit_text.getText().toString());


                                RealmList<monthModel> monthModelRealmList = new RealmList();
                                for (int i = 0; i < monthModelList.size(); i++) {
                                    monthModel model = realm.createObject(monthModel.class);
                                    model.setSelect(monthModelList.get(i).isSelect());
                                    model.setMonthsName(monthModelList.get(i).getMonthsName());
                                    monthModelRealmList.add(model);
                                }

                                RealmList buoiTapModelRealmList = new RealmList<buoiTapModel>();
                                for (int i = 0; i < buoiTapModels.size(); i++) {
                                    buoiTapModel model = realm.createObject(buoiTapModel.class);
                                    model.setSelect((buoiTapModels.get(i).isSelect()));
                                    model.setDayOfWeek(buoiTapModels.get(i).getDayOfWeek());
                                    model.setCahoc(buoiTapModels.get(i).getCahoc());
                                    buoiTapModelRealmList.add(model);

                                }
                                RealmList beltColorsListRealmList = new RealmList<BeltColor>();
                                for (int i = 0; i < beltColorList.size(); i++) {
                                    BeltColor model = realm.createObject(BeltColor.class);
                                    model.setSelect((beltColorList.get(i).isSelect()));
                                    model.setColor(beltColorList.get(i).getColor());
                                    beltColorsListRealmList.add(model);

                                }


                                results.get(counter).setMonthModels(monthModelRealmList);
                                results.get(counter).setBuoiTapModels(buoiTapModelRealmList);

                                results.get(counter).setBeltColorsModel(beltColorsListRealmList);
                                String imagesParams = imgProfile();
                                if (!imagesParams.isEmpty()) {
                                    results.get(counter).setImageUrl(imagesParams);
                                    System.out.println("cos nha" + imagesParams);
                                } else {
                                    String images  = student.getImageUrl();
                                    System.out.println("roongx nha");
                                    results.get(counter).setImageUrl(images);
                                }


                                realm.commitTransaction();
                                refreshData();
                                setMoney();
                                sendSMS(student.getPhone(), student);
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialog = builder.create();

                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            }
        });

    }

    public String imgProfile() {
        return imgs;
    }

    public void setimgProfile(String img) {
        imgs = img;
    }
     private void selectImage() {
        try {
            PackageManager pm = context.getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, context.getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            ((Activity) context).startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            ((Activity) context).startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(context, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(context, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }


    private void sendSMS(String phone, Student student) {
        List<String> strings = monthAdapter.getMonthClick();
        if (strings.size() > 0) {
            String mo = "";
            boolean flag = false;
            for (String value : strings) {
                if (!flag) {
                    mo = value;
                    flag = true;
                } else {
                    mo = mo + "," + value;
                }

                Log.i("Value of element ", value);
            }
            int year = Calendar.getInstance().get(Calendar.YEAR);

            Uri uri = Uri.parse("smsto:" + phone);
            Intent it = new Intent(Intent.ACTION_SENDTO, uri);
            it.putExtra("sms_body", "Võ sinh " + student.getName() + " đã đóng học phí tháng " + mo + "-"+year);
            context.startActivity(it);
        } else {
            Toast.makeText(context, "Cập nhật thành công", Toast.LENGTH_SHORT).show();

        }


//        String number = phone;  // The number on which you want to send SMS
//        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
    }

    public void refreshData() {
       // counter =0;


        listener.onContactSelected();
    }
    public void setMoney() {
        // counter =0;


        listener.onsetMonney(monthAdapter.getMonthMonney());
    }
    public List<Student> getModelList() {
        List<Student> list = new ArrayList<>();

        try {
            realm = Realm.getDefaultInstance();
            RealmResults<Student> results = realm
                    .where(Student.class)
                    .findAll();
            list.addAll(realm.copyFromRealm(results));
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return list;
    }


    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                charString = charSequence.toString();

                if (charString.isEmpty()) {
                    contactListFiltered = contactList;
                } else {
                    List<Student> filteredList = new ArrayList<>();
                    for (Student row : contactList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getPhone().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<Student>) filterResults.values;
                if (charString.isEmpty()){
                    listener.onContactSelected();
                }else {
                    notifyDataSetChanged();
                }

            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected( );
        void onsetMonney(List<String> strings);
    }
}
