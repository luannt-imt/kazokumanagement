package app.com.kazoku.lib.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import app.com.kazoku.lib.R;
import app.com.kazoku.lib.model.monthModel;

public class MonthAdapter extends RecyclerView.Adapter<MonthAdapter.ViewHolder> {
    List<monthModel> monthModelList;
    List<String> month = new ArrayList<>();
    boolean isPhone = false;
    List<String> monthSelect;
    List<String> monthMonney = new ArrayList<>();
    public MonthAdapter(List<monthModel> monthModelList,List<String> monthSelect, boolean isPhone ) {
        this.monthModelList = monthModelList;
        this.monthSelect = monthSelect;
        this.isPhone = isPhone;
     }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (isPhone){
              view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_button_phone, parent, false);
        }else {
              view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_button, parent, false);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String label = monthModelList.get(position).getMonthsName();
        holder.textView.setText(label);
        if (monthModelList.get(position).isSelect()) {
            holder.textView.setBackgroundResource(R.drawable.my_button_bg_select);

        } else {
            holder.textView.setBackgroundResource(R.drawable.my_button_bg);
            holder.textView.setClickable(true);
        }
        //handling item click event
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!monthModelList.get(position).isSelect()) {
                    monthModelList.get(position).setSelect(true);
                    month.add(monthModelList.get(position).getMonthsName());
                    monthMonney.add(String.valueOf(position));
                    notifyDataSetChanged();
                } else {

                    monthModelList.get(position).setSelect(false);
                    month.remove(monthModelList.get(position).getMonthsName());
                    monthMonney.remove(0);
                    notifyDataSetChanged();


                }

            }
        });
    }

    public List<String> getMonthClick() {
        return month;
    }
    public List<String> getMonthMonney() {
        return monthMonney;
    }
    @Override
    public int getItemCount() {
        return monthModelList.size();
    }

    public List<monthModel> getItem() {
        return monthModelList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text);
        }
    }
}