package app.com.kazoku.lib.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.com.kazoku.lib.utils.GTLogger;

public abstract class GTBaseComplexAdapter<E> extends RecyclerView.Adapter<GTBaseViewHolder> {
    public List<E> _items = new ArrayList<>();
    int[] arrItemLayoutId;
    int layoutGroupId;

    public interface ItemViewListener<E> {
        void onItemClick(int pos, E entity);
    }

    ItemViewListener<E> _listener;

    public void setItemViewListener(ItemViewListener<E> listener) {
        _listener = listener;
    }

    public GTBaseComplexAdapter(List<E> items) {
        _items = items;
    }

    @NonNull
    @Override
    public GTBaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View v;
        if(arrItemLayoutId == null || arrItemLayoutId.length == 0){
            ViewGroup group = (ViewGroup) inflater.inflate(layoutGroupId,null,false);

            v = group.findViewWithTag(""+viewType);
            if(v == null) v = group.getChildAt(viewType);

            group.removeView(v);

        }else {
            int viewId = arrItemLayoutId[viewType];
            v = inflater.inflate(viewId, parent, false);
        }

        if(v != null){

            GTSimpleViewHolder<E> vh = new GTSimpleViewHolder(v);
            return vh;
        }else {
            GTLogger.e("NULL viewItem:");
        }


        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull GTBaseViewHolder holder, int position) {
        if(holder == null) return;
        holder.itemView.setOnClickListener(view -> {
            if (_listener != null) {
                _listener.onItemClick(position, getItem(position));
            }
        });
    }

    public E getItem(int pos) {
        return _items.get(pos);
    }

    public void setArrItemLayoutId(int[] arr) {
        arrItemLayoutId = arr;
    }
    public void setItemLayoutId(int...arr) {
        arrItemLayoutId = arr;
    }

    public void setItemLayoutGroup(int layoutId){
        layoutGroupId = layoutId;
    }
    @Override
    public int getItemCount() {
        return _items.size();
    }

}
