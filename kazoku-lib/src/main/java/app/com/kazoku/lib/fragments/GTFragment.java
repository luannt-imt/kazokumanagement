package app.com.kazoku.lib.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import app.com.kazoku.lib.activities.GTActivity;
import app.com.kazoku.lib.dialog.GTDialogFragmentDefault;
import app.com.kazoku.lib.utils.CommonUtils;

import static android.view.View.NO_ID;

public abstract class GTFragment extends Fragment {
    public String TAG = "";
    private boolean showLogLifeCycle = true;
    private DialogFragment parrentDialog;
    private View rootView;
    private ProgressDialog mProgressDialog;

    public abstract int onGetViewContentId();
    public abstract void onViewLoaded();

    public void showAsDialog(Context context){
        GTDialogFragmentDefault dg = new GTDialogFragmentDefault();
        setAttachDialog(dg);

        dg.setRootFragment(this);
        dg.showDefault(context);
    }

    public GTFragment() {

    }
    public void showLoadingCircle() {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(getActivity());
    }

    public void hideLoadingCircle() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }
    public GTFragment(DialogFragment dlg) {
        this.parrentDialog = dlg;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getClass().getName();
        if (showLogLifeCycle) {
            log("===onCreate:" + TAG);
        }
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int layoutId = onGetViewContentId();
        if(layoutId == 0 || layoutId == NO_ID){
            rootView = new FrameLayout(getContext());

        }else {
            rootView = inflater.inflate(onGetViewContentId(), container, false);
            rootView.setClickable(true);
        }
        return rootView;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onViewLoaded();

        log("Load:" + getClass().getName());
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (showLogLifeCycle) {
            log("===onDestroy:" + TAG);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (showLogLifeCycle) {
            log("===onDetach:" + TAG);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (showLogLifeCycle) {
            log("===onResume:" + TAG);
        }
    }

    public void setShowLogLifeCycle(boolean value) {
        showLogLifeCycle = value;
    }

    public void setAttachDialog(DialogFragment dlg) {
        parrentDialog = dlg;
    }

    public DialogFragment getAttachDialog() {
        return parrentDialog;
    }

    public View getRootView() {
        return rootView;
    }

    public void log(String msg) {
        String tag = getClass().getSimpleName();
//        GTLogger.log(msg);
        Log.e(tag, msg);
    }

    public void logNav(String msg) {
        String tag = getClass().getSimpleName();
        Log.e(tag, "(" + getClass().getSimpleName() + ".java:10) - " + msg);
//        GTLogger.prependCallLocation(msg);
    }

    protected final <T extends View> T fV(@IdRes int id) {
        if (id == NO_ID) {
            return null;
        }
        return rootView.findViewById(id);
    }

    protected final <T extends View> T findViewById(@IdRes int id) {
        if (id == NO_ID) {
            return null;
        }
        return rootView.findViewById(id);
    }

    public void setText(int tvId, String text) {
        if (TextUtils.isEmpty(text)) text = "";
        TextView tv = fV(tvId);
        if (tv != null) {
            tv.setText(text);
        }
    }

    public void sText(int tvId, String text) {
        if (TextUtils.isEmpty(text)) text = "";
        TextView tv = fV(tvId);
        if (tv != null) {
            tv.setText(text);
        }
    }
    public void sTextColor(int tvId, int color) {

        TextView tv = fV(tvId);
        if (tv != null) {
            tv.setTextColor(color);
        }
    }
    public void sText(int tvId, SpannableStringBuilder text) {
        if (TextUtils.isEmpty(text)) return;
        TextView tv = fV(tvId);
        if (tv != null) {
            tv.setText(text);
        }
    }

    public void setText(int tvId, int textId) {
        if (textId == View.NO_ID) return;
        String text = getString(textId);
        setText(tvId, text);
    }

    public void sTap(View.OnClickListener onClick,int...viewIds) {
        if (viewIds.length>0){
            for(int i = 0;i < viewIds.length;i++){
                sTap(viewIds[i],onClick);
            }

        }
    }

    public void sTap(int viewId, View.OnClickListener onClick) {
        View v = fV(viewId);
        if (v != null) {
            v.setOnClickListener(onClick);
        }
    }

    public void sViewDisplay(int viewId, int display) {
        View v = fV(viewId);
        if (v != null) {
            v.setVisibility(display);
        }
    }

    public void goneOrVisibleBy(int viewId, boolean isGone) {
        if (isGone) {
            sViewDisplay(viewId, View.GONE);
        } else {
            sViewDisplay(viewId, View.VISIBLE);
        }
    }
    public void goneOrVisibleByMutilPayLater(int keepPlaceLayout,int directBarLayout , boolean isGone) {
        if (isGone) {
            sViewDisplay(keepPlaceLayout, View.GONE);
            sViewDisplay(directBarLayout, View.GONE);
        } else {
            sViewDisplay(keepPlaceLayout, View.VISIBLE);
            sViewDisplay(directBarLayout, View.VISIBLE);
        }
    }
    public void goneOrVisibleByMutilPayNow(int ATMLayout,int masterCardLayout ,int QRCodeLayout, boolean isGone) {
        if (isGone) {
            sViewDisplay(ATMLayout, View.VISIBLE);
            sViewDisplay(masterCardLayout, View.VISIBLE);
            sViewDisplay(QRCodeLayout, View.VISIBLE);
        } else {
            sViewDisplay(ATMLayout, View.GONE);
            sViewDisplay(masterCardLayout, View.GONE);
            sViewDisplay(QRCodeLayout, View.GONE);
        }
    }

    public boolean onFragmentBack() {
        if (showLogLifeCycle) {
            log("===onFragmentBack:" + TAG);
        }
        return false;
    }

    public GTActivity getGTActivity() {
        if (getActivity() instanceof GTActivity) {
            return (GTActivity) getActivity();
        }
        return null;
    }

    public void pushFragment(Fragment fg) {
        if (getGTActivity() != null) {
            getGTActivity().pushFragment(fg);
        }
    }

    public void pushFragmentDetails(Fragment fg) {
        if (getGTActivity() != null) {
            getGTActivity().pushFragmentDetails(fg);
        }
    }
    public void pushFragmentDetailsAnimationTopBottom(Fragment fg) {
        if (getGTActivity() != null) {
            getGTActivity().pushFragmentFromBottom(fg);
        }
    }

    public void addModalFrament(Fragment fg) {
        if (getGTActivity() != null) {
            getGTActivity().addFragment(fg);
        }
    }
    public void addModalFramentAc(Fragment fg) {
        if (getGTActivity() != null) {
            getGTActivity().addFragmentNew(fg);
        }
    }

    public void replaceFragment(Fragment fg) {
        if (getGTActivity() != null) {
            getGTActivity().replaceFragment(fg);
        }
    }
    public void replaceFragmentAnimationTopToBottom(Fragment fg) {
        if (getGTActivity() != null) {
            getGTActivity().replaceFragmentToptoBottomAnimation(fg);
        }
    }

    public void replaceFragmentTest(Fragment fg) {
        if (getGTActivity() != null) {
            getGTActivity().replaceFragmentTest(fg);
        }
    }
    public void replaceFragmentAnimation(Fragment fg) {
        if (getGTActivity() != null) {
            getGTActivity().replaceFragmentAnimation(fg);
        }
    }

    Dialog dg;
    public void showLoading() {
//        if(dgLoading == null){
//            dgLoading = new GTLoadingDialog();
//        }
//        dgLoading.showDefault(this);
        if(dg == null){
//            dg = new Dialog(getActivity());
//            dg.setContentView(R.layout.view_gotadi_progress_layout);
//            dg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            dg.show();
        }

    }
    public void hideLoading() {
//        if(dgLoading != null){
//            if(!isAdded()){
//                return;
//            }
//            dgLoading.dismissAllowingStateLoss();
//            dgLoading = null;
//        }
        if(dg != null){
            dg.dismiss();
            dg = null;
        }
    }
    public void handleLoginDialog(String msg,int id) {
//        GTDialogUtil.showGotadiUserErrorDialogPopupShow1Button(getFragmentManager(), msg, id,
//                getString(R.string.gotadi_common_btn_close), new GotadiUserErrorDialogFragment.OnClickDialogError() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                });
    }

    public void showAlert(String msg) {
//
//        GTDialogUtil.createDialog1Button(getActivity(), "", msg, 0,
//                getString(R.string.ok), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                    }
//                });
    }

    public  void showAlertDefault(Context context, String message){
        if(context == null) return;
        AlertDialog.Builder adb = new AlertDialog.Builder(context);

        adb.setMessage(message);
        adb.setCancelable(false);
        adb.setPositiveButton("OK",null);

        AlertDialog dialog = adb.show();
        TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);

        dialog.show();
    }
    public void alert(String msg) {

//        GTDialogHelper.

//        GTDialogUtil.createDialog1Button(getActivity(), "", msg, 0,
//                getString(R.string.ok), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                    }
//                });
    }
    public void alert(int  msgId) {
        String msg = getString(msgId);
        alert(msg);
//        GTDialogUtil.createDialog1Button(getActivity(), "", msg, 0,
//                getString(R.string.ok), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                    }
//                });
    }
    public void alertInput(String msg) {
//        GTDialogUtil.createDialog1Button(getActivity(), "", msg, 0,
//                getString(R.string.ok), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                    }
//                });
    }
    public void alertInput(int msgId) {
        String msg = getString(msgId);
//        GTDialogUtil.createDialog1Button(getActivity(), "", msg, 0,
//                getString(R.string.ok), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                    }
//                });
    }
//    public void alert(String msg, DialogInterface.OnClickListener onOk) {
//        GTDialogUtil.createDialog1Button(getActivity(), "", msg, 0,
//                getString(R.string.ok), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//
//                    }
//                });
//    }
    public void alert(String msg, DialogInterface.OnClickListener onOk) {
//        GTDialogUtil.createDialog1Button(getActivity(), "", msg, 0,
//                getString(R.string.ok), onOk);
    }

    public void showAlert(int msgId) {
//        GTDialogUtil.createDialog1Button(getActivity(), "", getString(msgId), 0,
//                getString(R.string.ok), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                    }
//                });
    }

    public String gText(int editId) {
        TextView edt = fV(editId);
        if (edt == null) return "";

        return edt.getText().toString();
    }

    public String gTextTrim(int editId) {
        return gText(editId).trim();
    }

    public boolean isNetworkAvailable() {
//        if (!GotadiUtils.isNetworkConnected(getContext())) {
//            GTDialogUtil.createDialog1Button(getActivity(), "", getString(R.string.err_connection_network), 0,
//                    getString(R.string.ok), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
//                        }
//                    });
//
//            return false;
//        }
        return true;
    }

    public void showInputError(String msg) {
//        GTDialogUtil.createDialog1Button(getActivity(), "", msg, 0, getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        });
    }
    public void setChildFragment(Fragment fg, int containerId){
        getChildFragmentManager().beginTransaction().replace(containerId,fg).commit();
    }
    public void showAlertNetwork(){
//        alert(getString(R.string.gotadi_error_occurred), (dialogInterface, i) -> {
//            dialogInterface.dismiss();
//            getActivity().onBackPressed();
//        });
    }
    public void showAlertServerConnection(){
//        alert(getString(R.string.gotadi_error_occurred), (dialogInterface, i) -> {
//            dialogInterface.dismiss();
//            getActivity().onBackPressed();
//        });
    }
    public  View getViewByInflate(int layoutId){
        LayoutInflater inflater = (LayoutInflater)   getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View content    = inflater.inflate(layoutId,null);
        return content;
    }
    public void onAction(int action){

    }

    public int getColor(int color){
        return ContextCompat.getColor(getContext(),color);
    }


}
