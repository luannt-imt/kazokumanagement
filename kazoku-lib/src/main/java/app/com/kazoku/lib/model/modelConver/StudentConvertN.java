package app.com.kazoku.lib.model.modelConver;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import app.com.kazoku.lib.model.modelConver.BeltColorN;
import app.com.kazoku.lib.model.modelConver.buoiTapModelN;
import app.com.kazoku.lib.model.modelConver.caModelN;
import app.com.kazoku.lib.model.modelConver.monthModelN;

public class StudentConvertN {
    public long id;

    public String imageUrl;

    public String name;

    public String dob;

    public String phone;

    public String address;

    public String note;
    public String dateStudy;
    public List<monthModelN> monthModels;
    public List<caModelN> caModels;
    public List<buoiTapModelN> buoiTapModels;
    public List<BeltColorN> beltColorsModel;
    public StudentConvertN() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public StudentConvertN(long id, String imageUrl, String name, String dob, String phone, String address, String note, String dateStudy, List<monthModelN> monthModels, List<caModelN> caModels, List<buoiTapModelN> buoiTapModels, List<BeltColorN> beltColorsModel) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.name = name;
        this.dob = dob;
        this.phone = phone;
        this.address = address;
        this.note = note;
        this.dateStudy = dateStudy;
        this.monthModels = monthModels;
        this.caModels = caModels;
        this.buoiTapModels = buoiTapModels;
        this.beltColorsModel = beltColorsModel;
    }

    @NonNull
    @Override
    public String toString() {
        return  address + phone + dob + name + imageUrl + id + note + dateStudy + monthModels +caModels+ buoiTapModels +beltColorsModel;
    }
}
