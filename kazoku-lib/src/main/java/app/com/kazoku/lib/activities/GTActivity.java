package app.com.kazoku.lib.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.Locale;

import app.com.kazoku.lib.R;
import app.com.kazoku.lib.fragments.GTFragment;
import app.com.kazoku.lib.utils.CommonUtils;
import app.com.kazoku.lib.utils.GTLogger;
import app.com.kazoku.lib.utils.ViewHelper;

public class GTActivity extends AppCompatActivity {
    private int fgContainerId;
    private Fragment currentFragment;
    FragmentManager fragmentManager;
    private ProgressDialog mProgressDialog;
    public String TAG = "sssssssssssssssssssssssssss: ";
    private boolean showLogLifeCycle = true;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransitionEnter();
        setContentView(R.layout.gtd_activity_fragment);
        fgContainerId = R.id.fgContainer;
        mProgressDialog = new  ProgressDialog(this);
        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            GTLogger.e("onBackStack..");
            showFragmentStack();
        });
        setAppLocale("vi");
        TAG = getClass().getName();
        if (showLogLifeCycle) {
            log("===onCreate:" + TAG);
        }

    }
    public void log(String msg) {
        String tag = getClass().getSimpleName();
//        GTLogger.log(msg);
        Log.e(tag, msg);
    }
    private void setAppLocale(String localeCode){
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(new Locale(localeCode.toLowerCase()));
        } else {
            config.locale = new Locale(localeCode.toLowerCase());
        }
        resources.updateConfiguration(config, dm);
    }
    public ViewGroup getContentView() {
        return findViewById(fgContainerId);
    }

    public void setFragmentContainerId(int viewId) {
        fgContainerId = viewId;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        int stackCount = getSupportFragmentManager().getBackStackEntryCount();
        if (stackCount == 0) {
            GTLogger.e("current is root fragment.");
            super.onBackPressed();
        } else {
            showFragmentStack();
            if (currentFragment != null && currentFragment instanceof GTFragment) {
                GTLogger.log("current:" + currentFragment.getClass().getSimpleName());
                if (!((GTFragment) currentFragment).onFragmentBack()) {
                    super.onBackPressed();
                }
            } else {
                super.onBackPressed();
            }
        }
    }

    public Fragment getCurrentFragment() {
        return currentFragment;
    }

    public void setOverlayStatusbar() {

//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
    }

    public void runAction(int action) {

    }

    public void hideLoading() {
        mProgressDialog.dismiss();
    }
    public void showLoadingCircle(String title, String content) {

        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(content);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

    }

    public void hideLoadingCircle() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();

        }
    }
    public void addView(int viewId) {
        ViewGroup rootView = (ViewGroup) getWindow().getDecorView().getRootView();
        View v = ViewHelper.getViewByInflate(this, viewId);
        if (rootView != null) rootView.addView(v, 0);
    }

    public void setLocale(String languageCode, String country) {
//        Locale locale = new Locale(languageCode, country);
//        Locale.setDefault(locale);
//        gtSettings.setCurrentLanguage(this, languageCode, country);
//        Resources resources = getResources();
//        Configuration configuration = resources.getConfiguration();
//        if (!configuration.locale.equals(locale)) {
//            configuration.locale = locale;
//            resources.updateConfiguration(configuration, null);
//        }
    }

    public void setLocaleVN() {
//        gtSettings.setCurrentLanguage(this, GlobalConst.LANG_VIE, GlobalConst.COUNTRY_VIETNAM);
//        Locale locale = new Locale(GlobalConst.LANG_VIE, GlobalConst.COUNTRY_VIETNAM);
//        Locale.setDefault(locale);
//        Resources resources = getResources();
//        Configuration configuration = resources.getConfiguration();
//        if (!configuration.locale.equals(locale)) {
//            configuration.locale = locale;
//            resources.updateConfiguration(configuration, null);
//        }
    }

    public void setLocaleReference(String lang, String country) {
        Locale locale = new Locale(lang, country);
        Resources resources = getResources();
        Configuration configuration = resources.getConfiguration();
        if (!configuration.locale.equals(locale)) {
            configuration.locale = locale;
            resources.updateConfiguration(configuration, null);
        }
    }

    public void showActivity(Class cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);


    }

    public void showActivityResult(Class cls, int requestCode) {
        Intent intent = new Intent(this, cls);
        startActivityForResult(intent, requestCode);
    }

    public void setRootFragment(Fragment fragment, int fgContainerId) {
        synchronized (this) {
            currentFragment = fragment;
            this.fgContainerId = fgContainerId;
            if (fragment == null || fgContainerId == 0) {
                return;
            }
            replaceFragment(fragment, false);
        }
    }

    public void setRootFragment(Fragment fragment) {
        synchronized (this) {
            currentFragment = fragment;
            if (fragment == null || fgContainerId == 0) {
                return;
            }
            replaceFragment(fragment, false);
        }
    }

    public void addModalFrament(Fragment fg, GTActivity gtActivity) {
        if (gtActivity != null) {
            if (!fg.isAdded()) {
                currentFragment = fg;
                gtActivity.addFragment(fg);
            }
        }
    }

    public void addFragment(Fragment fg) {
        currentFragment = fg;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.gotadi_slide_from_bottom, R.anim.gotadi_slide_to_top,
                R.anim.gotadi_slide_from_top, R.anim.gotadi_slide_to_bottom);
        transaction.add(this.fgContainerId, fg);
        transaction.addToBackStack(fg.getClass().getName());

        transaction.commit();
    }

    public void addFragmentNew(Fragment fg) {
        currentFragment = fg;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.gotadi_slide_from_bottom, R.anim.gotadi_slide_to_top,
                R.anim.gotadi_slide_from_top, R.anim.gotadi_slide_to_bottom);
        transaction.add(this.fgContainerId, fg);

        transaction.detach(currentFragment);
        transaction.attach(currentFragment);
        transaction.commit();

    }

    private void replaceFragment(@NonNull Fragment fm, boolean storeHistory) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        currentFragment = fm;
        transaction.replace(this.fgContainerId, fm);
        if (storeHistory) {
            transaction.addToBackStack(null);
        }
        // Commit the transaction
        transaction.commitAllowingStateLoss();
    }

    public void pushFragment(Fragment fg) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.gotadi_slide_from_right, R.anim.gotadi_slide_to_left,
                R.anim.gotadi_slide_from_left, R.anim.gotadi_slide_to_right);
        transaction.add(this.fgContainerId, fg);
        transaction.addToBackStack(fg.getClass().getName());

        transaction.commitAllowingStateLoss();
    }

    public void pushFragmentDetails(Fragment fg) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.gotadi_slide_from_right, R.anim.gotadi_slide_to_left,
                R.anim.gotadi_slide_from_left, R.anim.gotadi_slide_to_right);
        transaction.add(this.fgContainerId, fg);
        transaction.addToBackStack(null);

        transaction.commitAllowingStateLoss();
    }

    public void pushFragmentFromBottom(Fragment fg) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.gotadi_slide_from_bottom, R.anim.gotadi_slide_to_top,
                R.anim.gotadi_slide_to_top, R.anim.gotadi_slide_from_bottom);

        transaction.add(this.fgContainerId, fg);
        transaction.addToBackStack(null);

        transaction.commitAllowingStateLoss();
    }

    public static boolean isFragmentInBackstack(final FragmentManager fragmentManager, final String fragmentTagName) {
        for (int entry = 0; entry < fragmentManager.getBackStackEntryCount(); entry++) {
            if (fragmentTagName.equals(fragmentManager.getBackStackEntryAt(entry).getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean showFragmentStack() {
        int stackCount = getSupportFragmentManager().getBackStackEntryCount();
        if (stackCount == 0) GTLogger.e("Empty stack.");
        String tag = lastFragment;
        for (int entry = 0; entry < stackCount; entry++) {
            tag = getSupportFragmentManager().getBackStackEntryAt(entry).getName();
            GTLogger.log("- " + entry + "," + tag);
            currentFragment = getSupportFragmentManager().findFragmentByTag(tag);
        }
        return false;
    }

    public void removeFragment(Fragment fg) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.gotadi_slide_from_top, R.anim.gotadi_slide_to_bottom,
                R.anim.gotadi_slide_to_bottom, R.anim.gotadi_slide_from_top);
        transaction.remove(fg);
        transaction.commit();
    }

    public String lastFragment = "";

    public void replaceFragment(Fragment fg) {
        String tag = fg.getClass().getName();
        if (tag.equalsIgnoreCase(lastFragment)) return;

        currentFragment = fg;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.gotadi_slide_from_right, R.anim.gotadi_slide_to_left,
                R.anim.gotadi_slide_from_left, R.anim.gotadi_slide_to_right);

        transaction.replace(this.fgContainerId, fg);
        transaction.addToBackStack(tag);
        transaction.commitAllowingStateLoss();
    }

    public void replaceFragmentToptoBottomAnimation(Fragment fg) {
        String tag = fg.getClass().getName();
        if (tag.equalsIgnoreCase(lastFragment)) return;

        currentFragment = fg;

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.gotadi_slide_from_bottom, R.anim.gotadi_slide_to_top,
                R.anim.gotadi_slide_from_top, R.anim.gotadi_slide_to_bottom);
        transaction.replace(this.fgContainerId, fg);
        transaction.addToBackStack(tag);
        transaction.commitAllowingStateLoss();
    }

    public void replaceFragmentTest(Fragment fg) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.gotadi_slide_from_right, R.anim.gotadi_slide_to_left,
                R.anim.gotadi_slide_from_left, R.anim.gotadi_slide_to_right);
        transaction.replace(this.fgContainerId, fg);
        transaction.addToBackStack(null);

        transaction.commitAllowingStateLoss();
    }

    public void replaceFragmentBottomToTop(Fragment fg) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.gotadi_slide_from_bottom, R.anim.gotadi_slide_to_top,
                R.anim.gotadi_slide_from_top, R.anim.gotadi_slide_to_bottom);
        transaction.replace(this.fgContainerId, fg);
        transaction.addToBackStack(null);

        transaction.commitAllowingStateLoss();
    }

    public void replaceFragmentAnimation(Fragment fg) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.gotadi_slide_from_bottom, R.anim.gotadi_slide_to_top,
                R.anim.gotadi_slide_to_top, R.anim.gotadi_slide_from_bottom);
        transaction.replace(this.fgContainerId, fg);
        transaction.addToBackStack(null);

        transaction.commitAllowingStateLoss();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    protected boolean isAnimationTopBottom() {
        return false;
    }

    protected void overridePendingTransitionEnter() {
        if (isAnimationTopBottom()) {
            overridePendingTransition(R.anim.gotadi_slide_from_bottom, R.anim.gotadi_slide_to_top);
        } else {
            overridePendingTransition(R.anim.gotadi_slide_from_right, R.anim.gotadi_slide_to_left);
        }
    }

    protected void overridePendingTransitionExit() {
        if (isAnimationTopBottom()) {
            overridePendingTransition(R.anim.gotadi_slide_from_top, R.anim.gotadi_slide_to_bottom);
        } else {
            overridePendingTransition(R.anim.gotadi_slide_from_left, R.anim.gotadi_slide_to_right);
        }
    }

    public void updateStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }
}
