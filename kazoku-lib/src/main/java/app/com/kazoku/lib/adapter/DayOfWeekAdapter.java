package app.com.kazoku.lib.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;



import java.util.List;

import app.com.kazoku.lib.R;
import app.com.kazoku.lib.model.buoiTapModel;

public class DayOfWeekAdapter extends RecyclerView.Adapter<DayOfWeekAdapter.ViewHolder> {
     List<buoiTapModel> buoiTapModels;
    String[] listItems;
    Context mContext;
    adapterListener adapterListener;
    boolean isPhone;
    public DayOfWeekAdapter(List<buoiTapModel> monthModelList, String[] listItems, Context context,adapterListener adapterListener, boolean isPhone) {
       this.buoiTapModels = monthModelList;
       this.listItems = listItems;
       this.mContext = context;
       this.adapterListener = adapterListener;
       this.isPhone = isPhone;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (isPhone){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_button_phone, parent, false);
        }else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_button, parent, false);
        }
        return new ViewHolder(view);
    }
    public interface  adapterListener {

        void onClick(  buoiTapModel buoiTap);
        void onClickRemove(buoiTapModel buoiTap);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String label = buoiTapModels.get(position).getDayOfWeek();
        holder.textView.setText(label);
        if (buoiTapModels.get(position).isSelect()){
            holder.textView.setBackgroundResource(R.drawable.my_button_bg_select);
        }else {
            holder.textView.setBackgroundResource(R.drawable.my_button_bg);
        }
        //handling item click event
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!buoiTapModels.get(position).isSelect()){
                    buoiTapModels.get(position).setSelect(true);
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
                    mBuilder.setTitle("Chọn ca");
                    mBuilder.setSingleChoiceItems(listItems, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
//                            buoiTapModels.get(listItems[i]);
                            buoiTapModels.get(position).setCahoc(listItems[i]);
                            dialogInterface.dismiss();
                            notifyDataSetChanged();
                            adapterListener.onClick( buoiTapModels.get(position));
                        }
                    });


                    AlertDialog mDialog = mBuilder.create();
                    mDialog.setCancelable(false);

                    mDialog.show();

                }else {
                    buoiTapModels.get(position).setSelect(false);

                    notifyDataSetChanged();
                    adapterListener.onClickRemove(buoiTapModels.get(position) );
                    buoiTapModels.get(position).setCahoc("");
                }
//                Toast.makeText(holder.textView.getContext(), label, Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return buoiTapModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text);
        }
    }
}