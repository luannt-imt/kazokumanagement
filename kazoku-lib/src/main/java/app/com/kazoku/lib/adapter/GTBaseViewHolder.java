package app.com.kazoku.lib.adapter;

import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import app.com.kazoku.lib.utils.ViewHelper;


public abstract class GTBaseViewHolder<E> extends RecyclerView.ViewHolder {
    E entity;
    public GTBaseViewHolder(View itemView) {
        super(itemView);
    }
    public void sText(int textViewId,String text){
        TextView tv = itemView.findViewById(textViewId);
        if(tv != null){
            if(TextUtils.isEmpty(text) || text.equalsIgnoreCase("null")) text = "";
            tv.setText(text);
        }
    }
    public void sText(int textViewId, Spanned text){
        TextView tv = itemView.findViewById(textViewId);
        if(tv != null){
//            if(TextUtils.isEmpty(text) || text.equalsIgnoreCase("null")) text = "";
            tv.setText(text);
        }
    }
     public void sText(int textViewId,int textId){
        TextView tv = itemView.findViewById(textViewId);
        if(tv != null){
            String text = getString(textId);

            if(TextUtils.isEmpty(text) || text.equalsIgnoreCase("null")) text = "";
            tv.setText(text);
        }
    }
 
     public void sTextOrGoneEmpty(int textViewId,String text){
        TextView tv = itemView.findViewById(textViewId);
        if(tv != null){
            if(TextUtils.isEmpty(text)){
                text = "";
                tv.setVisibility(View.GONE);
                return;
            }
            tv.setText(text);
        }
    }
    public void sText(int textViewId,SpannableStringBuilder text){
        TextView tv = itemView.findViewById(textViewId);
        if(tv != null){
            tv.setText(text);
        }
    }
    public void setDisplay(int viewId,int displayState){
        View v = itemView.findViewById(viewId);
        if(v != null){
            v.setVisibility(displayState);
        }
    }
    public void sTap(int viewId, View.OnClickListener onClick) {
        View v = itemView.findViewById(viewId);
        if (v != null) {
            v.setOnClickListener(onClick);
        }
    }
    public void setImage(int viewId,String imageUrl){
        ViewHelper.bindImage(itemView,viewId,imageUrl);
    }
    public String getString(int stringId){
        if(stringId > 0){
            return itemView.getContext().getResources().getString(stringId);
        }
        return "";
    }
}
