package app.com.kazoku.lib.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import app.com.kazoku.lib.R;
import app.com.kazoku.lib.utils.GTLogger;

public class GTDialogFragmentDefault extends DialogFragment {
    public static final int ACTION_YES = 0;
    public static final int ACTION_NO = 1;

    int fgContainerId;
    public ViewGroup rootView;
    Dialog dialog;

    Fragment rootFragment;

    public interface DialogActionListener {
        public void onAction(int action);
    }

    DialogActionListener _listener;

    public void setDialogAction(DialogActionListener listener) {
        _listener = listener;

    }

    public DialogActionListener getListener() {
        return _listener;
    }

    public int onGetLayoutId(){
        return R.layout.gtd_fg_dialog;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
          dialog = super.onCreateDialog(savedInstanceState);
        dialog.setCancelable(false);

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();

        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;

//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        getDialog().getWindow().setAttributes(params);
        getDialog().getWindow().setGravity(Gravity.BOTTOM);

        getDialog().setCanceledOnTouchOutside(true);
        getDialog().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    public void setRootFragment(Fragment fg){
        rootFragment = fg;
    }
    public void setRootFragment(Fragment fg, int containerViewId){
        rootFragment    = fg;
        fgContainerId   = containerViewId;
    }

    public void setFragment(Fragment fg) {
        if (fgContainerId == View.NO_ID) return;
        this.getChildFragmentManager().beginTransaction().replace(fgContainerId, fg).commit();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int layoutId = onGetLayoutId();
        if (layoutId != 0) {
            fgContainerId = R.id.fgContainer;
            rootView = (ViewGroup) inflater.inflate(layoutId, container, false);
            return rootView;
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(rootFragment != null) addFragment(rootFragment);
        onViewLoaded();
    }

    public void onViewLoaded(){

    }

    public void addContentView(int viewId) {
        View v = getLayoutInflater().inflate(viewId, null, false);
        rootView.addView(v);
    }

    public void addFragment(Fragment fg) {
        if (fg == null) return;
        try {
            FragmentManager fm = getChildFragmentManager();
            String tag =  getClass().getName();

            FragmentTransaction ft = fm.beginTransaction();
            ft.add(fgContainerId,fg, tag);
            ft.commitAllowingStateLoss();


        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }

    public void showDefault(Context context) {
        if(context instanceof AppCompatActivity){
            FragmentActivity act = (FragmentActivity)context;
            show(act.getSupportFragmentManager(), getClass().getName());
        }
    }
    public void showDefault(Context context, Fragment fg) {
        if(context instanceof AppCompatActivity){
            FragmentActivity act = (FragmentActivity)context;
            show(act.getSupportFragmentManager(), getClass().getName());
        }
    }

    public void closeDialog(   ) {
         dialog.dismiss();
    }
//
//    public void showDefault(FragmentActivity fg) {
//        if (fg == null) return;
////        show(fg.getSupportFragmentManager(), getClass().getName());
//        try {
//            FragmentManager fm = fg.getSupportFragmentManager();
//            if(fm.findFragmentByTag(getClass().getName()) != null){
//                return;
//            }
//
//            FragmentTransaction ft = fm.beginTransaction();
//            ft.add(this, getClass().getName());
//            ft.commitAllowingStateLoss();
//
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        }
//    }

    public void log(String msg){
        GTLogger.e(msg);
    }
}
