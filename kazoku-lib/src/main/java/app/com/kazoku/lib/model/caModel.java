package app.com.kazoku.lib.model;

import io.realm.RealmObject;

public class caModel extends RealmObject {
    private boolean select;
    private String caHoc;

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }



    public String getCaHoc() {
        return caHoc;
    }

    public void setCaHoc(String caHoc) {
        this.caHoc = caHoc;
    }

}
