package app.com.kazoku.lib.model;


import java.util.ArrayList;
import java.util.List;

import app.com.kazoku.lib.R;
import io.realm.RealmObject;

public class BeltColor extends RealmObject {
    private int color;
    private boolean select;

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }


    public static List<BeltColor> getDemo() {
        List<BeltColor> beltColorList = new ArrayList<>();
        BeltColor beltColor = new BeltColor();
        beltColor.setColor(R.color.white);
        beltColor.setSelect(false);
        beltColorList.add(beltColor);

        beltColor = new BeltColor();
        beltColor.setColor(R.color.yellow);
        beltColor.setSelect(false);
        beltColorList.add(beltColor);

        beltColor = new BeltColor();
        beltColor.setColor(R.color.orage);
        beltColor.setSelect(false);
        beltColorList.add(beltColor);

        beltColor = new BeltColor();
        beltColor.setColor(R.color.green);
        beltColor.setSelect(false);
        beltColorList.add(beltColor);

        beltColor = new BeltColor();
        beltColor.setColor(R.color.blue_light);
        beltColor.setSelect(false);
        beltColorList.add(beltColor);


        beltColor = new BeltColor();
        beltColor.setColor(R.color.red);
        beltColor.setSelect(false);
        beltColorList.add(beltColor);

        beltColor = new BeltColor();
        beltColor.setColor(R.color.purple);
        beltColor.setSelect(false);
        beltColorList.add(beltColor);

        beltColor = new BeltColor();
        beltColor.setColor(R.color.brown1);
        beltColor.setSelect(false);
        beltColorList.add(beltColor);

        beltColor = new BeltColor();
        beltColor.setColor(R.color.brown2);
        beltColor.setSelect(false);
        beltColorList.add(beltColor);

        beltColor = new BeltColor();
        beltColor.setColor(R.color.brown3);
        beltColor.setSelect(false);
        beltColorList.add(beltColor);

        beltColor = new BeltColor();
        beltColor.setColor(R.color.black);
        beltColor.setSelect(false);
        beltColorList.add(beltColor);

        return beltColorList;
    }
}
