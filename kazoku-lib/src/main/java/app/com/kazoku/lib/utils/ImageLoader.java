package app.com.kazoku.lib.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class ImageLoader {
    public static void loadImage(Context context, ImageView iv, String url) {
        if (iv == null) return;
        if (TextUtils.isEmpty(url)) return;

        Glide
                .with(context)
                .load(url)
//                .centerCrop()
                .into(iv);
    }

    public static void loadImageWithCache(Context context, ImageView iv, String url) {
        if (context == null || iv == null || TextUtils.isEmpty(url)) {
            return;
        }
        Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(iv);
    }
}
