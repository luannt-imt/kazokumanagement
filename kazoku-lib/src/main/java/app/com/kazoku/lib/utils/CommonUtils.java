package app.com.kazoku.lib.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.provider.Settings;
 import android.widget.ProgressBar;

import androidx.core.content.ContextCompat;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.DoubleBounce;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.com.kazoku.lib.R;

public final class CommonUtils {

    private static final String TAG = "CommonUtils";

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }
    public static ProgressDialog showLoadingDialog(Activity context) {

        final ProgressDialog progress = new ProgressDialog(context);
        progress.setCancelable(false);
        progress.show();

        if (progress.getWindow() != null) {
            progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable drawable = new ProgressBar(context).getIndeterminateDrawable().mutate();
            drawable.setColorFilter(ContextCompat.getColor(context, R.color.button_color_bg_start),
                    PorterDuff.Mode.SRC_IN);
            progress.setIndeterminateDrawable(drawable);
        }

        progress.setContentView(R.layout.progress_dialog);
        Sprite doubleBounce = new DoubleBounce();
        progress.setIndeterminateDrawable(doubleBounce);



        return progress;
    }

}