package app.com.kazoku.lib.model;

import io.realm.RealmObject;

public class buoiTapModelPojo  {
    private String dayOfWeek;
    private boolean select;
    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}
