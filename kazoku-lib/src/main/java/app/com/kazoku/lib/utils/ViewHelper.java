package app.com.kazoku.lib.utils;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

import static android.view.View.NO_ID;

public class ViewHelper {
    public static class ViewBuilder{
        View pView;
        public ViewBuilder(View parrentView){
            pView = parrentView;
        }

        public ViewBuilder sText(int viewId,String text){
            if(TextUtils.isEmpty(text)) {
                text = "";
            }

            TextView tv = pView.findViewById(viewId);
            if(tv != null){
                tv.setText(text);
            }
            return this;
        }


        public ViewBuilder sTextDate(int viewId, Date date, String pattern){
            if(date == null) return this;

            TextView tv = pView.findViewById(viewId);
            if(tv != null){
               SimpleDateFormat fmt = new SimpleDateFormat(pattern);
                tv.setText(fmt.format(date));
            }
            return this;
        }

        public ViewBuilder bindView(View childView,int viewId){
//            childView = pView.findViewById(viewId);
            childView = fV(pView,viewId);
            return this;
        }
        public ViewBuilder sText(int viewId,int textId){
            if(textId <= 0) return this;

            TextView tv = pView.findViewById(viewId);
            if(tv != null){
                tv.setText(textId);
            }
            return this;
        }
        public ViewBuilder sTextAndColor(int viewId,int textId,int color){
            if(textId <= 0) return this;

            TextView tv = pView.findViewById(viewId);
            if(tv != null){
                tv.setTextColor(color);
                tv.setText(textId);
            }
            return this;
        }
        public ViewBuilder sTextAndColor(int viewId,String text,int color){
            if(TextUtils.isEmpty(text)) return this;

            TextView tv = pView.findViewById(viewId);
            if(tv != null){
                tv.setTextColor(color);
                tv.setText(text);
            }
            return this;
        }
        public ViewBuilder sTextAndDisplay(int viewId,String text){
            if(TextUtils.isEmpty(text)) text = "";

            TextView tv = pView.findViewById(viewId);
            if(tv != null){
                tv.setVisibility(View.VISIBLE);
                tv.setText(text);
            }
            return this;
        }
        public ViewBuilder sTextIfNotEmpty(int viewId,String text){

            TextView tv = pView.findViewById(viewId);
            if(TextUtils.isEmpty(text) || text.contains("null")) {
                if(tv != null) tv.setVisibility(View.GONE);
                return this;
            }

            if(tv != null){
                tv.setText(text);
            }
            return this;
        }
        public ViewBuilder sText(int viewId,SpannableStringBuilder text){
            TextView tv = pView.findViewById(viewId);
            if(tv != null){
                tv.setText(text,TextView.BufferType.SPANNABLE);
            }
            return this;
        }

        public ViewBuilder sLogoUrl(int viewId,String url){
            ImageView iv = pView.findViewById(viewId);
            if(iv != null) {
//                Glide.with(iv.getContext()).load(url).into(iv);
            }
            return this;
        }
        public ViewBuilder loadImage(int viewId,String imageUrl){
            ImageView iv = pView.findViewById(viewId);
            if(iv != null) {
                ViewHelper.loadImage(iv.getContext(),imageUrl,0,iv,true);
            }
            return this;
        }
        public ViewBuilder loadDrawable(int viewId,int drawableRes){
            ImageView iv = pView.findViewById(viewId);
            if(iv != null) {
                iv.setImageResource(drawableRes);
            }
            return this;
        }
        public ViewBuilder goneOrVisible(int viewId,boolean isGone){
            View iv = pView.findViewById(viewId);
            if(iv != null) {
                if(isGone)
                    iv.setVisibility(View.GONE);
                else
                    iv.setVisibility(View.VISIBLE);
            }
            return this;
        }
        public ViewBuilder sTap(int viewId, View.OnClickListener clickListener){
            View v = pView.findViewById(viewId);
            if(v != null){
                v.setOnClickListener(clickListener);
            }
            return this;
        }
    }

    public static <T extends View> T fV(View parent,@IdRes int id) {
        if (id == NO_ID) {
            return null;
        }
        return parent.findViewById(id);
    }

    public static void bindText(int viewId,String text){

    }
    public static void bindImage(View view,int viewId,String url){
        ImageView iv = view.findViewById(viewId);
        if(iv == null) return;
        ImageLoader.loadImage(view.getContext(),iv,url);

    }
    public static ViewBuilder getViewBuilder(View contentView){
        ViewBuilder v = new ViewBuilder(contentView);
        return v;
    }
    public static void loadImage(Context context,
                                 String url,
                                 int idDefault,
                                 ImageView imageView,
                                 boolean isCenterCrop) {
        ImageLoader.loadImage(imageView.getContext(),imageView,url);
    }
    public static View getViewByInflate(Context context,int layoutId){
        LayoutInflater inflater = (LayoutInflater)   context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View content    = inflater.inflate(layoutId,null);
        return content;
    }
    public static View getViewByInflateByChild(Context context,int layoutId,int childId){

        ViewGroup viewGroup = (ViewGroup) getViewByInflate(context,layoutId);
        View v = viewGroup.findViewById(childId);
        viewGroup.removeView(v);

        return v;
    }

    public static void setBackGround(View v,int color){

    }

    public static void sTap(AppCompatActivity activity, int viewId, View.OnClickListener onClickListener){
        View v = activity.findViewById(viewId);
        if(v != null){
            v.setOnClickListener(onClickListener);
        }
    }
//    public static DividerItemDecoration getFlightItemDivider(Context context, int resId){
//        DividerItemDecoration itemDecoration = new
//                DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
//        Drawable dividerDrawable = ContextCompat.getDrawable(context, resId);
//        itemDecoration.setDrawable(dividerDrawable);
//        return itemDecoration;
//
//    }
//    public static void bind(final Activity target){
//        bindViews(target, target.getClass().getDeclaredFields(),target.getWindow().getDecorView());
//    }
//    public static void bindViews(final Object obj, Field[] fields, View rootView){
//        for(final Field field : fields) {
//            Annotation annotation = (Annotation) field.getAnnotation(BindWidget.class);
//            if (annotation != null) {
//                BindWidget bindView = (BindWidget) annotation;
//                int id = bindView.value();
//                View view = rootView.findViewById(id);
//                try {
//                    field.setAccessible(true);
//                    field.set(obj, view);
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
}
