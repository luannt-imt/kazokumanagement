package app.com.kazoku.lib.model.modelConver;

import androidx.annotation.NonNull;

import java.util.List;

public class SchoolName {


    public String name;

    public SchoolName(String name) {
        this.name = name;
    }

    public SchoolName() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }


    @NonNull
    @Override
    public String toString() {
        return  name;
    }
}
