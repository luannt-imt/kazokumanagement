/*
+Lớp cơ bản hiển thị list các item với implement code tối thiểu nhất
- default layout,co header,footer có thể thêm vào
- itemlayoutid
- binditem layout
- pullto refresh
* */
package app.com.kazoku.lib.fragments;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import app.com.kazoku.lib.R;
import app.com.kazoku.lib.adapter.GTBaseComplexAdapter;
import app.com.kazoku.lib.adapter.GTBaseViewHolder;

public abstract class FgBaseRecycleView<E> extends GTFragment {
    static String RECYCLEVIEW_TAG = "recycleView";

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    SimpleAdapter _adapter;
    public boolean isLoadingData = false;
    public boolean hasLoadMore = true;

    public int pageIndex = 0;
    List<E> _items = new ArrayList<>();
    int itemViewId;
    Integer sPosition = 0;

    View vHeader;
    long selectedPosition = -1;

    SwipeController swipeController = null;

    public interface onDataChanged {
        void onChanged(int pos, int size);
    }

    onDataChanged _listener;


    OnActionClickedListener mCallback;

    public interface OnActionClickedListener {
        public void ActionCallBack(int possion);
    }

    public void createEvent(OnActionClickedListener mCallback) {
        if (this.mCallback == null) {
            this.mCallback = mCallback;
        }

    }

    private void callBack(int possion) {
        mCallback.ActionCallBack(possion);
    }

    public void setDataListener(onDataChanged listener) {
        _listener = listener;
    }

    @Override
    public int onGetViewContentId() {
        return R.layout.fg_base_recycleview;
    }

    @Override
    public void onViewLoaded() {

        swipeRefreshLayout = fV(R.id.swipeRefresh);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    onRefreshData();
                }
            });
        }

        recyclerView = fV(R.id.recycleView);
//        vHeader = fV(R.id.vHeader);
//        displayLookupView(false);

        itemViewId = R.layout.gtd_item_recycle_simple;

        linearLayoutManager = new LinearLayoutManager(getContext());

        if (recyclerView == null) {
            //try findbyTag
            recyclerView = getRootView().findViewWithTag(RECYCLEVIEW_TAG);
        }
        if (recyclerView != null) {
            recyclerView.setLayoutManager(linearLayoutManager);
        }
    }
//    public void displayLookupView(boolean value) {
//        if (vHeader == null) return;
//        if (value) {
//            vHeader.setVisibility(View.VISIBLE);
//        } else {
//            vHeader.setVisibility(View.GONE);
//        }
//    }

    public void onRefreshData() {
        swipeRefreshLayout.setRefreshing(false);
    }

    public void setPullToRefreshEnable(boolean value) {
        if (swipeRefreshLayout == null) return;
        swipeRefreshLayout.setEnabled(value);
    }

    public void setRecyclerView(int viewId) {
        recyclerView = fV(viewId);
        if (recyclerView == null) return;

        recyclerView.setNestedScrollingEnabled(false);

        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    public void setHeightRecylerView() {
        recyclerView.addItemDecoration(new SpaceItemDecoration(10));
    }


    public void setRecyclerViewHorizital(int viewId, Context context, int type) {
        recyclerView = fV(viewId);
        if (recyclerView == null) return;

        recyclerView.setNestedScrollingEnabled(false);

        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

//        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
//        LinearSnapHelper linearSnapHelper = new SnapHelperOneByOne();
//        linearSnapHelper.attachToRecyclerView(recyclerView);


        SnapHelper snapHelper = new PagerSnapHelper();
        if (recyclerView.getOnFlingListener() == null){
            snapHelper.attachToRecyclerView(recyclerView);
        }

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    View centerView = snapHelper.findSnapView(linearLayoutManager);
                    int pos = linearLayoutManager.getPosition(centerView);
                    Log.e("Snapped Item Position:", "" + pos);
                    if (pos < _items.size()) {
                        callBack(pos);
                    }

                }
            }
        });


    }
    public void setRecyclerViewSwipe(int viewId, Context context, onDataChanged onDataChanged) {
        _listener = onDataChanged;
        recyclerView = fV(viewId);
        if (recyclerView == null) return;

        recyclerView.setNestedScrollingEnabled(false);

        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
//        SwipeHelper swipeHelper = new SwipeHelper(context, recyclerView) {
//            @Override
//            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
//                underlayButtons.add(new SwipeHelper.UnderlayButton(
//                        getContext(),
//                        "Delete",
//                        R.drawable.confirmation,
//                        Color.parseColor("#FF3C30"),
//                        new SwipeHelper.UnderlayButtonClickListener() {
//                            @Override
//                            public void onClick(int pos) {
//                                // TODO: onDelete
////                                Toast.makeText(context, "delete", Toast.LENGTH_SHORT).show();
//                               /* _items.remove(pos);
//                                _adapter.notifyItemRangeChanged(pos, _adapter.getItemCount());
//                                notifyDatasetchange();*/
//                                if (_listener != null) _listener.onChanged(pos, _items.size());
//                            }
//                        }
//                ));
//
//            }
//        };

    }

    public void setRecyclerView(int viewId, RecyclerView.LayoutManager layoutManager) {
        recyclerView = fV(viewId);
        if (recyclerView != null) {
            recyclerView.setLayoutManager(layoutManager);
        }
    }

    public void setRecyclerViewCustom(int viewId) {
        recyclerView = fV(viewId);
        ViewGroup.LayoutParams params = recyclerView.getLayoutParams();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        params.height = dpToPx(95) * _items.size();
        params.width = width - dpToPx(40);
        recyclerView.setLayoutParams(params);

        recyclerView.setHasFixedSize(true);

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    //Hàm ocnvert dp to px
    private int dpToPx(int dp) {
        float density = getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }

    public RecyclerView getRecycleView() {
        return recyclerView;
    }


    public void clear() {
        _items.clear();
    }

    public List<E> getItems() {
        if (_items == null) _items = new ArrayList<>();
        return _items;
    }

    public void setUiRecylerview() {
        if (recyclerView == null) return;
        _adapter = new SimpleAdapter(_items);
        recyclerView.setAdapter(_adapter);
        notifyDatasetchange();

    }


    public void setAdapter(RecyclerView.Adapter adapter) {
        if (recyclerView == null) return;
        recyclerView.setAdapter(adapter);
    }

    public void reloadData() {
        if (recyclerView == null) return;
        recyclerView.setVisibility(View.VISIBLE);
        if (_adapter == null) {
            _adapter = new SimpleAdapter(_items);
        }
        recyclerView.setAdapter(_adapter);

        notifyDatasetchange();
        hideLoading();
    }

    public void reloadDataItemRange(int po, int poFist) {
        if (recyclerView == null) return;
        recyclerView.setVisibility(View.VISIBLE);
        if (_adapter == null) {
            _adapter = new SimpleAdapter(_items);
        }
        recyclerView.setAdapter(_adapter);
        recyclerView.scrollToPosition(po);
        if (po > 0) {
            recyclerView.scrollToPosition(po - poFist);
        }

        hideLoading();
    }

    public void reloadData(List<E> items) {
        _items.clear();
        _items.addAll(items);
        reloadData();
    }

    public void reloadDataNew(List<E> items) {

        _items.addAll(items);
        reloadData();
    }

    public void reloadDataRange(int po, int poFirst) {

        reloadDataItemRange(po, poFirst);
    }


    public void setItemViewId(int viewId) {
        itemViewId = viewId;
    }


    public void onBindItemView(GTBaseViewHolder holder, E entity, int pos) {
        if (itemViewId == R.layout.gtd_item_recycle_simple) {
            holder.sText(R.id.tvItemTitleSimple, entity.toString());
        }
    }

    public void onItemViewClick(int pos, E entity) {

    }

    public void onItemViewLoad(int pageSize) {

    }

    public void remove(int pos) {
        _items.remove(pos);
        _adapter.notifyDataSetChanged();
    }

    public void setLoadMoreEnable(boolean value) {
        if (value) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    int totalItemCount = linearLayoutManager.getItemCount();
                    int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (isLoadingData || totalItemCount == 0 || lastVisibleItem == -1) return;
//                    log("total:"+totalItemCount+",lastvisible:"+lastVisibleItem);
                    if (lastVisibleItem == totalItemCount - 1) {
                        onReachEndList();

                        if (hasLoadMore) {
                            getMoreData();
                        } else {

                        }

                    }
                }
            });
        }
    }

    public void onReachEndList() {

    }


    public void setLoadMoreEnableNew(boolean value, int position) {
        sPosition = position;
        if (value) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    if (!isLoadingData) {
                        if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == _items.size() - 1) {
                            //bottom of list!
                            if (hasLoadMore) {
                                getMoreDataNew();
                            } else {

                            }
                        }
                    }
                }
            });

        }
    }

    public void getMoreData() {
        if (!hasLoadMore) return;
        if (isLoadingData) return;

        isLoadingData = true;
        pageIndex++;
    }


    public void getMoreDataNew() {
        if (!hasLoadMore) return;
        if (isLoadingData) return;

        isLoadingData = false;
        sPosition++;
        pageIndex = sPosition;
        onItemViewLoad(pageIndex);
    }

    int pageSize = 20;

    public void addMoreData(List<E> moreData) {
        isLoadingData = false;
        if (moreData.size() < pageSize) {
            log("get more:" + moreData.size() + " flights");

            hasLoadMore = false;
            setLoadMoreEnable(false);
        }

        _adapter.notifyDataSetChanged();
    }

    public void addHeader(View v) {
        ViewGroup vg = fV(R.id.vHeader);
        if (vg != null && v != null) {
            vg.addView(v);
        }
    }

    public void notifyDatasetchange() {
        _adapter.notifyDataSetChanged();
    }

    public void notifyDatasetchangeRemove(int position) {
        _adapter.notifyItemRemoved(position);
        _adapter.notifyItemRangeChanged(position, _items.size());
        _adapter.notifyDataSetChanged();
    }

    public void setCorlor(long possion) {
        selectedPosition = possion;
    }


    public void scrollToPos(int pos) {
        if (recyclerView == null) return;
        recyclerView.setVisibility(View.VISIBLE);
        if (_adapter == null) {
            _adapter = new SimpleAdapter(_items);
        }
        recyclerView.setAdapter(_adapter);

        recyclerView.scrollToPosition(pos);

    }

    public int lnGetcount() {
        int count = 0;
        count = linearLayoutManager.getItemCount();
        return count;
    }

    public int lnLastPosision() {
        int count = 0;
        count = linearLayoutManager.findLastVisibleItemPosition();
        return count;
    }

    class SimpleAdapter extends GTBaseComplexAdapter<E> {

        public SimpleAdapter(List<E> items) {
            super(items);
            int[] ids = {itemViewId};
            this.setArrItemLayoutId(ids);
        }

        @Override
        public void onBindViewHolder(@NonNull GTBaseViewHolder holder, int position) {
            onBindItemView(holder, getItem(position), position);

            holder.itemView.setOnClickListener(view -> {
                onItemViewClick(position, getItem(position));
            });
        }
    }
}
