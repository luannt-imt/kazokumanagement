package com.example.managementkaratedokazoku.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.managementkaratedokazoku.R;
import com.example.managementkaratedokazoku.model.BeltColor;
import com.example.managementkaratedokazoku.model.buoiTapModel;

import java.util.List;

public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.ViewHolder> {
    List<BeltColor> beltColors;
    Context context;

    public ColorAdapter(List<BeltColor> beltColors, Context context) {
        this.beltColors = beltColors;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_color, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final int label = beltColors.get(position).getColor();
        holder.textView.setText("");
        holder.textView.setBackgroundResource(label);
        if (beltColors.get(position).isSelect()) {
            holder.imgSelect.setVisibility(View.VISIBLE);
        } else {
            holder.imgSelect.setVisibility(View.GONE);
        }

        //handling item click event
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < beltColors.size(); i++) {
                    beltColors.get(i).setSelect(false);
                }
                beltColors.get(position).setSelect(true);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return beltColors.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public ImageView imgSelect;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text);
            imgSelect = (ImageView) itemView.findViewById(R.id.imgSelect);
        }
    }
}