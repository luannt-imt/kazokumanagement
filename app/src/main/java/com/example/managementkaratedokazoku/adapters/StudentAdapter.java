package com.example.managementkaratedokazoku.adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.managementkaratedokazoku.R;
import com.example.managementkaratedokazoku.app.Prefs;
import com.example.managementkaratedokazoku.model.BeltColor;
import com.example.managementkaratedokazoku.model.Student;
import com.example.managementkaratedokazoku.model.buoiTapModel;
import com.example.managementkaratedokazoku.model.caModel;
import com.example.managementkaratedokazoku.model.monthModel;
import com.example.managementkaratedokazoku.realm.RealmController;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by ravi on 16/11/17.
 */

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Student> contactList;
    private List<Student> contactListFiltered;
    private ContactsAdapterListener listener;
    private Realm realm;
    boolean tmpCa1 = false;
    boolean tmpCa2 = false;
    boolean tmpCa3 = false;
    private LayoutInflater inflater;
    MonthAdapter monthAdapter;
    List<monthModel> monthModelList;
    List<buoiTapModel> buoiTapModels;
    List<BeltColor> beltColorList;
    DayOfWeekAdapter dayOfWeekAdapter;
    ColorAdapter colorAdapter;

    Bitmap myBitmap;
    Uri picUri;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;




    private final static int ALL_PERMISSIONS_RESULT = 107;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView card;
        public TextView txtName;
        public TextView txtDob;
        public TextView txtPhone;
        public TextView txtAddress;
        public TextView txtNote;
        public TextView txtStudyDay;
        public TextView txtFeePayment;
        public ImageView imageBackground;

        public MyViewHolder(View view) {
            super(view);
            card = (CardView) itemView.findViewById(R.id.card_books);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtDob = (TextView) itemView.findViewById(R.id.txtDob);
            txtPhone = (TextView) itemView.findViewById(R.id.txtPhone);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
            txtNote = (TextView) itemView.findViewById(R.id.txtNote);
            txtStudyDay = (TextView) itemView.findViewById(R.id.txtStudyDay);
            txtFeePayment = (TextView) itemView.findViewById(R.id.txtFeePayment);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(contactListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }


    public StudentAdapter(Context context, List<Student> contactList, ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.contactList = contactList;
        this.contactListFiltered = contactList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_student, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Student student = contactListFiltered.get(position);
        realm = RealmController.getInstance().getRealm();

        // set the title and the snippet
        holder.txtName.setText("Tên: " + student.getName());
        holder.txtDob.setText("Năm sinh: " + student.getDob());
        holder.txtPhone.setText("Số Điện Thoại: " + student.getPhone());
        holder.txtAddress.setText("Địa chỉ: " + student.getAddress());
        holder.txtNote.setText("Ghi chú: " + student.getNote());
        holder.txtStudyDay.setText("Ngày vào học: " + student.getDateStudy());
        String month = "";
        boolean temp = false;
        for (int i = 0; i < student.getMonthModels().size(); i++) {
            if (student.getMonthModels().get(i).isSelect()) {
                if (!temp) {
                    month = student.getMonthModels().get(i).getMonthsName();
                    temp = true;
                } else {
                    month = month + ", " + student.getMonthModels().get(i).getMonthsName();
                }
            }


        }


        holder.txtFeePayment.setText("Học phí đã đóng: " + month);

        // load the background image
        if (student.getImageUrl() != null) {
            Glide.with(context)
                    .load(student.getImageUrl())
                    .fitCenter()
                    .into(holder.imageBackground);
        }


        holder.card.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                RealmResults<Student> results = realm.where(Student.class).findAll();

                                // Get the student title to show it in toast message
                                Student b = results.get(position);

                                // All changes to data must happen in a transaction
                                realm.beginTransaction();

                                // remove single match
                                results.remove(position);
                                realm.commitTransaction();

                                if (results.size() == 0) {
                                    Prefs.with(context).setPreLoad(false);
                                }

                                removeAt(position);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                dialog.dismiss();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("HLV có muốn xóa võ sinh " + student.getName() + "?").setPositiveButton("Xóa", dialogClickListener)
                        .setNegativeButton("Không", dialogClickListener).show();


//                Toast.makeText(context, title + " is removed from Realm", Toast.LENGTH_SHORT).show();
                return false;
            }
        });




        //update single match from realm
        holder.card.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                tmpCa1 = student.getCaModels().get(0).isSelect();
                tmpCa2 = student.getCaModels().get(1).isSelect();
                tmpCa3 = student.getCaModels().get(2).isSelect();
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View content = inflater.inflate(R.layout.edit_item, null);
                int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.90);
                int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.90);

                final TextInputEditText name_edit_text = (TextInputEditText) content.findViewById(R.id.name_edit_text);
                final TextInputEditText dob_edit_text = (TextInputEditText) content.findViewById(R.id.dob_edit_text);
                final TextInputEditText phone_edit_text = (TextInputEditText) content.findViewById(R.id.phone_edit_text);
                final TextInputEditText address_edit_text = (TextInputEditText) content.findViewById(R.id.address_edit_text);
                final TextInputEditText note_edit_text = (TextInputEditText) content.findViewById(R.id.note_edit_text);
                final Button btnCa1 = (Button) content.findViewById(R.id.btnCa1);
                final Button btnCa2 = (Button) content.findViewById(R.id.btnCa2);
                final Button btnCa3 = (Button) content.findViewById(R.id.btnCa3);
                final Button btnCamera = (Button) content.findViewById(R.id.btnCamera);
                btnCamera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        pickFromGallery();
                    }
                });
                btnCa1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        tmpCa1 = !tmpCa1;
                        v.setBackgroundResource(tmpCa1 ? R.drawable.my_button_bg_select : R.drawable.my_button_bg);

                    }
                });
                btnCa2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tmpCa2 = !tmpCa2;
                        v.setBackgroundResource(tmpCa2 ? R.drawable.my_button_bg_select : R.drawable.my_button_bg);

                    }
                });

                btnCa3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tmpCa3 = !tmpCa3;
                        v.setBackgroundResource(tmpCa3 ? R.drawable.my_button_bg_select : R.drawable.my_button_bg);

                    }
                });

                name_edit_text.setText(student.getName());
                dob_edit_text.setText(student.getDob());
                phone_edit_text.setText(student.getPhone());
                address_edit_text.setText(student.getAddress());
                note_edit_text.setText(student.getNote());


                RecyclerView recyclerView = (RecyclerView) content.findViewById(R.id.recycler_view);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new GridLayoutManager(context, 6));
                monthModelList = new ArrayList<>();

                RecyclerView recyclerView2 = (RecyclerView) content.findViewById(R.id.recycler_view_dayOfWeek);
                recyclerView2.setHasFixedSize(true);
                recyclerView2.setLayoutManager(new GridLayoutManager(context, 7));
                buoiTapModels = new ArrayList<>();

                RecyclerView recyclerViewColor = (RecyclerView) content.findViewById(R.id.recycler_view_color);
                recyclerViewColor.setHasFixedSize(true);
                recyclerViewColor.setLayoutManager(new GridLayoutManager(context, 5));
                beltColorList = new ArrayList<>();

                for (int i = 0; i < student.getMonthModels().size(); i++) {
                    monthModel model = new monthModel();
                    model.setMonthsName(student.getMonthModels().get(i).getMonthsName());
                    model.setSelect(student.getMonthModels().get(i).isSelect());
                    monthModelList.add(model);
                }


                monthAdapter = new MonthAdapter(monthModelList);
                recyclerView.setAdapter(monthAdapter);


                for (int i = 0; i < student.getBuoiTapModels().size(); i++) {
                    buoiTapModel model = new buoiTapModel();
                    model.setDayOfWeek(student.getBuoiTapModels().get(i).getDayOfWeek());
                    model.setSelect(student.getBuoiTapModels().get(i).isSelect());
                    buoiTapModels.add(model);
                }

                dayOfWeekAdapter = new DayOfWeekAdapter(buoiTapModels);
                recyclerView2.setAdapter(dayOfWeekAdapter);

                for (int i = 0; i < student.getBeltColorsModel().size(); i++) {
                    BeltColor model = new BeltColor();
                    model.setColor(student.getBeltColorsModel().get(i).getColor());
                    model.setSelect(student.getBeltColorsModel().get(i).isSelect());
                    beltColorList.add(model);
                }

                colorAdapter = new ColorAdapter(beltColorList,context);
                recyclerViewColor.setAdapter(colorAdapter);


                for (int i = 0; i < student.getCaModels().size(); i++) {
                    if (student.getCaModels().get(i).isSelect()) {
                        if (student.getCaModels().get(i).getCaHoc().equals("1")) {
                            btnCa1.setBackgroundResource(R.drawable.my_button_bg_select);
                        }
                        if (student.getCaModels().get(i).getCaHoc().equals("2")) {
                            btnCa2.setBackgroundResource(R.drawable.my_button_bg_select);

                        }
                        if (student.getCaModels().get(i).getCaHoc().equals("3")) {
                            btnCa3.setBackgroundResource(R.drawable.my_button_bg_select);

                        }
                    } else {
                        if (student.getCaModels().get(i).getCaHoc().equals("1")) {
                            btnCa1.setBackgroundResource(R.drawable.my_button_bg);

                        }
                        if (student.getCaModels().get(i).getCaHoc().equals("2")) {
                            btnCa2.setBackgroundResource(R.drawable.my_button_bg);
                        }
                        if (student.getCaModels().get(i).getCaHoc().equals("3")) {
                            btnCa3.setBackgroundResource(R.drawable.my_button_bg);
                        }

                    }

                }


                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setView(content)
                        .setTitle("Edit Student")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                RealmResults<Student> results = realm.where(Student.class).findAll();

                                realm.beginTransaction();
                                results.get(position).setName(name_edit_text.getText().toString());
                                results.get(position).setDob(dob_edit_text.getText().toString());
                                results.get(position).setPhone(phone_edit_text.getText().toString());
                                results.get(position).setAddress(address_edit_text.getText().toString());
                                results.get(position).setNote(note_edit_text.getText().toString());


                                RealmList<monthModel> monthModelRealmList = new RealmList();
                                for (int i = 0; i < monthModelList.size(); i++) {
                                    monthModel model = realm.createObject(monthModel.class);
                                    model.setSelect(monthModelList.get(i).isSelect());
                                    model.setMonthsName(monthModelList.get(i).getMonthsName());
                                    monthModelRealmList.add(model);
                                }

                                RealmList buoiTapModelRealmList = new RealmList<buoiTapModel>();
                                for (int i = 0; i < buoiTapModels.size(); i++) {
                                    buoiTapModel model = realm.createObject(buoiTapModel.class);
                                    model.setSelect((buoiTapModels.get(i).isSelect()));
                                    model.setDayOfWeek(buoiTapModels.get(i).getDayOfWeek());
                                    buoiTapModelRealmList.add(model);

                                }
                                RealmList beltColorsListRealmList = new RealmList<BeltColor>();
                                for (int i = 0; i < beltColorList.size(); i++) {
                                    BeltColor model = realm.createObject(BeltColor.class);
                                    model.setSelect((beltColorList.get(i).isSelect()));
                                    model.setColor(beltColorList.get(i).getColor());
                                    beltColorsListRealmList.add(model);

                                }


                                RealmList listCa = new RealmList<caModel>();
                                if (tmpCa1) {
                                    caModel ca = realm.createObject(caModel.class);
                                    ca.setCaHoc("1");
                                    ca.setSelect(true);
                                    listCa.add(ca);
                                } else {
                                    caModel ca = realm.createObject(caModel.class);
                                    ca.setCaHoc("1");
                                    ca.setSelect(false);
                                    listCa.add(ca);
                                }

                                if (tmpCa2) {
                                    caModel ca = realm.createObject(caModel.class);
                                    ca.setCaHoc("2");
                                    ca.setSelect(true);
                                    listCa.add(ca);
                                } else {
                                    caModel ca = realm.createObject(caModel.class);
                                    ca.setCaHoc("2");
                                    ca.setSelect(false);
                                    listCa.add(ca);
                                }
                                if (tmpCa3) {
                                    caModel ca = realm.createObject(caModel.class);
                                    ca.setCaHoc("3");
                                    ca.setSelect(true);
                                    listCa.add(ca);
                                } else {
                                    caModel ca = realm.createObject(caModel.class);
                                    ca.setCaHoc("3");
                                    ca.setSelect(false);
                                    listCa.add(ca);
                                }


                                results.get(position).setMonthModels(monthModelRealmList);
                                results.get(position).setBuoiTapModels(buoiTapModelRealmList);
                                results.get(position).setCaModels(listCa);
                                results.get(position).setBeltColorsModel(beltColorsListRealmList);

                                realm.commitTransaction();

                                refreshData();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialog = builder.create();

                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            }
        });

    }

    private void pickFromGallery() {
        try {
            PackageManager pm = context.getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, context.getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            ((Activity) context).startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            ((Activity) context).startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(context, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(context, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void refreshData() {
        contactListFiltered = getModelList();
        notifyDataSetChanged();
    }
    public List<Student> getModelList() {
        List<Student> list = new ArrayList<>();

        try {
            realm = Realm.getDefaultInstance();
            RealmResults<Student> results = realm
                    .where(Student.class)
                    .findAll();
            list.addAll(realm.copyFromRealm(results));
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return list;
    }
    private void removeAt(int position) {
        contactListFiltered.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, contactListFiltered.size());
    }

    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = contactList;
                } else {
                    List<Student> filteredList = new ArrayList<>();
                    for (Student row : contactList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getPhone().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<Student>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected(Student contact);
    }
}
