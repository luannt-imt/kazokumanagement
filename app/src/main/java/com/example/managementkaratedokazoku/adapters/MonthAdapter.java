package com.example.managementkaratedokazoku.adapters;

 import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

 import com.example.managementkaratedokazoku.R;
 import com.example.managementkaratedokazoku.model.monthModel;

 import java.util.List;

public class MonthAdapter extends RecyclerView.Adapter<MonthAdapter.ViewHolder> {
     List<monthModel> monthModelList;
    public MonthAdapter(List<monthModel> monthModelList) {
       this.monthModelList = monthModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_button, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String label = monthModelList.get(position).getMonthsName();
        holder.textView.setText(label);
        if (monthModelList.get(position).isSelect()){
            holder.textView.setBackgroundResource(R.drawable.my_button_bg_select);
        }else {
            holder.textView.setBackgroundResource(R.drawable.my_button_bg);
        }
        //handling item click event
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!monthModelList.get(position).isSelect()){
                    monthModelList.get(position).setSelect(true);
                    notifyDataSetChanged();
                }else {
                    monthModelList.get(position).setSelect(false);
                    notifyDataSetChanged();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return monthModelList.size();
    }
    public  List<monthModel> getItem(){
        return monthModelList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text);
        }
    }
}