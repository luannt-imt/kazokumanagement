package com.example.managementkaratedokazoku.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.managementkaratedokazoku.R;
import com.example.managementkaratedokazoku.model.buoiTapModel;
import com.example.managementkaratedokazoku.model.monthModel;

import java.util.List;

public class DayOfWeekAdapter extends RecyclerView.Adapter<DayOfWeekAdapter.ViewHolder> {
     List<buoiTapModel> buoiTapModels;
    public DayOfWeekAdapter(List<buoiTapModel> monthModelList) {
       this.buoiTapModels = monthModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_button, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String label = buoiTapModels.get(position).getDayOfWeek();
        holder.textView.setText(label);
        if (buoiTapModels.get(position).isSelect()){
            holder.textView.setBackgroundResource(R.drawable.my_button_bg_select);
        }else {
            holder.textView.setBackgroundResource(R.drawable.my_button_bg);
        }
        //handling item click event
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!buoiTapModels.get(position).isSelect()){
                    buoiTapModels.get(position).setSelect(true);
                    notifyDataSetChanged();
                }else {
                    buoiTapModels.get(position).setSelect(false);
                    notifyDataSetChanged();
                }
                Toast.makeText(holder.textView.getContext(), label, Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return buoiTapModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text);
        }
    }
}