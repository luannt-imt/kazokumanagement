package com.example.managementkaratedokazoku;

import android.app.SearchManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import com.example.managementkaratedokazoku.adapters.StudentAdapter;
import com.example.managementkaratedokazoku.adapters.MyDividerItemDecoration;
import com.example.managementkaratedokazoku.model.Student;
import com.example.managementkaratedokazoku.realm.RealmController;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

import static android.Manifest.permission.CAMERA;

public class ListStudentActivity extends AppCompatActivity implements StudentAdapter.ContactsAdapterListener {
    private RecyclerView recycler;
    private Realm realm;
    private SearchView searchView;
    StudentAdapter studentAdapter;

    List<Student> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_student);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recycler = (RecyclerView) findViewById(R.id.recycler);


        this.realm = RealmController.with(this).getRealm();
        setupRecycler();
        // refresh the realm instance
        RealmController.with(this).refresh();
        // get all persisted objects
        // create the helper adapter and notify data set changes
        // changes will be reflected automatically
        setRealmAdapter(RealmController.with(this).getBooks());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    public void setRealmAdapter(RealmResults<Student> students) {

//        RealmBooksAdapter realmAdapter = new RealmBooksAdapter(this.getApplicationContext(), students, true);
        // Set the data and tell the RecyclerView to draw
//        adapter.setRealmAdapter(realmAdapter);
//        adapter.notifyDataSetChanged();
    }

    private void setupRecycler() {


        recycler.setHasFixedSize(true);
        studentAdapter = new StudentAdapter(this, getModelList(), this);

        // white background notification bar

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler.setLayoutManager(mLayoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 36));
        recycler.setAdapter(studentAdapter);
      /*   final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);


        studentAdapter = new StudentAdapter(this, getModelList(), this);
        recycler.setAdapter(adapter);*/
    }

    public List<Student> getModelList() {
          list = new ArrayList<>();

        try {
            realm = Realm.getDefaultInstance();
            RealmResults<Student> results = realm
                    .where(Student.class)
                    .findAll();
            list.addAll(realm.copyFromRealm(results));
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        convert(list);
        return list;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_attendance:

                break;

        }
        return true;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                studentAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                studentAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;


    }

    @Override
    public void onContactSelected(Student contact) {

    }

    public void convert(List<Student> list){
        for (int i = 0; i< list.size(); i++){
            for (int j = 0; j< list.get(i).getBuoiTapModels().size(); j++){
//                if (list.get(i).getBuoiTapModels().get(j).getDayOfWeek()){
//
//                }
            }
        }
    }
}
