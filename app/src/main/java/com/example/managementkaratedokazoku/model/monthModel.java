package com.example.managementkaratedokazoku.model;

import io.realm.RealmObject;

public class monthModel extends RealmObject {
    private String monthsName;
    private boolean select;

    public String getMonthsName() {
        return monthsName;
    }

    public void setMonthsName(String monthsName) {
        this.monthsName = monthsName;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}
