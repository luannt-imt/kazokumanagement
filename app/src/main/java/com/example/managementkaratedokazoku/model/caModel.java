package com.example.managementkaratedokazoku.model;

import io.realm.RealmObject;

public class caModel extends RealmObject {
    private boolean select;

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    private String caHoc;

    public String getCaHoc() {
        return caHoc;
    }

    public void setCaHoc(String caHoc) {
        this.caHoc = caHoc;
    }
}
