package com.example.managementkaratedokazoku;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements RecyclerViewAdapter.ItemListener {
    RecyclerView recyclerView;
    ArrayList<DataModel> arrayList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        arrayList = new ArrayList<>();
        arrayList.add(new DataModel("Ghi Danh", R.drawable.img1));
        arrayList.add(new DataModel("Danh Sách", R.drawable.img2));
        arrayList.add(new DataModel("Order Võ Phục", R.drawable.img3));
        arrayList.add(new DataModel("Item 4", R.drawable.img4));
        arrayList.add(new DataModel("Item 5", R.drawable.img5));
        arrayList.add(new DataModel("Item 6", R.drawable.img6));

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, arrayList, this);
        recyclerView.setAdapter(adapter);
        /**
         Simple GridLayoutManager that spans two columns
         **/
        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);

    }

    @Override
    public void onItemClick(DataModel item) {
        Toast.makeText(getApplicationContext(), item.text + " is clicked", Toast.LENGTH_SHORT).show();
        Intent intent;
        switch (item.text) {

            case "Ghi Danh":
                intent = new Intent(this, RegisterStudentActivity.class);
                startActivity(intent);
                break;
            case "Danh Sách":
                intent = new Intent(this, ListStudentActivity.class);
                startActivity(intent);
                break;
            default:

                break;
        }


    }
}
