package com.example.managementkaratedokazoku;

import android.app.ProgressDialog;
import android.os.Bundle;


import com.example.managementkaratedokazoku.adapters.ColorAdapter;
import com.example.managementkaratedokazoku.adapters.DayOfWeekAdapter;
import com.example.managementkaratedokazoku.adapters.MonthAdapter;
import com.example.managementkaratedokazoku.model.BeltColor;
import com.example.managementkaratedokazoku.model.Student;
import com.example.managementkaratedokazoku.model.buoiTapModel;
import com.example.managementkaratedokazoku.model.caModel;
import com.example.managementkaratedokazoku.model.monthModel;
import com.example.managementkaratedokazoku.realm.RealmController;
import com.google.android.material.textfield.TextInputEditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmList;

public class RegisterStudentActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnCa1, btnCa2, btnCa3, btnRegister;

    boolean tmpCa1 = false;
    boolean tmpCa2 = false;
    boolean tmpCa3 = false;
    MonthAdapter monthAdapter;
    DayOfWeekAdapter dayOfWeekAdapter;
    ColorAdapter colorAdapter;
    TextInputEditText name_edit_text, dob_edit_text, phone_edit_text, address_edit_text, note_edit_text;
    ProgressDialog progressDialog;
    private Realm realm;
    List<monthModel> monthModelList;
    List<buoiTapModel> buoiTapModels;
    List<BeltColor> beltColors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_student);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                onBackPressed();
            }
        });
        this.realm = RealmController.with(this).getRealm();


        btnCa1 = findViewById(R.id.btnCa1);
        btnCa2 = findViewById(R.id.btnCa2);
        btnCa3 = findViewById(R.id.btnCa3);


        btnRegister = findViewById(R.id.btnRegister);
        name_edit_text = findViewById(R.id.name_edit_text);
        dob_edit_text = findViewById(R.id.dob_edit_text);
        phone_edit_text = findViewById(R.id.phone_edit_text);
        address_edit_text = findViewById(R.id.address_edit_text);
        note_edit_text = findViewById(R.id.note_edit_text);

        btnCa1.setOnClickListener(this);
        btnCa2.setOnClickListener(this);
        btnCa3.setOnClickListener(this);

        btnRegister.setOnClickListener(this);


        setFee();
        setDayOfWeek();
        setBeltColor();

    }

    private void setBeltColor() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view_color);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 5));
        beltColors = new ArrayList<>();

        beltColors = BeltColor.getDemo();
        colorAdapter = new ColorAdapter(beltColors,this);
        recyclerView.setAdapter(colorAdapter);
    }

    private void setDayOfWeek() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view_dayOfWeek);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 7));
        buoiTapModels = new ArrayList<>();

        for (int i = 2; i < 9; i++) {
            buoiTapModel model = new buoiTapModel();
            if (i == 8) {
                model.setDayOfWeek("CN");
            } else {
                model.setDayOfWeek("T" + i);
            }
            model.setSelect(false);
            buoiTapModels.add(model);
        }
        dayOfWeekAdapter = new DayOfWeekAdapter(buoiTapModels);
        recyclerView.setAdapter(dayOfWeekAdapter);
    }

    private void setFee() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 6));
        monthModelList = new ArrayList<>();

        for (int i = 1; i < 13; i++) {
            monthModel model = new monthModel();
            model.setMonthsName("T" + i);
            model.setSelect(false);
            monthModelList.add(model);
        }
        monthAdapter = new MonthAdapter(monthModelList);
        recyclerView.setAdapter(monthAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnCa1:
                tmpCa1 = !tmpCa1;
                v.setBackgroundResource(tmpCa1 ? R.drawable.my_button_bg_select : R.drawable.my_button_bg);
                break;
            case R.id.btnCa2:
                tmpCa2 = !tmpCa2;
                v.setBackgroundResource(tmpCa2 ? R.drawable.my_button_bg_select : R.drawable.my_button_bg);
                break;
            case R.id.btnCa3:
                tmpCa3 = !tmpCa3;
                v.setBackgroundResource(tmpCa3 ? R.drawable.my_button_bg_select : R.drawable.my_button_bg);
                break;


            case R.id.btnRegister:

                if (validate()) {
                    showDialog();
                    Student student = new Student();

                    try {
                        student.setId(RealmController.getInstance().getBooks().size() + System.currentTimeMillis());
                        student.setName(name_edit_text.getText().toString().trim());
                        student.setDob(dob_edit_text.getText().toString().trim());
                        student.setPhone(phone_edit_text.getText().toString().trim());
                        student.setAddress(address_edit_text.getText().toString().trim());
                        student.setNote(note_edit_text.getText().toString().trim());
                        String currentDate = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault()).format(new Date());

                        student.setDateStudy(currentDate);


                        RealmList<monthModel> realmIntRealmList = new RealmList<>();

                        for (int i = 0; i < monthModelList.size(); i++) {
                            monthModel realmInt = new monthModel();

                            realmInt.setSelect((monthModelList.get(i).isSelect()));
                            realmInt.setMonthsName(monthModelList.get(i).getMonthsName());
                            realmIntRealmList.add(realmInt);
                        }
                        RealmList list = new RealmList<buoiTapModel>();
                        for (int i = 0; i < buoiTapModels.size(); i++) {
                            buoiTapModel realmInt = new buoiTapModel();
                            realmInt.setSelect((buoiTapModels.get(i).isSelect()));
                            realmInt.setDayOfWeek(buoiTapModels.get(i).getDayOfWeek());
                            list.add(realmInt);

                        }
                         RealmList beltColorsRealmList = new RealmList<BeltColor>();

                        for (int i = 0; i < beltColors.size(); i++) {
                            BeltColor beltColor = new BeltColor();
                            beltColor.setSelect(beltColors.get(i).isSelect());
                            beltColor.setColor(beltColors.get(i).getColor());

                            beltColorsRealmList.add(beltColor);

                        }
                        RealmList listCa = new RealmList<caModel>();
                        if (tmpCa1) {
                            caModel ca = new caModel();
                            ca.setCaHoc("1");
                            ca.setSelect(true);
                            listCa.add(ca);
                        } else {
                            caModel ca = new caModel();
                            ca.setCaHoc("1");
                            ca.setSelect(false);
                            listCa.add(ca);
                        }

                        if (tmpCa2) {
                            caModel ca = new caModel();
                            ca.setCaHoc("2");
                            ca.setSelect(true);
                            listCa.add(ca);
                        } else {
                            caModel ca = new caModel();
                            ca.setCaHoc("2");
                            ca.setSelect(false);
                            listCa.add(ca);
                        }
                        if (tmpCa3) {
                            caModel ca = new caModel();
                            ca.setCaHoc("3");
                            ca.setSelect(true);
                            listCa.add(ca);
                        } else {
                            caModel ca = new caModel();
                            ca.setCaHoc("3");
                            ca.setSelect(false);
                            listCa.add(ca);
                        }

                        student.setMonthModels(realmIntRealmList);
                        student.setCaModels(listCa);
                        student.setBuoiTapModels(list);
                        student.setBeltColorsModel(beltColorsRealmList);

                        realm.beginTransaction();
                        realm.copyToRealm(student);
                        realm.commitTransaction();
                        hideDialog();
                        Toast.makeText(this, "Thành công", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                 /*   runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (!isFinishing()){
                                new AlertDialog.Builder(RegisterStudentActivity.this)
                                        .setTitle("Chúc mừng")
                                        .setMessage("Đã đăng ký võ sinh thành công")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        }).show();
                            }
                        }
                    });*/

                } else {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            if (!isFinishing()) {
//                                new AlertDialog.Builder(RegisterStudentActivity.this)
//                                        .setTitle("Thật tiếc")
//                                        .setMessage("Tên và SDT liên hệ không được trống")
//                                        .setCancelable(false)
//                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//
//                                            }
//                                        }).show();
//                            }
//                        }
//                    });
                }
                break;
        }
    }

    private boolean validate() {
        boolean stastus = true;
        if (TextUtils.isEmpty(name_edit_text.getText().toString().trim())) {
            stastus = false;
            name_edit_text.setError("Tên không được rỗng");
        }
        if (TextUtils.isEmpty(phone_edit_text.getText().toString().trim()) || phone_edit_text.getText().toString().length() < 10 || phone_edit_text.getText().toString().length() > 13) {
            stastus = false;
            phone_edit_text.setError("SĐT không hợp lệ");
        }
        int tempMonth = 0;
        int tempbthap = 0;
        int tempColor = 0;
        for (int i = 0; i < monthModelList.size(); i++) {
            if (!monthModelList.get(i).isSelect()) {
                tempMonth++;

//                Toast.makeText(this, "Vui lòng chọn tháng học", Toast.LENGTH_SHORT).show();
            }
        }
        for (int i = 0; i < buoiTapModels.size(); i++) {
            if (!buoiTapModels.get(i).isSelect()) {
                tempbthap++;
//                stastus = true;
//                Toast.makeText(this, "Vui lòng chọn buổi tập", Toast.LENGTH_SHORT).show();

            }
        }
        for (int i = 0; i< beltColors.size();i++){
            if (!beltColors.get(i).isSelect()){
                tempColor++;
            }
        }
        if (tempMonth == 12) {
            stastus = false;
            Toast.makeText(this, "Vui lòng chọn tháng học", Toast.LENGTH_SHORT).show();
        }
        if (tempbthap == 7) {
            stastus = false;
            Toast.makeText(this, "Vui lòng chọn buổi tập", Toast.LENGTH_SHORT).show();
        }
        if (tempColor == 10) {
            stastus = false;
            Toast.makeText(this, "Vui lòng chọn màu đai", Toast.LENGTH_SHORT).show();

        }
        if (!tmpCa1 && !tmpCa2 && !tmpCa3) {
            stastus = false;
            Toast.makeText(this, "Vui lòng chọn ca tập", Toast.LENGTH_SHORT).show();
        }

        return stastus;
    }

    public void showDialog() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Chờ tí nhé!....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    public void hideDialog() {
        progressDialog.dismiss();
    }
}
