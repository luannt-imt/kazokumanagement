package app.com.kazoku.adapter

 import android.content.Context
 import android.view.LayoutInflater
 import android.view.View
 import android.view.ViewGroup
 import android.webkit.WebChromeClient
 import android.webkit.WebView
 import android.widget.TextView
 import androidx.recyclerview.widget.RecyclerView
 import app.com.kazoku.models.UploadVideoModel
 import com.daimajia.swipe.SwipeLayout
 import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
 import com.google.firebase.database.*
 import java.util.*
 import app.com.kazoku.R

/**
 * Created by Tofiq Quadri on 27-03-2017.
 */

class VideoAdapter(private val mContext: Context) : RecyclerSwipeAdapter<VideoAdapter.VideoViewHolder>() {
    override fun getSwipeLayoutResourceId(position: Int): Int {

        return R.id.swipe
    }

    internal var databaseReference = FirebaseDatabase.getInstance().reference
    private var userList = ArrayList<UploadVideoModel>()
    var datasnap: DataSnapshot? = null

    fun setData(users: List<UploadVideoModel>) {
        userList.clear()
        userList.addAll(users)
        notifyDataSetChanged()


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.card_video, parent, false)



        return VideoViewHolder(view)
    }

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        val user = userList[position]

        holder.videoWeb.loadData(userList!![position].url, "text/html", "utf-8")
        holder.baiTap.text = "Bài tập : " + userList[position].title
        holder.mauDai.text = "Cấp Đai : " + userList[position].mauDai

        holder.tvDelete.setOnClickListener { v ->
            databaseReference.child("link").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    for (snapshot in dataSnapshot.children) {
                        val userTemp = snapshot.getValue(UploadVideoModel::class.java)
                        if (user.title == userTemp!!.title) {
                            databaseReference.child("link").child(snapshot.key.toString()).removeValue()
                            userList.removeAt(position)
                            notifyDataSetChanged()
                            if (userList.size == 0) {

                                //MainActivity.textViewEmptyView.setVisibility(View.VISIBLE);
                            }
                            break

                        }

                    }

                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            })
        }


    }


    override fun getItemCount(): Int {
        return userList.size
    }

    inner class VideoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var videoWeb: WebView
        internal var mauDai: TextView
        internal var baiTap: TextView
        internal var swipeLayout: SwipeLayout
        internal var tvEdit: TextView
        internal var tvDelete: TextView

        init {

            videoWeb = itemView.findViewById<View>(R.id.webVideoView) as WebView
            mauDai = itemView.findViewById<View>(R.id.txtMaudai) as TextView
            baiTap = itemView.findViewById<View>(R.id.txtbaitap) as TextView
            swipeLayout = itemView.findViewById<View>(R.id.swipe) as SwipeLayout
            tvEdit = itemView.findViewById<View>(R.id.tvEdit) as TextView
            tvDelete = itemView.findViewById<View>(R.id.tvDelete) as TextView

            videoWeb.settings.javaScriptEnabled = true
            videoWeb.webChromeClient = object : WebChromeClient() {


            }
        }
    }

    fun setOnItemClickListener(clickListener: VideoAdapter.ClickListener) {
        VideoAdapter.clickListener = clickListener
    }

    interface ClickListener {
        fun OnItemClick(user: UploadVideoModel, datasnap: DataSnapshot)
        fun OnItemClickChooseImages()
    }

    companion object {
        var clickListener: ClickListener? = null

    }
}


