package app.com.kazoku

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import app.com.kazoku.fragments.FgHome
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.activities.realm.RealmController
import app.com.kazoku.lib.model.*
import app.com.kazoku.lib.model.modelConver.deleteModelN
import app.com.kazoku.lib.model.modelConver.deleteModelReaml
import app.com.kazoku.models.monneyModel
import app.com.kazoku.ui.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.fg_list_student.*
import kotlinx.android.synthetic.main.home1.*
import kotlinx.android.synthetic.main.home_logout.*
import kotlinx.android.synthetic.main.home_register.*
import kotlinx.android.synthetic.main.home_sysc_data.*
import kotlinx.android.synthetic.main.management.*
import kotlinx.android.synthetic.main.order.*
import java.util.*




class MainActivity : GTActivity(),  ChildEventListener {
    override fun onCancelled(error: DatabaseError) {

    }

    override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
     }

    override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
     }

    override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
        println("keeeeeeeeeêyyyyyyyyyyyyyy: "+snapshot.key)


        listMonney.add(snapshot!!.getValue(monneyModel::class.java)!!)
        println(listMonney.size)
     }

    override fun onChildRemoved(snapshot: DataSnapshot) {
     }
    private val sharedPrefFile = "kotlinsharedpreference"

    private val PICK_IMAGE_CAMERA = 1
    private val PICK_IMAGE_GALLERY = 2
    private var realm: Realm? = null
    private lateinit var databaseReference: DatabaseReference
    private lateinit var dbChild: DatabaseReference
    var listMonneyRealm = ArrayList<monneyModelReaml>()
    var listStudentDeleteRealm = ArrayList<deleteModelReaml>()
    private val listMonney = ArrayList<monneyModel>()
    private val listStudentDelete = ArrayList<deleteModelN>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setRootFragment(FgHome())
        setContentView(R.layout.home1)
        this.realm = RealmController.with(this).realm
        databaseReference = FirebaseDatabase.getInstance().reference

        if (LoginActivity.instance.nameUser!!.contains("wakai")){
            imageView.setImageResource(R.drawable.logo_wakai)
        }

        lnRegister.setOnClickListener {
            //            Toast.makeText(context, "Hi there! This is a Toast.", Toast.LENGTH_LONG).show()

            showActivity(FgRegister::class.java)
        }
        lnListStudent.setOnClickListener {

            showActivity(FgListStudent::class.java)
            //replaceFragmentAnimationTopToBottom(FgListStudent())
        }
        lnSyscData.setOnClickListener {
            //replaceFragmentAnimationTopToBottom(FgSyscData())
            showActivity(FgSyscData::class.java)
            // addFragment(FgSyscData())
        }
        lnManagement.setOnClickListener {
            if (LoginActivity.instance.nameUser == "admin"){
                showActivity(FgManagement::class.java)
            }else{
                showActivity(ChangePassActivity::class.java)
               // finish()
               // Toast.makeText(this, "Bạn không có quyền truy cập",Toast.LENGTH_LONG).show()
            }

        }
        lnUploadVideo.setOnClickListener {
            showActivity(FgUploadVideo::class.java)
        }
        lnLogout.setOnClickListener {
            dialog("Bạn đăng xuất sẽ mất dữ liệu. Vui lòng up dữ liệu trước khi Đăng xuất")


        }
        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        val editor:SharedPreferences.Editor =  sharedPreferences.edit()
        listMonneyRealm =getModelLists()
        listStudentDeleteRealm = getStudentDelete()
        if (isOnline(this)){
            if(listMonneyRealm.isEmpty()){
                println("rrrrrrrrrrrrronggg")

                actionYesDownLoad()
                // Toast.makeText(applicationContext,"Rong",Toast.LENGTH_SHORT).show()
            }
            if (listStudentDeleteRealm.isEmpty()){
                actionDowloadStudentDelete()
            }
        }

        val c = Calendar.getInstance()
        val cyear = c.get(Calendar.YEAR)//calender year starts from 1900 so you must add 1900 to the value recevie.i.e., 1990+112 = 2012
        val cmonth = c.get(Calendar.MONTH) + 1
        val cday = c.get(Calendar.DATE)
        if (cday == 1){
            try {
                val sharedIdValue = sharedPreferences.getBoolean("id_key",false)
                if (!sharedIdValue){
                    realm!!.executeTransaction { realm ->
                        realm.clear(monneyModelReaml::class.java)
                        //realm.clear(deleteModelReaml::class.java)
                        editor.putBoolean("id_key",true)
                        editor.apply()
                        editor.commit()
                    }
                }

            } finally {
                //realm?.close()
            }
            //FirebaseDatabase.getInstance().getReference("monney"+LoginActivity.instance.nameUser.toString()).removeValue()
        }else if (cday == 2){
            editor.putBoolean("id_key",false)
            editor.apply()
            editor.commit()
        }
    }
    fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
    private fun actionDowloadStudentDelete() {
        //showLoadingCircle("Tải dữ liệu","Đang tiến hành. Vui lòng chờ trong giây lát..")
        listStudentDelete.clear()
        dbChild = databaseReference.child("delete" + LoginActivity.instance.nameUser)
        dbChild.addChildEventListener(object : ChildEventListener{
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
             }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
             }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                println("keeeeeeeeeêyyyyyyyyyyyyyy: "+snapshot.key)


                listStudentDelete.add(snapshot!!.getValue(deleteModelN::class.java)!!)
                println(listStudentDelete.size)
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
             }

        } )

        databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                try {
                   // println("We're done loading the initial " + dataSnapshot.childrenCount + " items")
                    if (listStudentDelete.size > 0){
                        for (i in listStudentDelete.indices) {
//            var monneyModel = monneyModel(strings[i])
                            var deleteModelReaml = deleteModelReaml()
                            deleteModelReaml.name = listStudentDelete[i].name
                            deleteModelReaml.phone = listStudentDelete[i].phone
                            deleteModelReaml.id = listStudentDelete[i].id!!

//            databaseReference.child("monney" + LoginActivity.instance.nameUser).push().setValue(monneyModel)

                            realm!!.beginTransaction()
                            realm!!.copyToRealm(deleteModelReaml)
                            realm!!.commitTransaction()
                        }

                    }

                    hideLoadingCircle()
                }catch (e: Exception){

                }

                // showActivity(MainActivity::class.java)
                // finish()



            }


        })

    }
    private fun actionYesDownLoad() {
        showLoadingCircle("Tải dữ liệu","Đang tiến hành. Vui lòng chờ trong giây lát..")
        listMonney.clear()



        dbChild = databaseReference.child("monney" + LoginActivity.instance.nameUser)
        dbChild.addChildEventListener(this)

        databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                println("We're done loading the initial " + dataSnapshot.childrenCount + " items")
                if (listMonney.size > 0){
                    for (i in listMonney.indices) {
//            var monneyModel = monneyModel(strings[i])
                        var monneyModelReamls = monneyModelReaml()
                        monneyModelReamls.monney = listMonney[i].monney
                        monneyModelReamls.id = listMonney[i].id!!

//            databaseReference.child("monney" + LoginActivity.instance.nameUser).push().setValue(monneyModel)

                        realm!!.beginTransaction()
                        realm!!.copyToRealm(monneyModelReamls)
                        realm!!.commitTransaction()
                    }

                }

                hideLoadingCircle()
               // showActivity(MainActivity::class.java)
               // finish()



            }


        })
    }

    fun getStudentDelete(): ArrayList<deleteModelReaml> {
        var list = ArrayList<deleteModelReaml>()

        try {
            realm = Realm.getDefaultInstance()
            val results = realm!!.where<deleteModelReaml>(deleteModelReaml::class.java)
                    .findAll()
            list.addAll(realm!!.copyFromRealm<deleteModelReaml>(results))
        } finally {
            if (realm != null) {
                realm!!.close()
            }
        }
        return list
    }
    fun getModelLists(): ArrayList<monneyModelReaml> {
        var list = ArrayList<monneyModelReaml>()

        try {
            realm = Realm.getDefaultInstance()
            val results = realm!!.where<monneyModelReaml>(monneyModelReaml::class.java)
                    .findAll()
            list.addAll(realm!!.copyFromRealm<monneyModelReaml>(results))
        } finally {
            if (realm != null) {
                realm!!.close()
            }
        }
        return list
    }
    private fun actionYes() {
        try {
            realm!!.executeTransaction { realm ->
                realm.clear(Student::class.java)
                realm.clear(monneyModelReaml::class.java)
                realm.clear(deleteModelReaml::class.java)
                finish()

                FirebaseAuth.getInstance().signOut()
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
        } finally {
            //realm?.close()
        }

    }

    private fun dialog(msg: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Đăng Xuất!!")
        builder.setMessage(msg)

        builder.setPositiveButton("Đồng Ý") { dialog, which ->
            actionYes()
        }


        builder.setNegativeButton("Bỏ Qua") { dialog, which ->
            dialog.dismiss()
        }
        builder.show()
    }


}
