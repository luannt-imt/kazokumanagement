package app.com.kazoku.fragments

import android.content.Intent
import android.widget.Toast
import app.com.kazoku.LoginActivity
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.fragments.GTFragment
import app.com.kazoku.ui.FgListStudent
import app.com.kazoku.ui.FgRegister
import app.com.kazoku.ui.FgSyscData
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fg_list_student.*
import kotlinx.android.synthetic.main.home_logout.*
import kotlinx.android.synthetic.main.home_register.*
import kotlinx.android.synthetic.main.home_sysc_data.*

class FgHome : GTFragment() {
    override fun onGetViewContentId(): Int {
        return R.layout.home1
    }

    override fun onViewLoaded() {
        lnRegister.setOnClickListener {
            //            Toast.makeText(context, "Hi there! This is a Toast.", Toast.LENGTH_LONG).show()
           // replaceFragmentAnimationTopToBottom(FgRegister())
        }
        lnListStudent.setOnClickListener {
            //replaceFragmentAnimationTopToBottom(FgListStudent())
        }
        lnSyscData.setOnClickListener {
            //replaceFragmentAnimationTopToBottom(FgSyscData())
        }
        lnLogout.setOnClickListener {
            gtActivity.finish()

            FirebaseAuth.getInstance().signOut()
            val intent = Intent(context, LoginActivity::class.java)
            startActivity(intent)

        }
    }
}