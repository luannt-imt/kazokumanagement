package app.com.kazoku

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.adapter.StudentSchoolAdapter
import app.com.kazoku.lib.model.BeltColor
import app.com.kazoku.lib.model.Student
import app.com.kazoku.lib.model.buoiTapModel
import app.com.kazoku.lib.model.modelConver.StudentConvertN
import app.com.kazoku.lib.model.monthModel
import com.google.firebase.database.*
import io.realm.RealmList
 import kotlinx.android.synthetic.main.content_list_student.recycler
import kotlinx.android.synthetic.main.content_list_student_school.*
import java.util.*

class DetailsSchoolActivity : GTActivity() , ChildEventListener  {


    override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

    }

    override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
     }

    override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
        listStudent.add(snapshot!!.getValue(StudentConvertN::class.java)!!)

        println("aaaaaaaaaaaaaaaaaa: "+listStudent.size)
     }

    override fun onChildRemoved(snapshot: DataSnapshot) {
     }

    override fun onCancelled(error: DatabaseError) {

     }

    private lateinit var toolbar: Toolbar
    internal var databaseReference = FirebaseDatabase.getInstance().reference
    private lateinit var dbChild: DatabaseReference
    var list = ArrayList<Student>()
    //    var listConvert = ArrayList<StudentConvert>()
    private val listStudent = ArrayList<StudentConvertN>()

    private var studentSchoolAdapter: StudentSchoolAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_school)
        val name = intent.getStringExtra("name")
        println("ahihii $name")
        toolbar = findViewById(R.id.toolbar)



        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        supportActionBar!!.title = "DS $name"

        actionDownLoad(name)
    }
    private fun actionDownLoad(name:  String) {
        showLoadingCircle("Tải dữ liệu","Đang tiến hành. Vui lòng chờ trong giây lát..")
        listStudent.clear()



        dbChild = databaseReference.child(name)
        dbChild.addChildEventListener(this)

        databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                println("We're done loading the initial " + dataSnapshot.childrenCount + " items")
                var  students : Student
                for (i in 0 until listStudent.size) {
                    students =   Student()
                    students.id = listStudent[i].id
                    students.imageUrl = listStudent[i].imageUrl
                    students.name = listStudent[i].name
                    students.dob = listStudent[i].dob
                    students.phone = listStudent[i].phone
                    students.address = listStudent[i].address
                    students.note = listStudent[i].note
                    students.dateStudy = listStudent[i].dateStudy
                    val realmMonth= RealmList<monthModel>()

                    for (j in listStudent[i].monthModels!!.indices) {
                        val realmInt = monthModel()

                        realmInt.isSelect =listStudent[i].monthModels[j]!!.isSelect
                        realmInt.monthsName =listStudent[i].monthModels[j]!!.monthsName
                        realmMonth.add(realmInt)
                    }

//                    val realmCa= RealmList<caModel>()
//                    for (j in listStudent[i].caModels!!.indices){
//                        val realmInt = caModel()
//                        realmInt.caHoc = listStudent[i].caModels[j]!!.caHoc
//                        realmInt.isSelect = listStudent[i].caModels[j]!!.isSelect
//                        realmCa.add(realmInt)
//                    }
                    val realmBuoiTap= RealmList<buoiTapModel>()
                    for (j in listStudent[i].buoiTapModels!!.indices){
                        val realmInt = buoiTapModel()
                        realmInt.dayOfWeek = listStudent[i].buoiTapModels[j]!!.dayOfWeek
                        realmInt.isSelect = listStudent[i].buoiTapModels[j]!!.isSelect
                        realmInt.cahoc = listStudent[i].buoiTapModels[j]!!.cahoc
                        realmBuoiTap.add(realmInt)
                    }

                    val beltColorsRealmList = RealmList<BeltColor>()

                    for (j in listStudent[i].beltColorsModel!!!!.indices) {
                        val beltColor = BeltColor()
                        beltColor.isSelect = listStudent[i].beltColorsModel[j]!!.isSelect
                        beltColor.color = listStudent[i]!!.beltColorsModel[j].color

                        beltColorsRealmList.add(beltColor)

                    }
                    students.monthModels = realmMonth
                    //todo hash caModels
//                    students.caModels = realmCa
                    students.buoiTapModels = realmBuoiTap
                    students.beltColorsModel = beltColorsRealmList
                    list.add(students)

                }
                hideLoadingCircle()
                counter!!.text =  listStudent.size.toString()
                gotoRecylerview()
             }


        })
    }
    private fun gotoRecylerview(){



            recycler.setHasFixedSize(true)
            studentSchoolAdapter = StudentSchoolAdapter(this, list)

            // white background notification bar

            val mLayoutManager = LinearLayoutManager(this)
            recycler.layoutManager = mLayoutManager
           // recycler.itemAnimator = DefaultItemAnimator()
            //recycler.addItemDecoration(MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 36))
            recycler.adapter = studentSchoolAdapter




    }
}
