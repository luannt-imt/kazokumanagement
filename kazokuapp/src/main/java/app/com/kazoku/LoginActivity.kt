package app.com.kazoku

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import app.com.kazoku.lib.activities.GTActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.layout_login.*
import android.app.ProgressDialog
import android.content.Context
import com.google.firebase.database.*
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import android.text.method.TextKeyListener.clear
import com.google.firebase.database.ValueEventListener
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.ConnectivityManager
import androidx.core.content.ContextCompat.getSystemService
import app.com.kazoku.lib.activities.realm.RealmController
import app.com.kazoku.lib.model.*
import app.com.kazoku.lib.model.modelConver.StudentConvertN
import app.com.kazoku.ui.FgQC
import io.realm.Realm
import io.realm.RealmList
import java.util.*


class LoginActivity : GTActivity(), ValueEventListener, ChildEventListener {
    override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

    }

    override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
     }

    override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
        listStudent.add(snapshot!!.getValue(StudentConvertN::class.java)!!)

        println("aaaaaaaaaaaaaaaaaa: "+listStudent.size)
     }

    override fun onChildRemoved(snapshot: DataSnapshot) {
     }

    override fun onCancelled(p0: DatabaseError) {

    }

    override fun onDataChange(p0: DataSnapshot) {
        val dialog = ProgressDialog.show(this, "", "Loading. Please wait...", true)
    }
    private val listStudent = ArrayList<StudentConvertN>()
    var list = ArrayList<Student>()

    public var listSchoolName = ArrayList<String>()
    private var mAuth: FirebaseAuth? = null
    private var firebaseAuthListener: FirebaseAuth.AuthStateListener? = null
    var emailUser: String? = null
    var nameUser: String? = null
    private lateinit var databaseReference: DatabaseReference
    private lateinit var dbChild: DatabaseReference
    var passWord: String? = null
    var rePassWord: String? = null
    var nameUsers: String? = null
    private var realm: Realm? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        databaseReference = FirebaseDatabase.getInstance().reference
        instance = this

        setContentView(R.layout.activity_login)
        mAuth = FirebaseAuth.getInstance()
        this.realm = RealmController.with(this).realm
        //val currentUser = mAuth!!.currentUser!!.email
        firebaseAuthListener = FirebaseAuth.AuthStateListener {
            val user = FirebaseAuth.getInstance().currentUser
            if (user != null) {
                emailUser = user.email!!

                nameUser = emailUser!!.substring(0, emailUser!!.indexOf("@"))
//                if (isTablet(this)){
//                    val intent = Intent(this@MainActivity, LoginSuccessActivityForTablet::class.java)
//                    startActivity(intent)
//                }else{
//                    val intent = Intent(this@MainActivity, LoginSuccessActivity::class.java)
//                    startActivity(intent)
//                }
                if (!TextUtils.isEmpty(nameUser)) {

                    if(list.isEmpty()){
                        println("rrrrrrrrrrrrronggg")
                        actionYesDownLoad()
                       // Toast.makeText(applicationContext,"Rong",Toast.LENGTH_SHORT).show()
                    }else{
                        println(nameUser)
                        if (nameUser == "qc"){
                            showActivity(FgQC::class.java)
                            finish()
                        }else{
                            showActivity(MainActivity::class.java)
                            finish()
                        }

                    }

                  //  showActivity(MainActivity::class.java)
                  //  finish()
                    // loadata(nameUser.toString())
//                    val dialog = ProgressDialog.show(this@MyActivity, "", "Loading. Please wait...", true)

                }
            }
        }
        btnLogin.setOnClickListener {
            checkSingIn()
        }

        txtForgetPass.setOnClickListener {
            startActivity(Intent(applicationContext, ForgetPassActivity::class.java))

        }

        txtLoginTitle.setOnClickListener {
            txtLoginTitle.setTextColor(this.resources.getColor(R.color.primaryTextColor))
            lnLogin.visibility = View.VISIBLE

            txtRegisterTitle.setTextColor(this.resources.getColor(R.color.gray))
            lnRegister.visibility = View.GONE
        }
        txtRegisterTitle.setOnClickListener {
            txtRegisterTitle.setTextColor(this.resources.getColor(R.color.primaryTextColor))
            lnRegister.visibility = View.VISIBLE


            txtLoginTitle.setTextColor(this.resources.getColor(R.color.gray))
            lnLogin.visibility = View.GONE
        }
        btnRegister.setOnClickListener {
            passWord = edTextPassword.text.toString()
            rePassWord = edReTextPassword.text.toString()
            if (passWord!!.contains(rePassWord!!)) {
                if (!TextUtils.isEmpty(edEmailRegister.text.toString())) {
                    //registerUser()
                    Toast.makeText(this, "Bạn không được cấp phép", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Email Không được rỗng", Toast.LENGTH_SHORT).show()
                }

            } else {
                Toast.makeText(this, "Pass Không khớp", Toast.LENGTH_SHORT).show()
            }

        }
        list = getModelList()




    }
    fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
    private fun actionYesDownLoad() {

        if (isOnline(this)) {
            // Toast.makeText(context, "co mang", Toast.LENGTH_SHORT).show()
            //uploadData()
            showLoadingCircle("Tải dữ liệu","Đang tiến hành. Vui lòng chờ trong giây lát..")
            listStudent.clear()



            dbChild = databaseReference.child(LoginActivity.instance.nameUser.toString())
            dbChild.addChildEventListener(this)

            databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {

                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    println("We're done loading the initial " + dataSnapshot.childrenCount + " items")
                    var  students : Student
                    for (i in 0 until listStudent.size) {
                        students =   Student()
                        students.id = listStudent[i].id
                        students.imageUrl = listStudent[i].imageUrl
                        students.name = listStudent[i].name
                        students.dob = listStudent[i].dob
                        students.phone = listStudent[i].phone
                        students.address = listStudent[i].address
                        students.note = listStudent[i].note
                        students.dateStudy = listStudent[i].dateStudy
                        val realmMonth= RealmList<monthModel>()

                        for (j in listStudent[i].monthModels!!.indices) {
                            val realmInt = monthModel()

                            realmInt.isSelect =listStudent[i].monthModels[j]!!.isSelect
                            realmInt.monthsName =listStudent[i].monthModels[j]!!.monthsName
                            realmMonth.add(realmInt)
                        }

//                    val realmCa= RealmList<caModel>()
//                    for (j in listStudent[i].caModels!!.indices){
//                        val realmInt = caModel()
//                        realmInt.caHoc = listStudent[i].caModels[j]!!.caHoc
//                        realmInt.isSelect = listStudent[i].caModels[j]!!.isSelect
//                        realmCa.add(realmInt)
//                    }
                        val realmBuoiTap= RealmList<buoiTapModel>()
                        for (j in listStudent[i].buoiTapModels!!.indices){
                            val realmInt = buoiTapModel()
                            realmInt.dayOfWeek = listStudent[i].buoiTapModels[j]!!.dayOfWeek
                            realmInt.isSelect = listStudent[i].buoiTapModels[j]!!.isSelect
                            realmInt.cahoc = listStudent[i].buoiTapModels[j]!!.cahoc
                            realmBuoiTap.add(realmInt)
                        }

                        val beltColorsRealmList = RealmList<BeltColor>()

                        for (j in listStudent[i].beltColorsModel!!!!.indices) {
                            val beltColor = BeltColor()
                            beltColor.isSelect = listStudent[i].beltColorsModel[j]!!.isSelect
                            beltColor.color = listStudent[i]!!.beltColorsModel[j].color

                            beltColorsRealmList.add(beltColor)

                        }
                        students.monthModels = realmMonth
//                    students.caModels = realmCa
                        //todo hash caModels
                        students.buoiTapModels = realmBuoiTap
                        students.beltColorsModel = beltColorsRealmList

                        realm!!.beginTransaction()
                        realm!!.copyToRealm(students)
                        realm!!.commitTransaction()
                    }

                    hideLoadingCircle()
                    if (nameUser == "qc"){
                        showActivity(FgQC::class.java)
                        finish()
                    }else{
                        showActivity(MainActivity::class.java)
                        finish()
                    }


                }


            })
        } else {
            hideLoadingCircle()
            if (nameUser == "qc"){
                showActivity(FgQC::class.java)
                finish()
            }else{
                showActivity(MainActivity::class.java)
                finish()
            }
        }

    }


    fun getModelList(): ArrayList<Student> {
        var list = ArrayList<Student>()

        try {
            realm = Realm.getDefaultInstance()
            val results = realm!!.where<Student>(Student::class.java)
                    .findAll()
            list.addAll(realm!!.copyFromRealm<Student>(results))
        } finally {
            if (realm != null) {
                realm!!.close()
            }
        }
        return list
    }
    private fun registerUser() {
        mAuth!!.createUserWithEmailAndPassword(edEmailRegister.text.toString(), passWord!!).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                val userId = mAuth!!.currentUser!!.uid
                nameUsers = edEmailRegister.text.toString()!!.substring(0, edEmailRegister.text.toString()!!.indexOf("@"))
               // val databaseReference = FirebaseDatabase.getInstance().reference.child(nameUsers!!)
                //databaseReference.setValue(true)
              //  getDataFromServer()
                showActivity(MainActivity::class.java)
                finish()
                Toast.makeText(this, "đăng ký thành công", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "đăng ký thất bại", Toast.LENGTH_SHORT).show()

            }
        }

    }

    private fun loadata(nameUser: String) {
        val dialog = ProgressDialog(this) // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        dialog.setTitle("Loading")
        dialog.setMessage("Loading data. Please wait...")
        dialog.isIndeterminate = true
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
        databaseReference.child(nameUser).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                dialog.dismiss()
                for (data in dataSnapshot.children) {

                }

            }
            override fun onCancelled(databaseError: DatabaseError) {
                println("fais")
                dialog.dismiss()
            }
        })

    }

    override fun onStart() {
        super.onStart()
        mAuth!!.addAuthStateListener(this!!.firebaseAuthListener!!)
    }

    override fun onStop() {
        super.onStop()
        mAuth!!.removeAuthStateListener(this!!.firebaseAuthListener!!)
    }

    private fun checkSingIn() {
        if (editTextEmail.text.toString() == "") {
            Toast.makeText(this, "Vui lòng nhập tên", Toast.LENGTH_SHORT).show()
            return
        }
        if (editTextPassword.text.toString() == "") {
            Toast.makeText(this, "Vui lòng nhập pass", Toast.LENGTH_SHORT).show()
            return
        }

        mAuth!!.signInWithEmailAndPassword(editTextEmail.text.toString().trim()+"@gmail.com", editTextPassword.text.toString().trim())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                       // Toast.makeText(this, "Đăng nhập thành công!!!", Toast.LENGTH_SHORT).show()
                        //val email = mAuth!!.currentUser!!.email


                        // showActivity(MainActivity::class.java)
                        //finish()
                        //val nameUsers = email!!.substring(0, email!!.indexOf("@"))
                       // loadata(nameUsers!!)
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(this, "Đăng nhập lỗi!!!. Vui lòng kiểm tra tên đănq nhập",
                                Toast.LENGTH_SHORT).show()
                    }

                    // ...
                }
    }
    companion object {
        lateinit var instance: LoginActivity
            private set
    }
}
