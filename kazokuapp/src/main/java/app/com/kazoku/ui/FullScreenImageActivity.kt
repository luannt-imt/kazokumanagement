package app.com.kazoku.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import app.com.kazoku.R
import kotlinx.android.synthetic.main.activity_full_screen_image.*

class FullScreenImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_image)
        val name = intent.getIntExtra("img",0)
        println("ahihii $name")
        imageView12.setImageResource(name)
    }
}
