package app.com.kazoku.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.adapter.MyImagesRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_learn.*


class LearnActivity : GTActivity() ,MyImagesRecyclerViewAdapter.ItemClickListener{
    override fun onItemClick(view: View?, position: Int) {
//        showActivity(FullScreenImageActivity::class.java)
        val intent = Intent(this, FullScreenImageActivity::class.java)
        intent.putExtra("img",myImageList[position])
        startActivity(intent)
    }

    var myImageList: ArrayList<Int> = ArrayList()
    private var schoolAdapter: MyImagesRecyclerViewAdapter? = null
    private lateinit var toolbar: Toolbar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_learn)
        toolbar = findViewById(R.id.toolbar)



        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        supportActionBar!!.title ="Danh Sách Bài Tập"
        myImageList.add(R.drawable.dai)
        myImageList.add(R.drawable.chan)
        myImageList.add(R.drawable.geri_waza)
        myImageList.add(R.drawable.positions)
        myImageList.add(R.drawable.ude1)
        myImageList.add(R.drawable.uchi_waza)
        myImageList.add(R.drawable.defense_001)
        myImageList.add(R.drawable.defenses002)
        myImageList.add(R.drawable.defenses003)
        myImageList.add(R.drawable.defenses004)
        myImageList.add(R.drawable.heian_shodan)
        myImageList.add(R.drawable.heian_nidan)
        myImageList.add(R.drawable.heian_sandan)
        myImageList.add(R.drawable.heian_yodan)
        myImageList.add(R.drawable.heian_godan)
        myImageList.add(R.drawable.tekki_shodan)

        recyclerView.setHasFixedSize(true)
        schoolAdapter = MyImagesRecyclerViewAdapter(this,myImageList)
        val mLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = mLayoutManager

        schoolAdapter!!.setClickListener(this)
        recyclerView.adapter = schoolAdapter

    }
}
