package app.com.kazoku.ui

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.activities.realm.RealmController
import app.com.kazoku.lib.adapter.AttendanceAdapter
import app.com.kazoku.lib.model.*
import app.com.kazoku.lib.model.modelConver.StudentConvertN
import com.google.firebase.database.*
import io.realm.RealmList
import kotlinx.android.synthetic.main.attendance_fg.*
import kotlinx.android.synthetic.main.content_list_student_school.*
import java.text.SimpleDateFormat
import java.util.*

class QCDiemDanhActivity : GTActivity(), ChildEventListener {
    override fun onCancelled(error: DatabaseError) {

    }

    override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

    }

    override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {

    }

    override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
        listStudent.add(snapshot!!.getValue(StudentConvertN::class.java)!!)

        println("aaaaaaaaaaaaaaaaaa: "+listStudent.size)
    }

    override fun onChildRemoved(snapshot: DataSnapshot) {

    }
    var listFilterDay = ArrayList<Student>()
    private var studentAdapter: AttendanceAdapter? = null
    var listCa = ArrayList<Student>()
    private lateinit var toolbar: Toolbar
    var list = ArrayList<Student>()
    //    var listConvert = ArrayList<StudentConvert>()
    private val listStudent = ArrayList<StudentConvertN>()
    internal var databaseReference = FirebaseDatabase.getInstance().reference
    private lateinit var dbChild: DatabaseReference
    var dayOfTheWeek: String? = null
    var day: String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qcdiem_danh)
        val name = intent.getStringExtra("name")
        println("ahihii $name")
        toolbar = findViewById(R.id.toolbar)



        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        supportActionBar!!.title ="Điểm danh $name"
        actionDownLoad(name)
        val sdf = SimpleDateFormat("EEEE")
        val d = Date()


        dayOfTheWeek = sdf.format(d).toString()
        txtDay.text = "Hôm nay là thứ: $dayOfTheWeek"

          day = dayOfTheWeek
//        day = day!!.replace("Thứ Sáu", "")
        if (dayOfTheWeek == "Thứ Hai") {
            day = day!!.replace(dayOfTheWeek + "", "T2")
        }
        if (dayOfTheWeek == "Thứ Ba") {

            day = day!!.replace(dayOfTheWeek + "", "T3")
        }
        if (dayOfTheWeek == "Thứ Tư") {
            day = day!!.replace(dayOfTheWeek + "", "T4")
        }
        if (dayOfTheWeek == "Thứ Năm") {
            day = day!!.replace(dayOfTheWeek + "", "T5")
        }
        if (dayOfTheWeek == "Thứ Sáu") {
//            day!!.replacea("Thứ Sáu", "T6")
            day = day!!.replace(dayOfTheWeek + "", "T6")
        }
        if (dayOfTheWeek == "Thứ Bảy") {
            day = day!!.replace(dayOfTheWeek + "", "T7")
        }
        if (dayOfTheWeek == "Chủ Nhật") {
            day = day!!.replace(dayOfTheWeek + "", "CN")
        }

       /* btnCa1.setOnClickListener {
            listCa.clear()
            getData("1")
            btnCa1.setTextColor(Color.RED)
            btnCa2.setTextColor(Color.WHITE)
            btnCa3.setTextColor(Color.WHITE)
        }
        btnCa2.setOnClickListener {
            listCa.clear()
            getData("2")
            btnCa2.setTextColor(Color.RED)
            btnCa1.setTextColor(Color.WHITE)
            btnCa3.setTextColor(Color.WHITE)
        }
        btnCa3.setOnClickListener {
            listCa.clear()
            getData("3")
            btnCa3.setTextColor(Color.RED)
            btnCa1.setTextColor(Color.WHITE)
            btnCa2.setTextColor(Color.WHITE)
        }*/
        btnCa1.setOnClickListener {
            listCa.clear()
            getData("Ca 1")
            btnCa1.setTextColor(Color.RED)
            btnCa2.setTextColor(Color.WHITE)
            btnCa3.setTextColor(Color.WHITE)
            btnCa4.setTextColor(Color.WHITE)
            btnCa5.setTextColor(Color.WHITE)
            btnCa6.setTextColor(Color.WHITE)
        }
        btnCa2.setOnClickListener {
            listCa.clear()
            getData("Ca 2")
            btnCa2.setTextColor(Color.RED)
            btnCa1.setTextColor(Color.WHITE)
            btnCa3.setTextColor(Color.WHITE)
            btnCa4.setTextColor(Color.WHITE)
            btnCa5.setTextColor(Color.WHITE)
            btnCa6.setTextColor(Color.WHITE)
        }
        btnCa3.setOnClickListener {
            listCa.clear()
            getData("Ca 3")
            btnCa3.setTextColor(Color.RED)
            btnCa1.setTextColor(Color.WHITE)
            btnCa2.setTextColor(Color.WHITE)
            btnCa4.setTextColor(Color.WHITE)
            btnCa5.setTextColor(Color.WHITE)
            btnCa6.setTextColor(Color.WHITE)
        }
        btnCa4.setOnClickListener {
            listCa.clear()
            getData("Ca 4")
            btnCa4.setTextColor(Color.RED)
            btnCa1.setTextColor(Color.WHITE)
            btnCa2.setTextColor(Color.WHITE)
            btnCa3.setTextColor(Color.WHITE)
            btnCa5.setTextColor(Color.WHITE)
            btnCa6.setTextColor(Color.WHITE)
        }
        btnCa5.setOnClickListener {
            listCa.clear()
            getData("Ca 5")
            btnCa5.setTextColor(Color.RED)
            btnCa1.setTextColor(Color.WHITE)
            btnCa2.setTextColor(Color.WHITE)
            btnCa3.setTextColor(Color.WHITE)
            btnCa4.setTextColor(Color.WHITE)
            btnCa6.setTextColor(Color.WHITE)
        }
        btnCa6.setOnClickListener {
            listCa.clear()
            getData("Ca 6")
            btnCa6.setTextColor(Color.RED)
            btnCa1.setTextColor(Color.WHITE)
            btnCa2.setTextColor(Color.WHITE)
            btnCa3.setTextColor(Color.WHITE)
            btnCa4.setTextColor(Color.WHITE)
            btnCa5.setTextColor(Color.WHITE)
        }
    }
    private fun getData(caString: String) {
        for (i in 0 until listFilterDay.size) {

            for (j in 0 until listFilterDay[i].buoiTapModels.size) {
                if (listFilterDay[i].buoiTapModels[j].isSelect) {
                    if (listFilterDay[i].buoiTapModels[j].cahoc == caString) {
                        if (listFilterDay[i].buoiTapModels[j].dayOfWeek == day) {
                            var student = Student()
                            student.address = listFilterDay[i].address
                            student.beltColorsModel = listFilterDay[i].beltColorsModel
                            student.buoiTapModels = listFilterDay[i].buoiTapModels
//                        student.buoiTapModels. = listFilterDay[i].caModels
                            student.dateStudy = listFilterDay[i].dateStudy
                            student.dob = listFilterDay[i].dob
                            student.id = listFilterDay[i].id
                            student.name = listFilterDay[i].name
                            student.imageUrl = listFilterDay[i].imageUrl
                            student.note = listFilterDay[i].note
                            student.phone = listFilterDay[i].phone
                            student.monthModels = listFilterDay[i].monthModels
                            listCa.add(student)
                        }

                    }
                }


            }
        }
        setupRecycler()


    }

    private fun setupRecycler() {


        rcvAttendance.setHasFixedSize(true)
        studentAdapter = AttendanceAdapter(this, listCa)

        // white background notification bar

        val mLayoutManager = LinearLayoutManager(this)
        rcvAttendance.layoutManager = mLayoutManager
        rcvAttendance.itemAnimator = DefaultItemAnimator()
        rcvAttendance.adapter = studentAdapter

    }
    private fun actionDownLoad(name:  String) {
        showLoadingCircle("Tải dữ liệu","Đang tiến hành. Vui lòng chờ trong giây lát..")
        listStudent.clear()



        dbChild = databaseReference.child(name)
        dbChild.addChildEventListener(this)

        databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                println("We're done loading the initial " + dataSnapshot.childrenCount + " items")
                var  students : Student
                for (i in 0 until listStudent.size) {
                    students =   Student()
                    students.id = listStudent[i].id
                    students.imageUrl = listStudent[i].imageUrl
                    students.name = listStudent[i].name
                    students.dob = listStudent[i].dob
                    students.phone = listStudent[i].phone
                    students.address = listStudent[i].address
                    students.note = listStudent[i].note
                    students.dateStudy = listStudent[i].dateStudy
                    val realmMonth= RealmList<monthModel>()

                    for (j in listStudent[i].monthModels!!.indices) {
                        val realmInt = monthModel()

                        realmInt.isSelect =listStudent[i].monthModels[j]!!.isSelect
                        realmInt.monthsName =listStudent[i].monthModels[j]!!.monthsName
                        realmMonth.add(realmInt)
                    }


                    val realmBuoiTap= RealmList<buoiTapModel>()
                    for (j in listStudent[i].buoiTapModels!!.indices){
                        val realmInt = buoiTapModel()
                        realmInt.dayOfWeek = listStudent[i].buoiTapModels[j]!!.dayOfWeek
                        realmInt.isSelect = listStudent[i].buoiTapModels[j]!!.isSelect
                        realmInt.cahoc = listStudent[i].buoiTapModels[j]!!.cahoc
                        realmBuoiTap.add(realmInt)
                    }

                    val beltColorsRealmList = RealmList<BeltColor>()

                    for (j in listStudent[i].beltColorsModel!!.indices) {
                        val beltColor = BeltColor()
                        beltColor.isSelect = listStudent[i].beltColorsModel[j]!!.isSelect
                        beltColor.color = listStudent[i]!!.beltColorsModel[j].color

                        beltColorsRealmList.add(beltColor)

                    }
                    students.monthModels = realmMonth
//                    students.caModels = realmCa
                    students.buoiTapModels = realmBuoiTap
                    students.beltColorsModel = beltColorsRealmList
                    list.add(students)



                }
                hideLoadingCircle()
                for (i in 0 until list.size) {
                    for (j in 0 until list[i].buoiTapModels.size) {
                        if (list[i].buoiTapModels[j].isSelect) {

                            if (list[i].buoiTapModels[j].dayOfWeek == day) {
                                var student = Student()
                                student.address = list[i].address
                                student.beltColorsModel = list[i].beltColorsModel
                                student.buoiTapModels = list[i].buoiTapModels
//                                student.caModels = list[i].caModels
                                //todo hash caModels
                                student.dateStudy = list[i].dateStudy
                                student.dob = list[i].dob
                                student.id = list[i].id
                                student.name = list[i].name
                                student.imageUrl = list[i].imageUrl
                                student.note = list[i].note
                                student.phone = list[i].phone
                                student.monthModels = list[i].monthModels
                                listFilterDay.add(student)
                            }
                        }
                    }
                }
             }


        })
    }
}
