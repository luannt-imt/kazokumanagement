package app.com.kazoku.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.activities.realm.RealmController

import app.com.kazoku.lib.adapter.AttendanceAdapter
import app.com.kazoku.lib.adapter.FeeAdapter
import app.com.kazoku.lib.fragments.GTFragment
import app.com.kazoku.lib.model.Student
import io.realm.Realm
import kotlinx.android.synthetic.main.fee_fg.*
import java.util.*




class FgFee : GTActivity() {

    var listUsersRemove = ArrayList<Student>()
    var listUsersModel : Student?= null

    var list = ArrayList<Student>()
    var listFilterDay = ArrayList<Student>()
    var listCa = ArrayList<Student>()
    private var studentAdapter: FeeAdapter? = null
    private var realm: Realm? = null
    private lateinit var toolbar: Toolbar



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fee_fg)
        this.realm = RealmController.with(this).realm
        RealmController.with(this).refresh()
        toolbar = findViewById(R.id.toolbar)



        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        supportActionBar!!.title ="Ds nợ học phí"

        list = getModelList()
        loadData()
    }

    fun getModelList(): ArrayList<Student> {
        var list = ArrayList<Student>()

        try {
            realm = Realm.getDefaultInstance()
            val results = realm!!.where<Student>(Student::class.java)
                    .findAll()
            list.addAll(realm!!.copyFromRealm<Student>(results))
        } finally {
            if (realm != null) {
                realm!!.close()
            }
        }
        return list
    }

    private fun setupRecycler() {


        rcvFee.setHasFixedSize(true)
        studentAdapter = FeeAdapter(this, listUsersRemove)

        // white background notification bar

        val mLayoutManager = LinearLayoutManager(this)
        rcvFee.layoutManager = mLayoutManager
        rcvFee.itemAnimator = DefaultItemAnimator()
        rcvFee.adapter = studentAdapter
        txtTotal.text = "Tổng: " + listUsersRemove.size.toString()

    }
    private fun loadData() {
        var temp = ""
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        var month = c.get(Calendar.MONTH)+1
        if (list.size == 1) {
            temp = list[0].monthModels!![0].toString()
            println(temp)
        } else {

            listUsersRemove.clear()
            for (i in 0 until list.size) {
                var temp = false
                var tempDay = false
                var months = ""
                var day = ""
                for ( ii in  0 until list[i].monthModels!!.size) {
                    var a = list[i].monthModels!![ii].monthsName.toString()
                    var b = list[i].monthModels!![ii].isSelect
                    if (b) {
                        if (!temp) {
                            months = list[i].monthModels!![ii].monthsName.toString()
                            temp = true
                        } else {
                            months = list[i].monthModels!![ii].monthsName.toString()
                        }
                    }

                }
                if (months == getString(R.string.txt_fee_free) || months == getString(R.string.txt_study)){

                }else{
                    val monthFee = months.replace("T", "")//replaces all occurrences of "is" to "was"
                    if (month > monthFee.toInt()){
                        var student = Student()
                        student.address = list[i].address
                        student.beltColorsModel = list[i].beltColorsModel
                        student.buoiTapModels = list[i].buoiTapModels
//                    student.caModels = list[i].caModels
                        student.dateStudy = list[i].dateStudy
                        student.dob = list[i].dob
                        student.id = list[i].id
                        student.name = list[i].name
                        student.imageUrl = list[i].imageUrl
                        student.note = list[i].note
                        student.phone = list[i].phone
                        student.monthModels = list[i].monthModels
                        listUsersRemove.add(student!!)
                    }
                    println("bbbbb: "+ months)
                }


            }
            setupRecycler()
        }


    }
}