package app.com.kazoku.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import app.com.kazoku.LoginActivity
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.model.*
import app.com.kazoku.models.monneyModel
import com.google.firebase.database.*
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_fee_details.*
import kotlinx.android.synthetic.main.content_list_student_school.*
import java.util.ArrayList

class FeeDetailsActivity : GTActivity() , ChildEventListener{
    override fun onCancelled(error: DatabaseError) {

    }

    override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

    }

    override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
     }

    override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
        listMonney.add(snapshot!!.getValue(monneyModel::class.java)!!)
        println(listMonney.size)
     }

    override fun onChildRemoved(snapshot: DataSnapshot) {
     }

    private lateinit var toolbar: Toolbar
    private lateinit var databaseReference: DatabaseReference
    private lateinit var dbChild: DatabaseReference
    private val listMonney = ArrayList<monneyModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fee_details)
        val name = intent.getStringExtra("name")
        println("ahihii $name")
        toolbar = findViewById(R.id.toolbar)

        databaseReference = FirebaseDatabase.getInstance().reference

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        supportActionBar!!.title = "Quản lý thu phí"
        actionDownLoad(name)
    }
    private fun actionDownLoad(name:  String) {
        showLoadingCircle("Tải dữ liệu","Đang tiến hành. Vui lòng chờ trong giây lát..")

        listMonney.clear()
         dbChild = databaseReference.child("monney$name")
         dbChild.addChildEventListener(this)
        databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                println(listMonney.size)
                txtFee.text = "Số lần thu học phí: "+ listMonney.size
                txtFee.setTextColor(resources.getColor(R.color.red))
                hideLoadingCircle()
            }


        })
    }
}
