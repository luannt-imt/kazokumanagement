package app.com.kazoku.ui

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import app.com.kazoku.LoginActivity
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.activities.realm.RealmController
import app.com.kazoku.lib.model.*
import app.com.kazoku.lib.model.modelConver.StudentConvertN
import app.com.kazoku.lib.model.modelConver.deleteModelReaml
import com.google.firebase.database.*
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.content_sysc_data.*
import java.util.*

import android.os.CountDownTimer


class FgSyscData : GTActivity(), ChildEventListener {
    override fun onCancelled(error: DatabaseError) {

    }

    override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
    }

    override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
    }

    override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
        // listConvert.clear()
        ///listConvert.add(snapshot!!.getValue(StudentConvert::class.java)!!)
        listStudent.add(snapshot!!.getValue(StudentConvertN::class.java)!!)

        println("aaaaaaaaaaaaaaaaaa: " + listStudent.size)

    }

    override fun onChildRemoved(snapshot: DataSnapshot) {

    }

    private lateinit var toolbar: Toolbar
    private var realm: Realm? = null
    internal var databaseReference = FirebaseDatabase.getInstance().reference
    var list = ArrayList<Student>()
    var listMonney = ArrayList<monneyModelReaml>()
    var listStudentDelete = ArrayList<deleteModelReaml>()
    //    var listConvert = ArrayList<StudentConvert>()
    private val listStudent = ArrayList<StudentConvertN>()
    private lateinit var dbChild: DatabaseReference
    var isSuccess = false
    var count = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.item_sysc_data)
        this.realm = RealmController.with(this).realm
        list = getModelList()
        listMonney = getModelLists()
        listStudentDelete = getModelListsDelete()

        btnSysc.setOnClickListener {
            if (!list.isEmpty()) {

                dialog("Bạn có muốn đồng bộ lên server? Vui lòng bấm Đồng ý để xác nhận.")
            } else {
                dialogNoData("Hiện tại dữ liệu của bạn đang trống..")
            }
        }
        btnDownload.setOnClickListener {

            dialogDownload("Bạn có muốn tải dữ liệu từ hệ thống về máy?")

        }
        toolbar = findViewById(R.id.toolbar)



        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        supportActionBar!!.title = "Đồng bộ dữ liệu"
    }

    private fun actionYesDownLoad() {
        showLoadingCircle("Tải dữ liệu", "Đang tiến hành. Vui lòng chờ trong giây lát..")
        listStudent.clear()
        try {
            realm!!.executeTransaction { realm ->
                realm.clear(Student::class.java)
            }
        } finally {
            //realm?.close()
        }


        dbChild = databaseReference.child(LoginActivity.instance.nameUser.toString())
        dbChild.addChildEventListener(this)

        databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                println("We're done loading the initial " + dataSnapshot.childrenCount + " items")
                var students: Student
                for (i in 0 until listStudent.size) {
                    students = Student()
                    students.id = listStudent[i].id
                    students.imageUrl = listStudent[i].imageUrl
                    students.name = listStudent[i].name
                    students.dob = listStudent[i].dob
                    students.phone = listStudent[i].phone
                    students.address = listStudent[i].address
                    students.note = listStudent[i].note
                    students.dateStudy = listStudent[i].dateStudy
                    val realmMonth = RealmList<monthModel>()

                    for (j in listStudent[i].monthModels!!.indices) {
                        val realmInt = monthModel()

                        realmInt.isSelect = listStudent[i].monthModels[j]!!.isSelect
                        realmInt.monthsName = listStudent[i].monthModels[j]!!.monthsName
                        realmMonth.add(realmInt)
                    }

//                    val realmCa= RealmList<caModel>()
//                    for (j in listStudent[i].caModels!!.indices){
//                        val realmInt = caModel()
//                        realmInt.caHoc = listStudent[i].caModels[j]!!.caHoc
//                        realmInt.isSelect = listStudent[i].caModels[j]!!.isSelect
//                        realmCa.add(realmInt)
//                    }
                    val realmBuoiTap = RealmList<buoiTapModel>()
                    for (j in listStudent[i].buoiTapModels!!.indices) {
                        val realmInt = buoiTapModel()
                        realmInt.dayOfWeek = listStudent[i].buoiTapModels[j]!!.dayOfWeek
                        realmInt.isSelect = listStudent[i].buoiTapModels[j]!!.isSelect
                        realmInt.cahoc = listStudent[i].buoiTapModels[j]!!.cahoc
                        realmBuoiTap.add(realmInt)
                    }

                    val beltColorsRealmList = RealmList<BeltColor>()

                    for (j in listStudent[i].beltColorsModel!!!!.indices) {
                        val beltColor = BeltColor()
                        beltColor.isSelect = listStudent[i].beltColorsModel[j]!!.isSelect
                        beltColor.color = listStudent[i]!!.beltColorsModel[j].color

                        beltColorsRealmList.add(beltColor)

                    }
                    students.monthModels = realmMonth
//                    students.caModels = realmCa
                    //todo hash caModels
                    students.buoiTapModels = realmBuoiTap
                    students.beltColorsModel = beltColorsRealmList

                    realm!!.beginTransaction()
                    realm!!.copyToRealm(students)
                    realm!!.commitTransaction()
                }
                //Toast.makeText(applicationContext,"Thành Công",Toast.LENGTH_SHORT).show()
                hideLoadingCircle()

            }


        })
    }

    private fun actionYes() {
        showLoadingCircle("up dữ liệu", "Đang tiến hành. Vui lòng chờ trong giây lát..")
        FirebaseDatabase.getInstance().getReference(LoginActivity.instance.nameUser.toString()).removeValue()
        FirebaseDatabase.getInstance().getReference("monney" + LoginActivity.instance.nameUser.toString()).removeValue()
        FirebaseDatabase.getInstance().getReference("delete" + LoginActivity.instance.nameUser.toString()).removeValue()

        for (i in 0 until listMonney.size) {
            val monneyModelReamls = monneyModelReaml()
            try {
                monneyModelReamls.monney = listMonney[i].monney
                monneyModelReamls.id = listMonney[i].id
                uploadDataMonney(monneyModelReamls)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        for (i in 0 until listStudentDelete.size) {
            val deleteModelReaml = deleteModelReaml()
            try {
                deleteModelReaml.name = listStudentDelete[i].name
                deleteModelReaml.phone = listStudentDelete[i].phone
                deleteModelReaml.id = listStudentDelete[i].id
                uploadDataDetele(deleteModelReaml)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        for (i in 0 until list.size) {
            val student = Student()

            try {
                student.id = list[i].id
                student.name = list[i].name
                student.dob = list[i].dob
                student.phone = list[i].phone
                student.address = list[i].address
                student.note = list[i].note
                student.imageUrl = list[i].imageUrl
                // val currentDate = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault()).format(Date())

                student.dateStudy = list[i].dateStudy

                student.monthModels = list[i].monthModels
//                student.caModels = list[i].caModels
                student.buoiTapModels = list[i].buoiTapModels
                student.beltColorsModel = list[i].beltColorsModel

                uploadData(student, list.size)


            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val timer = object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
                Log.e(TAG, "xonggggggggggggggggggggggggggggggg")
                if (!isSuccess) {
                    showDialog()
                } else {

                }

            }
        }
        timer.start()

    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    fun showDialog() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Thất bại!")
        builder.setMessage("Vui lòng kiểm tra kết nối")

//builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))

        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            Toast.makeText(applicationContext,
                    android.R.string.yes, Toast.LENGTH_SHORT).show()

        }
        builder.show()
        hideLoadingCircle()
    }

    private fun dialog(msg: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Chú ý!!")
        builder.setMessage(msg)


        builder.setPositiveButton("Đồng Ý") { dialog, which ->
            if (isOnline(this)) {
                count = 0
                actionYes()
            } else {
                Toast.makeText(this, "Mạng không khả dụng. Vui lòng kiểm tra và thực hiện lại..", Toast.LENGTH_LONG).show()
            }


        }


        builder.setNegativeButton("Bỏ Qua") { dialog, which ->
            dialog.dismiss()
            hideLoadingCircle()
        }
        builder.show()
    }

    private fun dialogDownload(msg: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Chú ý!!")
        builder.setMessage(msg)

        builder.setPositiveButton("Đồng Ý") { dialog, which ->
            actionYesDownLoad()
        }


        builder.setNegativeButton("Bỏ Qua") { dialog, which ->
            dialog.dismiss()
        }
        builder.show()
    }

    private fun dialogDownloadWarning(msg: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Chú ý!!")
        builder.setMessage(msg)

        builder.setPositiveButton("Đồng Ý") { dialog, which ->
            actionYesDownLoad()
        }


        builder.setNegativeButton("Bỏ Qua") { dialog, which ->
            dialog.dismiss()
        }
        builder.show()
    }

    private fun dialogNoData(msg: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Chú ý!!")
        builder.setMessage(msg)

        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            dialog.dismiss()
        }
        builder.show()
    }

    private fun uploadData(student: Student, size: Int) {
//        databaseReference.child(LoginActivity.instance.nameUser.toString()).push().setValue(student).addOnSuccessListener {
//             object : OnSuccessListener{
//
//             }
//        }


        databaseReference.child(LoginActivity.instance.nameUser.toString()).push().setValue(student).addOnSuccessListener {
            Log.e(TAG, "Success !")
            count++
            isSuccess = true
            if (count == size) {

                Toast.makeText(this, "Thành Công", Toast.LENGTH_SHORT).show()
                hideLoadingCircle()
            }
        }.addOnFailureListener {
            Log.e(TAG, "FAIL")
            isSuccess = false
        }


    }

    private fun uploadDataMonney(data: monneyModelReaml) {
        databaseReference.child("monney" + LoginActivity.instance.nameUser).push().setValue(data)
        // databaseReference.child(LoginActivity.instance.nameUser.toString()).push().setValue(data)
        // hideLoadingCircle()
        // Toast.makeText(this,"Thành Công",Toast.LENGTH_SHORT).show()

    }

    private fun uploadDataDetele(data: deleteModelReaml) {
        databaseReference.child("delete" + LoginActivity.instance.nameUser).push().setValue(data)
        // databaseReference.child(LoginActivity.instance.nameUser.toString()).push().setValue(data)
        // hideLoadingCircle()
        // Toast.makeText(this,"Thành Công",Toast.LENGTH_SHORT).show()

    }

    fun getModelList(): ArrayList<Student> {
        var list = ArrayList<Student>()

        try {
            realm = Realm.getDefaultInstance()
            val results = realm!!.where<Student>(Student::class.java)
                    .findAll()
            list.addAll(realm!!.copyFromRealm<Student>(results))
        } finally {
            if (realm != null) {
                realm!!.close()
            }
        }
        return list
    }

    fun getModelLists(): ArrayList<monneyModelReaml> {
        var list = ArrayList<monneyModelReaml>()

        try {
            realm = Realm.getDefaultInstance()
            val results = realm!!.where<monneyModelReaml>(monneyModelReaml::class.java)
                    .findAll()
            list.addAll(realm!!.copyFromRealm<monneyModelReaml>(results))
        } finally {
            if (realm != null) {
                realm!!.close()
            }
        }
        return list
    }

    fun getModelListsDelete(): ArrayList<deleteModelReaml> {
        var list = ArrayList<deleteModelReaml>()

        try {
            realm = Realm.getDefaultInstance()
            val results = realm!!.where<deleteModelReaml>(deleteModelReaml::class.java)
                    .findAll()
            list.addAll(realm!!.copyFromRealm<deleteModelReaml>(results))
        } finally {
            if (realm != null) {
                realm!!.close()
            }
        }
        return list
    }
}
