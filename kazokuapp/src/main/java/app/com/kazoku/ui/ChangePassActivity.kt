package app.com.kazoku.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import app.com.kazoku.LoginActivity
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.activities.realm.RealmController
import app.com.kazoku.lib.model.Student
import app.com.kazoku.lib.model.monneyModelReaml
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_change_pass.*
import kotlin.system.exitProcess


class ChangePassActivity : GTActivity() {
    var passWord: String? = null
    var newPassWord: String? = null
    var email: String? = null
    private var realm: Realm? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_pass)
        this.realm = RealmController.with(this).realm
        btnChangePass.setOnClickListener {
            passWord = edTextPassword.text.toString()
            newPassWord = edReTextPassword.text.toString()
            email = edEmailRegister.text.toString()
            if (TextUtils.isEmpty(email)) {
                //email is empty
                Toast.makeText(this, "Vui lòng nhập email", Toast.LENGTH_SHORT).show()
                //stopping execution further
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(passWord)) {
                //password is empty
                Toast.makeText(this, "Vui lòng nhập mật khẩu", Toast.LENGTH_SHORT).show()
                //stopping execution furthe
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(newPassWord)) {
                //password is empty
                Toast.makeText(this, "Vui lòng nhập mật khẩu mới", Toast.LENGTH_SHORT).show()
                //stopping execution further
                return@setOnClickListener
            }

            registerUser()
            /* if (!TextUtils.isEmpty(edEmailRegister.text.toString()) && !TextUtils.isEmpty()) {
                 registerUser()
             } else {
                 Toast.makeText(this, "Email Không được rỗng", Toast.LENGTH_SHORT).show()
             }*/


        }
    }

    private fun registerUser() {
        showLoadingCircle("Đổi mật khẩu.. ","vui lòng đợi..")
        var user = FirebaseAuth.getInstance().currentUser
        val credential = EmailAuthProvider.getCredential(email!! + "@gmail.com", passWord!!)
        user!!.reauthenticate(credential).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                println("true")
                user.updatePassword(newPassWord!!).addOnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        println("failse")

                    } else {
                        hideLoadingCircle()
                        dialogNoData("Thành công")
                    }
                }

            } else {
                println("failse")
            }
        }

    }

    private fun dialogNoData(msg: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Đổi Mật Khẩu")
        builder.setMessage(msg)

        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            dialog.dismiss()
            finish()

        }
        builder.show()
    }
}
