package app.com.kazoku.ui

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import app.com.kazoku.DetailsSchoolActivity
import app.com.kazoku.LoginActivity
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.adapter.MySchoolRecyclerViewAdapter
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.content_management.*
import java.util.*


class FgManagement : GTActivity() , ChildEventListener,MySchoolRecyclerViewAdapter.ItemClickListener {
    override fun onItemClick(view: View?, position: Int) {
//        println("click: $position")
        gotoDetails(listSchoolName[position])
    }

    override fun onCancelled(error: DatabaseError) {

    }

    override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
     }

    override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
     }

    override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
        println(snapshot.key)
        if (snapshot.key!!.contains("monney") || snapshot.key!!.contains("delete")|| snapshot.key!!.contains("link")){
            println("giong")
        }else{

                listSchoolName.add(snapshot!!.key!!.toString()!!)

        }

     }

    override fun onChildRemoved(snapshot: DataSnapshot) {

     }


    private lateinit var toolbar: Toolbar
    internal var databaseReference = FirebaseDatabase.getInstance().reference

    private var schoolAdapter: MySchoolRecyclerViewAdapter? = null

    private val listSchoolName = ArrayList<String>()
    private lateinit var dbChild: DatabaseReference
     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.management_layout_fg)

        toolbar = findViewById(R.id.toolbar)



        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        supportActionBar!!.title = "Danh sách tất cả CLB"


        loadData()


    }
    private fun gotoDetails(name:  String){
        val intent = Intent(this,DetailsSchoolActivity::class.java)
        intent.putExtra("name",name)
        startActivity(intent)
    }

    private fun gotoRecylerview(){

        list_item.setHasFixedSize(true)
        schoolAdapter = MySchoolRecyclerViewAdapter(this,listSchoolName)
        val mLayoutManager = LinearLayoutManager(this)
        list_item.layoutManager = mLayoutManager
        schoolAdapter!!.setClickListener(this)
        //list_item.itemAnimator = DefaultItemAnimator()
       // list_item.addItemDecoration(MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 0))
        list_item.adapter = schoolAdapter
    }
    fun loadData (){
        showLoadingCircle("Tải dữ liệu","Đang tiến hành. Vui lòng chờ trong giây lát..")
        listSchoolName.clear()
        dbChild = databaseReference
        dbChild.addChildEventListener(this)
        databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                println(listSchoolName.size)

                gotoRecylerview()

                hideLoadingCircle()
            }


        })
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the main_menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_management, menu)


        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items

        when (item.itemId) {

            R.id.action_fee -> {
                LoginActivity.instance.listSchoolName = listSchoolName
                showActivity(ManagementFeeActivity::class.java)
                //replaceFragmentAnimationTopToBottom(FgFee.newInstance(getModelList()))
                return true
            }
            R.id.action_check -> {
                LoginActivity.instance.listSchoolName = listSchoolName
                showActivity(checkStudentOutActivity::class.java)
                //replaceFragmentAnimationTopToBottom(FgFee.newInstance(getModelList()))
                return true
            }
            R.id.action_changepass -> {
                showActivity(ChangePassActivity::class.java)
                //showActivity(ManagementFeeActivity::class.java)
                //replaceFragmentAnimationTopToBottom(FgFee.newInstance(getModelList()))
                return true
            }
        }
        return super.onOptionsItemSelected(item)

    }
}