package app.com.kazoku.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import kotlinx.android.synthetic.main.content_upload_video.*


class FgUploadVideo : GTActivity()   {
    private lateinit var toolbar: Toolbar
     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.video_layout_fg)

        toolbar = findViewById(R.id.toolbar)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
         btnUpload.setOnClickListener {
             showActivity(UploadVideoActivity::class.java)
         }
         btnShow.setOnClickListener {
             showActivity(ActivityShowYouTuBe::class.java)
         }
         btnNotifi.setOnClickListener {

             showActivity(LearnActivity::class.java)
         }
        supportActionBar!!.title = "Quản lý bài tập"
    }




}