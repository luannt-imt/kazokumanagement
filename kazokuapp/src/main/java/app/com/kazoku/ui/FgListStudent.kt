package app.com.kazoku.ui

import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager

import app.com.kazoku.lib.activities.realm.RealmController
import app.com.kazoku.lib.adapter.MyDividerItemDecoration
import app.com.kazoku.lib.adapter.StudentAdapter
import app.com.kazoku.lib.fragments.GTFragment
import app.com.kazoku.lib.model.Student
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import io.realm.Realm
import kotlinx.android.synthetic.main.content_list_student.*
import java.util.ArrayList
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.DisplayMetrics
import android.view.*
 import app.com.kazoku.LoginActivity
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.model.monneyModelReaml
import app.com.kazoku.models.monneyModel
import com.google.firebase.database.*
import java.io.*


class FgListStudent : GTActivity(), StudentAdapter.ContactsAdapterListener, ChildEventListener {
    override fun onCancelled(error: DatabaseError) {

    }

    override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
    }

    override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
    }

    override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
        println(snapshot.key)


        listMonney.add(snapshot!!.getValue(monneyModel::class.java)!!)
        println(listMonney.size)

    }

    override fun onChildRemoved(snapshot: DataSnapshot) {

    }

    override fun onsetMonney(strings: List<String>?) {
        println(strings!!.size)
        //  var monney = listMonney[0].monney


        for (i in strings.indices) {
//            var monneyModel = monneyModel(strings[i])
           var monneyModelReamls = monneyModelReaml()
            monneyModelReamls.monney = strings[i]
            monneyModelReamls.id = RealmController.getInstance().books.size + System.currentTimeMillis()

//            databaseReference.child("monney" + LoginActivity.instance.nameUser).push().setValue(monneyModel)

            realm!!.beginTransaction()
            realm!!.copyToRealm(monneyModelReamls)
            realm!!.commitTransaction()
        }


    }

    override fun onContactSelected() {
        setupRecycler()
    }
    var isPhone =  false
    private val PICK_IMAGE_CAMERA = 1
    private val PICK_IMAGE_GALLERY = 2
    private val listMonney = ArrayList<monneyModel>()
    private var realm: Realm? = null
    private var studentAdapter: StudentAdapter? = null
    private var searchView: SearchView? = null
    private lateinit var databaseReference: DatabaseReference
    private lateinit var toolbar: Toolbar
    private lateinit var dbChild: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.item_list_student)
        this.realm = RealmController.with(this).realm

        instance = this
        //set toolbar appearance
        databaseReference = FirebaseDatabase.getInstance().reference
        //for crate home button


        // refresh the realm instance
        RealmController.with(this).refresh()
        toolbar = findViewById(R.id.toolbar)



        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        supportActionBar!!.title = "Danh sách võ sinh"
        val metrics = DisplayMetrics()
        this.windowManager.defaultDisplay.getMetrics(metrics)

        val yInches: Float = metrics.heightPixels / metrics.ydpi
        val xInches: Float = metrics.widthPixels / metrics.xdpi
        val diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches.toDouble())
        if (diagonalInches >= 6.5) {
            // 6.5inch device or bigger
            // Toast.makeText(this,"> 6.5", Toast.LENGTH_LONG).show()
        } else {
            // smaller device
            //Toast.makeText(this,"phone", Toast.LENGTH_LONG).show()
            isPhone = true
        }
        setupRecycler()
        //listMonney.clear()
       // dbChild = databaseReference.child("monney" + LoginActivity.instance.nameUser)
       // dbChild.addChildEventListener(this)
//        databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
//            override fun onCancelled(error: DatabaseError) {
//
//            }
//
//            override fun onDataChange(dataSnapshot: DataSnapshot) {
//                println(listMonney.size)
//
//            }
//
//
//        })
    }

    private fun setupRecycler() {


        recycler.setHasFixedSize(true)
        studentAdapter = StudentAdapter(this, getModelList(), this, this,isPhone)

        // white background notification bar

        val mLayoutManager = LinearLayoutManager(this)
        recycler.layoutManager = mLayoutManager
        recycler.itemAnimator = DefaultItemAnimator()
        recycler.addItemDecoration(MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 36))
        recycler.adapter = studentAdapter


        counterStudents.text = getModelList().size.toString()

    }

    fun getModelList(): List<Student> {
        var list = ArrayList<Student>()

        try {
            realm = Realm.getDefaultInstance()
            val results = realm!!.where<Student>(Student::class.java)
                    .findAll()
            list.addAll(realm!!.copyFromRealm<Student>(results))
        } finally {
            if (realm != null) {
                realm!!.close()
            }
        }
        return list
    }
    /*fun getModelLists(): List<monneyModelReaml> {
        var list = ArrayList<monneyModelReaml>()

        try {
            realm = Realm.getDefaultInstance()
            val results = realm!!.where<monneyModelReaml>(monneyModelReaml::class.java)
                    .findAll()
            list.addAll(realm!!.copyFromRealm<monneyModelReaml>(results))
        } finally {
            if (realm != null) {
                realm!!.close()
            }
        }
        return list
    }*/
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_CAMERA) {

            onCaptureImageResult(data)
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            onSelectFromGalleryResult(data)
        }
    }

    private fun onSelectFromGalleryResult(data: Intent?) {

        var bm: Bitmap? = null
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(applicationContext.contentResolver, data.data)
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        println(bm)
        val bytes = ByteArrayOutputStream()
        bm!!.compress(Bitmap.CompressFormat.PNG, 50, bytes)

        val imageBytes = bytes.toByteArray()
        val imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
        println(imageString)
        studentAdapter!!.setimgProfile(imageString)
        //ivImage.setImageBitmap(bm)
    }

    private fun onCaptureImageResult(data: Intent?) {


        val thumbnail = data!!.extras!!.get("data") as Bitmap
        val bytes = ByteArrayOutputStream()
        thumbnail.compress(Bitmap.CompressFormat.PNG, 50, bytes)

        val destination = File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis().toString() + ".jpg")
        val imageBytes = bytes.toByteArray()
        val imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
        println(imageString)


        studentAdapter!!.setimgProfile(imageString)
        val fo: FileOutputStream
        try {
            destination.createNewFile()
            fo = FileOutputStream(destination)
            fo.write(bytes.toByteArray())
            fo.close()
            println("da dong FileOutputStream")
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        // ivImage.setImageBitmap(thumbnail)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the main_menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        val searchManager = this!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager?
        searchView = menu!!.findItem(R.id.action_search).actionView as SearchView
        searchView!!.setSearchableInfo(searchManager!!.getSearchableInfo(this!!.getComponentName()))
        searchView!!.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                studentAdapter!!.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                studentAdapter!!.filter.filter(query)
                return false
            }
        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items

        when (item.itemId) {
            R.id.action_attendance -> {
                showActivity(FgDiemDanh::class.java)

                //replaceFragmentAnimationTopToBottom(FgDiemDanh.newInstance(getModelList()))
                return true
            }
            R.id.action_fee -> {
                showActivity(FgFee::class.java)
                //replaceFragmentAnimationTopToBottom(FgFee.newInstance(getModelList()))
                return true
            }
            R.id.action_export_excel -> {
                showActivity(FgExportExcel::class.java)

                return true
            }

        }
        return super.onOptionsItemSelected(item)

    }


    companion object {
        lateinit var instance: FgListStudent
            private set
    }


}