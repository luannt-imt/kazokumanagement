package app.com.kazoku.ui

import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.activities.realm.RealmController
import app.com.kazoku.lib.adapter.AttendanceAdapter
import app.com.kazoku.lib.adapter.AttendanceAdapterCompact
import app.com.kazoku.lib.model.Student
import io.realm.Realm
import kotlinx.android.synthetic.main.attendance_fg.*
import java.text.SimpleDateFormat
import java.util.*


class FgDiemDanh : GTActivity() {

    private var realm: Realm? = null
    var dayOfTheWeek: String? = null
    var list = ArrayList<Student>()
    var listFilterDay = ArrayList<Student>()
    var listCa = ArrayList<Student>()
    private var studentAdapter: AttendanceAdapter? = null
    private var studentAdapterCompact: AttendanceAdapterCompact? = null

    var day: String? = null
    var caChoose: String? = null
    var type = true


    fun getModelList(): ArrayList<Student> {
        var list = ArrayList<Student>()

        try {
            realm = Realm.getDefaultInstance()
            val results = realm!!.where<Student>(Student::class.java)
                    .findAll()
            list.addAll(realm!!.copyFromRealm<Student>(results))
        } finally {
            if (realm != null) {
                realm!!.close()
            }
        }
        return list
    }

    public  fun getDayName():String{
        var Str = ""
//        <item>Thứ 2</item>
//        <item>Thứ 3</item>
//        <item>Thứ 4</item>
//        <item>Thứ 5</item>
//        <item>Thứ 6</item>
//        <item>Thứ 7</item>
//        <item>Chủ nhật</item>
        if (day == "Thứ 2") {
            Str = "T2"
        }
        if (day == "Thứ 3") {

            Str = "T3"
        }
        if (day == "Thứ 4") {
            Str = "T4"
        }
        if (day == "Thứ 5") {
            Str = "T5"
        }
        if (day == "Thứ 6") {
//            day!!.replacea("Thứ Sáu", "T6")
            Str = "T6"
        }
        if (day == "Thứ 7") {
            Str = "T7"
        }
        if (day == "Chủ nhật") {
            Str = "CN"
        }
        return Str

    }

    private lateinit var toolbar: Toolbar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.attendance_fg)

        toolbar = findViewById(R.id.toolbar)

        val dayArray = resources.getStringArray(R.array.day)


        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        supportActionBar!!.title = "Danh sách điểm danh"

        val sdf = SimpleDateFormat("EEEE")
        val d = Date()
        this.realm = RealmController.with(this).realm
        RealmController.with(this).refresh()
        dayOfTheWeek = sdf.format(d).toString()
        txtDay.text = "Hôm nay là thứ: $dayOfTheWeek"


//        day = day!!.replace("Thứ Sáu", "")
//        if (dayOfTheWeek == "Thứ Hai" || dayOfTheWeek == "Thứ hai") {
//            day = day!!.replace(dayOfTheWeek + "", "T2")
//        }
//        if (dayOfTheWeek == "Thứ Ba" || dayOfTheWeek == "Thứ ba") {
//
//            day = day!!.replace(dayOfTheWeek + "", "T3")
//        }
//        if (dayOfTheWeek == "Thứ Tư" || dayOfTheWeek == "Thứ tư") {
//            day = day!!.replace(dayOfTheWeek + "", "T4")
//        }
//        if (dayOfTheWeek == "Thứ Năm" || dayOfTheWeek == "Thứ năm") {
//            day = day!!.replace(dayOfTheWeek + "", "T5")
//        }
//        if (dayOfTheWeek == "Thứ Sáu" || dayOfTheWeek == "Thứ sáu") {
////            day!!.replacea("Thứ Sáu", "T6")
//            day = day!!.replace(dayOfTheWeek + "", "T6")
//        }
//        if (dayOfTheWeek == "Thứ Bảy" || dayOfTheWeek == "Thứ bảy") {
//            day = day!!.replace(dayOfTheWeek + "", "T7")
//        }
//        if (dayOfTheWeek == "Chủ Nhật" || dayOfTheWeek == "Chủ nhật") {
//            day = day!!.replace(dayOfTheWeek + "", "CN")
//        }

        list = getModelList()
        listFilterDay = list;
//        for (i in 0 until list.size) {
//            for (j in 0 until list[i].buoiTapModels.size) {
//                if (list[i].buoiTapModels[j].isSelect) {
//
//                    if (list[i].buoiTapModels[j].dayOfWeek == getDayName()) {
//                        var student = Student()
//                        student.address = list[i].address
//                        student.beltColorsModel = list[i].beltColorsModel
//                        student.buoiTapModels = list[i].buoiTapModels
 //                        student.dateStudy = list[i].dateStudy
//                        student.dob = list[i].dob
//                        student.id = list[i].id
//                        student.name = list[i].name
//                        student.imageUrl = list[i].imageUrl
//                        student.note = list[i].note
//                        student.phone = list[i].phone
//                        student.monthModels = list[i].monthModels
//                        listFilterDay.add(student)
//                    }
//                }
//            }
//        }

        btnCa1.setOnClickListener {
            listCa.clear()
            caChoose = "Ca 1"
            getData()
            btnCa1.setTextColor(Color.RED)
            btnCa2.setTextColor(Color.WHITE)
            btnCa3.setTextColor(Color.WHITE)
            btnCa4.setTextColor(Color.WHITE)
            btnCa5.setTextColor(Color.WHITE)
            btnCa6.setTextColor(Color.WHITE)
        }
        btnCa2.setOnClickListener {
            listCa.clear()
            caChoose = "Ca 2"
            getData()
            btnCa2.setTextColor(Color.RED)
            btnCa1.setTextColor(Color.WHITE)
            btnCa3.setTextColor(Color.WHITE)
            btnCa4.setTextColor(Color.WHITE)
            btnCa5.setTextColor(Color.WHITE)
            btnCa6.setTextColor(Color.WHITE)
        }
        btnCa3.setOnClickListener {
            listCa.clear()
            caChoose = "Ca 3"
            getData()

            btnCa3.setTextColor(Color.RED)
            btnCa1.setTextColor(Color.WHITE)
            btnCa2.setTextColor(Color.WHITE)
            btnCa4.setTextColor(Color.WHITE)
            btnCa5.setTextColor(Color.WHITE)
            btnCa6.setTextColor(Color.WHITE)
        }
        btnCa4.setOnClickListener {
            listCa.clear()
            caChoose = "Ca 4"
            getData()
            btnCa4.setTextColor(Color.RED)
            btnCa1.setTextColor(Color.WHITE)
            btnCa2.setTextColor(Color.WHITE)
            btnCa3.setTextColor(Color.WHITE)
            btnCa5.setTextColor(Color.WHITE)
            btnCa6.setTextColor(Color.WHITE)
        }
        btnCa5.setOnClickListener {
            listCa.clear()
            caChoose = "Ca 5"
            getData()
            btnCa5.setTextColor(Color.RED)
            btnCa1.setTextColor(Color.WHITE)
            btnCa2.setTextColor(Color.WHITE)
            btnCa3.setTextColor(Color.WHITE)
            btnCa4.setTextColor(Color.WHITE)
            btnCa6.setTextColor(Color.WHITE)
        }
        btnCa6.setOnClickListener {
            listCa.clear()
            caChoose = "Ca 6"
            getData()
            btnCa6.setTextColor(Color.RED)
            btnCa1.setTextColor(Color.WHITE)
            btnCa2.setTextColor(Color.WHITE)
            btnCa3.setTextColor(Color.WHITE)
            btnCa4.setTextColor(Color.WHITE)
            btnCa5.setTextColor(Color.WHITE)
        }

        val spinner = findViewById<Spinner>(R.id.spnGift)
        if (spinner != null) {
            val adapter = ArrayAdapter(this,
                    R.layout.spinner_row, dayArray)
            spinner.adapter = adapter

            spinner.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long) {
//                    Toast.makeText(this, getString(R.string.selected_item) + " " + "" + languages[position], Toast.LENGTH_SHORT).show()
                    (view as TextView).setTextColor(ContextCompat.getColor(this@FgDiemDanh, R.color.colorAccent))
                    day = dayArray[position].toString()
                    btnCa1.setTextColor(Color.WHITE)
                    btnCa2.setTextColor(Color.WHITE)
                    btnCa3.setTextColor(Color.WHITE)
                    btnCa4.setTextColor(Color.WHITE)
                    btnCa5.setTextColor(Color.WHITE)
                    btnCa6.setTextColor(Color.WHITE)
                    studentAdapter?.refreshData()


                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the main_menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_list_style, menu)


        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items

        when (item.itemId) {

            R.id.action_compact -> {
                // Toast.makeText(this,"action_compact",Toast.LENGTH_SHORT).show()
                type = false
                setupRecyclerCompact()
                return true
            }
            R.id.action_normal -> {
                //Toast.makeText(this,"action_normal",Toast.LENGTH_SHORT).show()
                type = true
                setupRecycler()
                return true
            }

        }
        return super.onOptionsItemSelected(item)

    }

    private fun getData( ) {
        for (i in 0 until listFilterDay.size) {

            for (j in 0 until listFilterDay[i].buoiTapModels.size) {
                if (listFilterDay[i].buoiTapModels[j].isSelect) {
                    if (listFilterDay[i].buoiTapModels[j].cahoc == caChoose) {
                        if (listFilterDay[i].buoiTapModels[j].dayOfWeek == getDayName()) {
                            var student = Student()
                            student.address = listFilterDay[i].address
                            student.beltColorsModel = listFilterDay[i].beltColorsModel
                            student.buoiTapModels = listFilterDay[i].buoiTapModels
//                        student.buoiTapModels. = listFilterDay[i].caModels
                            student.dateStudy = listFilterDay[i].dateStudy
                            student.dob = listFilterDay[i].dob
                            student.id = listFilterDay[i].id
                            student.name = listFilterDay[i].name
                            student.imageUrl = listFilterDay[i].imageUrl
                            student.note = listFilterDay[i].note
                            student.phone = listFilterDay[i].phone
                            student.monthModels = listFilterDay[i].monthModels
                            listCa.add(student)
                        }

                    }
                }


            }
        }
        if (type) {
            setupRecycler()
        } else {
            setupRecyclerCompact()
        }


    }

    private fun setupRecycler() {


        rcvAttendance.setHasFixedSize(true)
        studentAdapter = AttendanceAdapter(this, listCa)

        // white background notification bar

        val mLayoutManager = LinearLayoutManager(this)
        rcvAttendance.layoutManager = mLayoutManager
        rcvAttendance.itemAnimator = DefaultItemAnimator()
        rcvAttendance.adapter = studentAdapter
        txtTotal.text = "Tổng số: " + studentAdapter!!.itemCount

    }

    private fun setupRecyclerCompact() {


        rcvAttendance.setHasFixedSize(true)
        studentAdapterCompact = AttendanceAdapterCompact(this, listCa)

        // white background notification bar

        val mLayoutManager = LinearLayoutManager(this)
        rcvAttendance.layoutManager = mLayoutManager
        rcvAttendance.itemAnimator = DefaultItemAnimator()
        rcvAttendance.adapter = studentAdapterCompact
        txtTotal.text = "Tổng số: " + studentAdapterCompact!!.itemCount

    }
}