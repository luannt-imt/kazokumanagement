package app.com.kazoku.ui

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import app.com.kazoku.R
import app.com.kazoku.adapter.VideoAdapter
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.models.UploadVideoModel
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_show_you_tu_be.*
import java.util.ArrayList

class ActivityShowYouTuBe : GTActivity(), ChildEventListener {
    override fun onCancelled(error: DatabaseError) {

    }

    override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

    }

    override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
        finish()
    }

    override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
        listData.add(snapshot!!.getValue(UploadVideoModel::class.java)!!)

        displayUsers(listData)
    }

    override fun onChildRemoved(snapshot: DataSnapshot) {
     }

    private lateinit var toolbar: Toolbar
    private lateinit var databaseReference: DatabaseReference
    private lateinit var dbChild: DatabaseReference
    private val listData = ArrayList<UploadVideoModel>()
    private var myAdapter: VideoAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_you_tu_be)
        toolbar = findViewById(R.id.toolbar)



        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        supportActionBar!!.title ="Danh Sách Bài Tập"
        loadData()
    }

    private fun loadData() {
        listData.clear()
        databaseReference = FirebaseDatabase.getInstance().reference
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        myAdapter = VideoAdapter(this)
        recyclerView.adapter = myAdapter
        dbChild = databaseReference.child("link")
        dbChild.addChildEventListener(this)
    }

    //display the user on Adapter
    private fun displayUsers(ls: List<UploadVideoModel>) {

        myAdapter!!.setData(ls)
    }
}
