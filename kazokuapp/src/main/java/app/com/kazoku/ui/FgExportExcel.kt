package app.com.kazoku.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import app.com.kazoku.LoginActivity
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.activities.realm.RealmController
import app.com.kazoku.lib.model.Student
import io.realm.Realm
import kotlinx.android.synthetic.main.content_export_data.*
import org.apache.poi.hssf.usermodel.HSSFDateUtil
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.FormulaEvaluator
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.util.WorkbookUtil
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*


class FgExportExcel : GTActivity() {
    var months = "";
    private val columns = arrayOf("Tên", "Ngày Vào Học", "Phone", "Ghi chú", "Học phí")

    var list = ArrayList<Student>()

    private var realm: Realm? = null
    private lateinit var toolbar: Toolbar

    var outFileName: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.export_excel_fg)
        this.realm = RealmController.with(this).realm
        RealmController.with(this).refresh()
        toolbar = findViewById(R.id.toolbar)



        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        supportActionBar!!.title = "Xuất danh sách học sinh"

        list = getModelList()



    }

    fun onWriteClick(view: View) {
        printlnToUser("writing xlsx file")
        val workbook = XSSFWorkbook()
        val sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("mysheet"))
        val dateOfBirth = Calendar.getInstance()



        val headerFont = workbook.createFont()
        headerFont.bold = true
        headerFont.fontHeightInPoints = 14.toShort()
        headerFont.color = IndexedColors.RED.getIndex()

        // Create a CellStyle with the font
        val headerCellStyle = workbook.createCellStyle()
        headerCellStyle.setFont(headerFont)

        // Create a Row
        val headerRow = sheet.createRow(0)

        // Creating cells
        for (i in columns.indices) {
            val cell = headerRow.createCell(i)
            cell.setCellValue(columns[i])
            cell.cellStyle = headerCellStyle
        }

        /*  for (int i=0;i<10;i++) {
            Row row = sheet.createRow(i);
            Cell cell = row.createCell(0);
            cell.setCellValue(i);
        }*/
        val createHelper = workbook.creationHelper

        val dateCellStyle = workbook.createCellStyle()
        dateCellStyle.dataFormat = createHelper.createDataFormat().getFormat("dd-MM-yyyy")

        var rowNum = 1
        //"id","Tên","Tuổi","Ngày Vào Học","Phone", "Học phí", "Hình", "Ghi chú","FaceBook","Address","Ca","Buổi tập"
        for (employee in list) {
            var temp = false
            months = ""
            val row = sheet.createRow(rowNum++)

            row.createCell(0).setCellValue(employee.name)

            row.createCell(1).setCellValue(employee.dateStudy)
            row.createCell(2).setCellValue(employee.phone)
            row.createCell(3).setCellValue(employee.note)
           // for (var i = 0 i < student!!.getBuoiTapModels ().size; i++)
            for (i in 0 until employee.monthModels.size) {

                var select = employee.monthModels!![i].isSelect
                 if (select){
                     if (!temp) {
                         months = employee.monthModels[i].monthsName
                         temp = true
                     } else {
                         months =  months + ", " + employee.monthModels[i].monthsName
                     }
                 }
            }
            row.createCell(4).setCellValue(months)

        }
        try {
            val formatter = SimpleDateFormat("yyyy_MM_dd", Locale.US)
            val now = Date()
            val fileName = formatter.format(now)
            outFileName = fileName +"_"+LoginActivity.instance.nameUser+ "_DSach_Vsinh.xlsx"
        }catch (e:Exception){
            e.printStackTrace()
        }
//yy
        try {
            printlnToUser("writing file $outFileName")
            val cacheDir = cacheDir
            val outFile = File(cacheDir, outFileName)
            val outputStream = FileOutputStream(outFile.absolutePath)
            workbook.write(outputStream)
            outputStream.flush()
            outputStream.close()
            printlnToUser("sharing file...")
            /*printlnToUser("writing file $outFileName")
            val cacheDir = cacheDir
            val outFile = File(getExternalFilesDir(null), outFileName)
            val outputStream = FileOutputStream(outFile.absolutePath)
            workbook.write(outputStream)
            outputStream.flush()
            outputStream.close()
            printlnToUser("sharing file...")*/

            share(this!!.outFileName!!, applicationContext)
        } catch (e: Exception) {
            /* proper exception handling to be here */
            printlnToUser(e.toString())
        }

    }
     protected fun getCellAsString(row: Row, c: Int, formulaEvaluator: FormulaEvaluator): String {
        var value = ""
        try {
            val cell = row.getCell(c)
            val cellValue = formulaEvaluator.evaluate(cell)
            when (cellValue.cellType) {
                Cell.CELL_TYPE_BOOLEAN -> value = "" + cellValue.booleanValue
                Cell.CELL_TYPE_NUMERIC -> {
                    val numericValue = cellValue.numberValue
                    if (HSSFDateUtil.isCellDateFormatted(cell)) {
                        val date = cellValue.numberValue
                        val formatter = SimpleDateFormat("dd/MM/yy")
                        value = formatter.format(HSSFDateUtil.getJavaDate(date))
                    } else {
                        value = "" + numericValue
                    }
                }
                Cell.CELL_TYPE_STRING -> value = "" + cellValue.stringValue
            }
        } catch (e: NullPointerException) {
            /* proper error handling should be here */
            printlnToUser(e.toString())
        }

        return value
    }

    /**
     * print line to the output TextView
     * @param str
     */
    private fun printlnToUser(str: String) {
        if (textOut.length() > 8000) {
            var fullOutput: CharSequence = textOut.text
            fullOutput = fullOutput.subSequence(5000, fullOutput.length)
            textOut.setText(fullOutput)
            textOut.setSelection(fullOutput.length)
        }
        textOut.append(str + "\n")
    }
    fun share(fileName: String, context: Context) {
        val fileUri = Uri.parse("content://$packageName/$fileName")

        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri)
        shareIntent.type = "application/octet-stream"
        startActivity(Intent.createChooser(shareIntent, "send to.."))
    }
    fun getModelList(): ArrayList<Student> {
        var list = ArrayList<Student>()

        try {
            realm = Realm.getDefaultInstance()
            val results = realm!!.where<Student>(Student::class.java)
                    .findAll()
            list.addAll(realm!!.copyFromRealm<Student>(results))
        } finally {
            if (realm != null) {
                realm!!.close()
            }
        }
        return list
    }

}