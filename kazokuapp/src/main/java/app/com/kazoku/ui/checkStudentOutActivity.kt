package app.com.kazoku.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import app.com.kazoku.LoginActivity
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.adapter.MyFeeRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_check_student_out.*
import java.util.ArrayList

class checkStudentOutActivity :  GTActivity(), MyFeeRecyclerViewAdapter.ItemClickListener{
    override fun onItemClick(view: View?, position: Int) {
        gotoDetails(listSchoolName[position])
    }
    private fun gotoDetails(name:  String){
        val intent = Intent(this, StudentDeleteActivity::class.java)
        intent.putExtra("name",name)
        startActivity(intent)
    }
    private lateinit var toolbar: Toolbar
    private var schoolAdapter: MyFeeRecyclerViewAdapter? = null
    var listSchoolName = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_student_out)
        toolbar = findViewById(R.id.toolbar)



        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        listSchoolName = LoginActivity.instance.listSchoolName
        supportActionBar!!.title = "Quản lý học sinh nghỉ"

        gotoRecylerview()
    }
    private fun gotoRecylerview(){

        rcvFee.setHasFixedSize(true)
        schoolAdapter = MyFeeRecyclerViewAdapter(this,listSchoolName)
        val mLayoutManager = LinearLayoutManager(this)
        rcvFee.layoutManager = mLayoutManager
        schoolAdapter!!.setClickListener(this)

        rcvFee.adapter = schoolAdapter
    }
}
