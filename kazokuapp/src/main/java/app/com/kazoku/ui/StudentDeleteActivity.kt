package app.com.kazoku.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.adapter.MyStudentDeleteRecyclerViewAdapter
import app.com.kazoku.lib.model.modelConver.deleteModelN
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_student_delete.*
import java.util.ArrayList

class StudentDeleteActivity : GTActivity(), ChildEventListener , MyStudentDeleteRecyclerViewAdapter.ItemClickListener{
    override fun onItemClick(view: View?, position: Int) {
        println("phone: "+ listStudentDelete[position].phone)
        val callIntent = Intent(Intent.ACTION_DIAL)
        callIntent.data = Uri.parse("tel:" + listStudentDelete[position].phone)
        callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(callIntent)
    }

    override fun onCancelled(error: DatabaseError) {

    }

    override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
    }

    override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
    }

    override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
        listStudentDelete.add(snapshot!!.getValue(deleteModelN::class.java)!!)
        println(listStudentDelete.size)
    }

    override fun onChildRemoved(snapshot: DataSnapshot) {
    }
    private var schoolAdapter: MyStudentDeleteRecyclerViewAdapter? = null
     private val listStudentDelete = ArrayList<deleteModelN>()
    private lateinit var toolbar: Toolbar
    private lateinit var databaseReference: DatabaseReference
    private lateinit var dbChild: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student_delete)
        val name = intent.getStringExtra("name")
        println("ahihii $name")
        toolbar = findViewById(R.id.toolbar)

        databaseReference = FirebaseDatabase.getInstance().reference

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        supportActionBar!!.title = "Bấm vào để gọi phụ huynh"
        actionDownLoad(name)
    }

    private fun actionDownLoad(name: String) {
        showLoadingCircle("Tải dữ liệu", "Đang tiến hành. Vui lòng chờ trong giây lát..")

        listStudentDelete.clear()
        dbChild = databaseReference.child("delete$name")
        dbChild.addChildEventListener(this)
        databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
//                println(listMonney.size)
//                txtFee.text = "Số lần thu học phí: "+ listMonney.size
//                txtFee.setTextColor(resources.getColor(R.color.red))
                hideLoadingCircle()
                gotoRecylerview()
            }


        })
    }
    fun gotoRecylerview(){
        rcvDelete.setHasFixedSize(true)
        schoolAdapter = MyStudentDeleteRecyclerViewAdapter(this,listStudentDelete)
        val mLayoutManager = LinearLayoutManager(this)
        rcvDelete.layoutManager = mLayoutManager
        schoolAdapter!!.setClickListener(this)

        rcvDelete.adapter = schoolAdapter
    }
}
