package app.com.kazoku.ui

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Base64
import android.util.DisplayMetrics
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.com.kazoku.R
import app.com.kazoku.lib.activities.GTActivity
import app.com.kazoku.lib.activities.realm.RealmController
import app.com.kazoku.lib.adapter.CaAdapter
import app.com.kazoku.lib.adapter.ColorAdapter
import app.com.kazoku.lib.adapter.DayOfWeekAdapter
import app.com.kazoku.lib.adapter.MonthAdapter
import app.com.kazoku.lib.model.*
import app.com.kazoku.permison.RxPermissions
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.content_register_student.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class FgRegister : GTActivity(), DayOfWeekAdapter.adapterListener {


    override fun onClickRemove(buoiTap: buoiTapModel?) {

        for (i in 0 until buoiTapModelsCa!!.size) {
            if (!flag){
                if (buoiTapModelsCa!![i].dayOfWeek == buoiTap!!.dayOfWeek) {
                    try {
                        buoiTapModelsCa!!.removeAt(i)
                        flag = true

                    }catch (e: Exception){
                        e.printStackTrace()
                    }

                }
            }


        }
        println("hhhhhhhhh")
        caAdapter!!.notifyDataSetChanged()
        flag = false
    }

    override fun onClick(  buoiTap: buoiTapModel) {
        buoiTapModelsCa!!.add(buoiTap)
        caAdapter!!.notifyDataSetChanged()
    }
    var flag = false
    internal lateinit var listItems: Array<String>
    private lateinit var toolbar: Toolbar
    private lateinit var databaseReference: DatabaseReference
    val PICK_IMAGE_CAMERA = 1
    val PICK_IMAGE_GALLERY = 2
    var tmpCa1 = false
    var tmpCa2 = false
    var tmpCa3 = false
    var tmpCa4 = false
    var tmpCa5 = false
    var tmpCa6 = false
    private var monthAdapter: MonthAdapter? = null
    private var dayOfWeekAdapter: DayOfWeekAdapter? = null
    private var caAdapter: CaAdapter? = null
    private var colorAdapter: ColorAdapter? = null
    private var monthModelList: MutableList<monthModel>? = null
    private var buoiTapModels: MutableList<buoiTapModel>? = null
    private var buoiTapModelsCa: MutableList<buoiTapModel>? = null
    private var beltColors: List<BeltColor>? = null
    private var realm: Realm? = null
    var imageString: String? = ""
    var monthSelect: ArrayList<String> = ArrayList()
    var isPhone =  false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.item_register_student)
        this.realm = RealmController.with(this).realm
        databaseReference = FirebaseDatabase.getInstance().reference
        toolbar = findViewById(R.id.toolbar)
        // overridePendingTransition(R.anim.slide_bottom_to_top, R.anim.gotadi_view_slide_out_down)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        val metrics = DisplayMetrics()
        this.windowManager.defaultDisplay.getMetrics(metrics)

        val yInches: Float = metrics.heightPixels / metrics.ydpi
        val xInches: Float = metrics.widthPixels / metrics.xdpi
        val diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches.toDouble())
        if (diagonalInches >= 6.5) {
            // 6.5inch device or bigger
           // Toast.makeText(this,"> 6.5", Toast.LENGTH_LONG).show()
        } else {
            // smaller device
            //Toast.makeText(this,"phone", Toast.LENGTH_LONG).show()
            isPhone = true
        }
        supportActionBar!!.title = "Đăng ký võ sinh"

        btnRegister.setOnClickListener {
            if (validate()) {


                val student = Student()

                try {
                    student.id = RealmController.getInstance().books.size + System.currentTimeMillis()
                    student.name = name_edit_text.text!!.toString().trim()
                    student.dob = dob_edit_text.text!!.toString().trim()
                    student.phone = phone_edit_text.text!!.toString().trim()
                    student.address = address_edit_text.text!!.toString().trim()
                    student.note = note_edit_text.text!!.toString().trim()
                    student.imageUrl = imageString
                    val currentDate = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault()).format(Date())

                    student.dateStudy = currentDate


                    val realmIntRealmList = RealmList<monthModel>()

                    for (i in monthModelList!!.indices) {
                        val realmInt = monthModel()

                        realmInt.isSelect = monthModelList!![i].isSelect
                        realmInt.monthsName = monthModelList!![i].monthsName
                        realmIntRealmList.add(realmInt)
                        if (monthModelList!![i].isSelect) {
                            println(monthModelList!![i].monthsName)

                            val c = Calendar.getInstance()
                            val cmonth = c.get(Calendar.MONTH) + 1
                            var monthName = monthModelList!![i].monthsName
                            if (monthModelList!![i].monthsName == getString(R.string.txt_fee_free)){
                                monthName = "T13"
                            }
                            if (monthModelList!![i].monthsName == "Học thử"){
                                monthName = "T14"
                            }
                            val domain: String? = monthName.substringAfterLast("T")
                            var m = Integer.parseInt(domain.toString())
                            println(domain) // -
                            if (m == 13 || m == 14){

                            }else{
                                if (m >= cmonth) {
                                      monthSelect.add(monthModelList!![i].isSelect.toString())
                                    //Toast.makeText(this,"$m lon bang $cmonth hien tai",Toast.LENGTH_LONG).show()
                                }
                            }



                        }

                    }
                    val list = RealmList<buoiTapModel>()
                    for (i in buoiTapModels!!.indices) {
                        val realmInt = buoiTapModel()
                        realmInt.isSelect = buoiTapModels!![i].isSelect
                        realmInt.dayOfWeek = buoiTapModels!![i].dayOfWeek
                        realmInt.cahoc = buoiTapModels!![i].cahoc
                        list.add(realmInt)

                    }
                    val beltColorsRealmList = RealmList<BeltColor>()

                    for (i in beltColors!!.indices) {
                        val beltColor = BeltColor()
                        beltColor.isSelect = beltColors!![i].isSelect
                        beltColor.color = beltColors!![i].color

                        beltColorsRealmList.add(beltColor)

                    }
                   // val listCa = RealmList<caModel>()

                    student.monthModels = realmIntRealmList
//                    student.caModels = listCa
                    student.buoiTapModels = list
                    student.beltColorsModel = beltColorsRealmList

                    realm!!.beginTransaction()
                    realm!!.copyToRealm(student)
                    realm!!.commitTransaction()


                    uploadDataRealm()


                    if (isOnline(this)) {
                        // Toast.makeText(context, "co mang", Toast.LENGTH_SHORT).show()
                        //uploadData()
                    } else {

                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }


            }
        }
        btnCamera.setOnClickListener {
            var rx = RxPermissions(this)
            rx.request(Manifest.permission.CAMERA).subscribe {
                if (rx.isGranted(Manifest.permission.CAMERA)) {
                    selectImage()
                } else {
                    println("chua")
                }
            }

//             new RxPermissions(this)
//.request(Manifest.permission.CAMERA) // ask single or multiple permission once
//.subscribe(granted -> {
//    if (granted) {
//       // All requested permissions are granted
//    } else {
//       // At least one permission is denied
//    }
//});

        }
        setFee()
        setDayOfWeek()
        setCa()
        setBeltColor()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_IMAGE_CAMERA) {
                println("aaa11: PICK_IMAGE_CAMERA")
                onCaptureImageResult(data)
            } else if (requestCode == PICK_IMAGE_GALLERY) {
                println("aaa1111: PICK_IMAGE_GALLERY")
                onSelectFromGalleryResult(data)
            }
        }
    }

    private fun onSelectFromGalleryResult(data: Intent?) {

        var bm: Bitmap? = null
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(applicationContext.contentResolver, data.data)
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        println(bm)
        val bytes = ByteArrayOutputStream()
        bm!!.compress(Bitmap.CompressFormat.JPEG, 60, bytes)

        val imageBytes = bytes.toByteArray()
        imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
        println(imageString)
        // studentAdapter!!.setimgProfile(imageString)
        //ivImage.setImageBitmap(bm)
    }

    private fun onCaptureImageResult(data: Intent?) {


        val thumbnail = data!!.extras!!.get("data") as Bitmap
        val bytes = ByteArrayOutputStream()
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 60, bytes)

        val destination = File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis().toString() + ".jpg")
        val imageBytes = bytes.toByteArray()
        imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
        println(imageString)


        val fo: FileOutputStream
        try {
            destination.createNewFile()
            fo = FileOutputStream(destination)
            fo.write(bytes.toByteArray())
            fo.close()
            println("da dong FileOutputStream")
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        // ivImage.setImageBitmap(thumbnail)

    }

    private fun selectImage() {
        try {
            val pm = this.packageManager
            val hasPerm = pm.checkPermission(Manifest.permission.CAMERA, this.packageName)
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                val options = arrayOf<CharSequence>("Take Photo", "Choose From Gallery", "Cancel")
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Select Option")
                builder.setItems(options) { dialog, item ->
                    if (options[item] == "Take Photo") {
                        dialog.dismiss()
                        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        (this as Activity).startActivityForResult(intent, PICK_IMAGE_CAMERA)
                    } else if (options[item] == "Choose From Gallery") {
                        dialog.dismiss()
                        val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                        (this as Activity).startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY)
                    } else if (options[item] == "Cancel") {
                        dialog.dismiss()
                    }
                }
                builder.show()
            } else {
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }


    }


    private fun uploadDataRealm() {
        for (i in monthSelect.indices) {
//            var monneyModel = monneyModel(strings[i])
            var monneyModelReamls = monneyModelReaml()
            monneyModelReamls.monney = monthSelect[i]
            monneyModelReamls.id = RealmController.getInstance().books.size + System.currentTimeMillis()


            realm!!.beginTransaction()
            realm!!.copyToRealm(monneyModelReamls)
            realm!!.commitTransaction()
        }
        dialogNoData("Đăng ký thành công")
        //databaseReference.child(LoginActivity.instance.nameUser.toString()).push().setValue(student)
    }

    private fun dialogNoData(msg: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Chú ý!!")
        builder.setMessage(msg)

        builder.setPositiveButton("Đồng ý") { dialog, which ->
            dialog.dismiss()
            sendSMS()
        }
        builder.show()
    }

    private fun sendSMS() {
        val phone  = phone_edit_text.text!!.toString().trim()
        val name  = name_edit_text.text!!.toString().trim()
        val uri = Uri.parse("smsto:$phone")
        val it = Intent(Intent.ACTION_SENDTO, uri)
        it.putExtra("sms_body", "CLB Karatedo xác nhận võ sinh" + name + " đã đăng ký thành công!")
        startActivity(it)
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    private fun setCa() {
        val recyclerView = findViewById<View>(R.id.recycler_view_ca) as RecyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(this, 7)
        buoiTapModelsCa = ArrayList<buoiTapModel>()
        caAdapter = CaAdapter(buoiTapModelsCa)

        recyclerView.adapter = caAdapter
    }

    private fun setDayOfWeek() {
        val recyclerView = findViewById<View>(R.id.recycler_view_dayOfWeek) as RecyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(this, 7)
        buoiTapModels = ArrayList<buoiTapModel>()

        for (i in 2..8) {
            val model = buoiTapModel()
            if (i == 8) {
                model.dayOfWeek = "CN"
            } else {
                model.dayOfWeek = "T$i"
            }
            model.isSelect = false
            buoiTapModels!!.add(model)
        }
        listItems = resources.getStringArray(R.array.ca_item)
        dayOfWeekAdapter = DayOfWeekAdapter(buoiTapModels, listItems, this, this,isPhone)
        recyclerView.adapter = dayOfWeekAdapter
    }

    private fun setFee() {
        val recyclerView = findViewById<View>(R.id.recycler_view) as RecyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(this, 6)
        monthModelList = ArrayList<monthModel>()

        for (i in 1..12) {
            val model = monthModel()
            model.monthsName = "T$i"
            model.isSelect = false
            monthModelList!!.add(model)
        }

        val model = monthModel()
        model.monthsName = getString(R.string.txt_fee_free)
        model.isSelect = false
        monthModelList!!.add(model)
        val hocthu = monthModel()
        hocthu.monthsName = getString(R.string.txt_study)
        hocthu.isSelect = false
        monthModelList!!.add(hocthu)
        listItems = resources.getStringArray(R.array.ca_item)
        monthAdapter = MonthAdapter(monthModelList, monthSelect, isPhone)
        recyclerView.adapter = monthAdapter
    }

    private fun setBeltColor() {
        val recyclerView = findViewById<View>(R.id.recycler_view_color) as RecyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(this, 5)
        beltColors = ArrayList()

        beltColors = BeltColor.getDemo()
        colorAdapter = ColorAdapter(beltColors, this)
        recyclerView.adapter = colorAdapter
    }


    private fun validate(): Boolean {
        var stastus = true
        if (TextUtils.isEmpty(name_edit_text.text!!.toString().trim())) {
            stastus = false
            name_edit_text.error = "Tên không được rỗng"
        }
        if (TextUtils.isEmpty(phone_edit_text.text!!.toString().trim()) || phone_edit_text.text!!.toString().length < 10 || phone_edit_text.text!!.toString().length > 13) {
            stastus = false
            phone_edit_text.error = "SĐT không hợp lệ"
        }
        var tempMonth = 0
        var tempbthap = 0
        var tempColor = 0
        for (i in monthModelList!!.indices) {
            if (!monthModelList!![i].isSelect) {
                tempMonth++

                //                Toast.makeText(this, "Vui lòng chọn tháng học", Toast.LENGTH_SHORT).show();
            }
        }
        for (i in buoiTapModels!!.indices) {
            if (!buoiTapModels!![i].isSelect) {
                tempbthap++
                //                stastus = true;
                //                Toast.makeText(this, "Vui lòng chọn buổi tập", Toast.LENGTH_SHORT).show();

            }
        }
        for (i in beltColors!!.indices) {
            if (!beltColors!![i].isSelect) {
                tempColor++
            }
        }
        if (tempMonth == 14) {
            stastus = false
            Toast.makeText(this, "Vui lòng chọn tháng học", Toast.LENGTH_SHORT).show()
        }
        if (tempbthap == 7) {
            stastus = false
            Toast.makeText(this, "Vui lòng chọn buổi tập", Toast.LENGTH_SHORT).show()
        }
        if (tempColor == 11) {
            stastus = false
            Toast.makeText(this, "Vui lòng chọn màu đai", Toast.LENGTH_SHORT).show()

        }


        return stastus
    }

}
