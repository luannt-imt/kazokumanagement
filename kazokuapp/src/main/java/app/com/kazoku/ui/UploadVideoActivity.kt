package app.com.kazoku.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import app.com.kazoku.R
import app.com.kazoku.models.UploadVideoModel
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_upload_video.*

class UploadVideoActivity : AppCompatActivity() {
    private lateinit var databaseReference: DatabaseReference
    var url:String?=null
    var iframe:String?=null
    var id:String?=null
    var maudai:String?=null
    var baiTap:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_video)
        databaseReference = FirebaseDatabase.getInstance().reference
        btnUpload.setOnClickListener {
            id = edID.text.toString()
            maudai=edMauDai.text.toString()
            baiTap=edTitle.text.toString()

            if (TextUtils.isEmpty(id)) {
                Toast.makeText(applicationContext, "Vui lòng nhập ID",
                        Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(maudai)) {
                Toast.makeText(applicationContext, "Vui lòng nhập màu đai",
                        Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(maudai)) {
                Toast.makeText(applicationContext, "Vui lòng nhập Tên Bài Tập",
                        Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val id = "DVY0wBMmdcI"

            url = "https://www.youtube.com/embed/$id"
            iframe = "<iframe width=\"100%\" height=\"100%\" src=\"$url\" frameborder=\"0\" allowfullscreen></iframe>"
            var uploadData= UploadVideoModel(maudai!!,id!!,iframe!!,baiTap!!)
            updateDatabase(uploadData)
        }

    }
    fun updateDatabase(uploadData: UploadVideoModel) {
        databaseReference.child("link").push().setValue(uploadData)
        Toast.makeText(this, "Thành công", Toast.LENGTH_SHORT).show()
     }
}
